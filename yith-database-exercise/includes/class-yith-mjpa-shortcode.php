<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

if ( ! class_exists( 'YITH_MJPA_Shortcode' ) ) {
	/**
	 * YITH_MJPA_Shortcode
	 */
	class YITH_MJPA_Shortcode {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Shortcode
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Shortcode Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_mp_add_shortcode' ) );
		}
		/**
		 * Yith_mp_add_shortcode
		 *
		 * @return void
		 */
		public function yith_mp_add_shortcode() {

			add_shortcode( 'yith_mp_show_shortcode_bd', array( $this, 'yith_mp_setup_show_database' ) );
		}

		/**
		 * Yith_mp_setup_show_database
		 *
		 * @param  mixed $attr attr inside shortcode.
		 * @return attr call template with all attr needed.
		 */
		public function yith_mp_setup_show_database( $attr ) {
			$users_list = YITH_MJPA_Transients::check_transient();
			if ( is_user_logged_in() ) {
				$user_id    = get_current_user_id();
				$user_email = get_userdata( $user_id )->data->user_email;
				$parameters = array( $user_email );

				ob_start();
				yith_ps_get_template( '/frontend/show-database.php', $parameters, $users_list );
				return ob_get_clean();
			} else {
				ob_start();
				yith_ps_get_template( '/frontend/show-database.php', $users_list );
				return ob_get_clean();
			}

		}

	}
}
