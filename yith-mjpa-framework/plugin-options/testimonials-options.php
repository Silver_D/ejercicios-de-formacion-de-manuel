<?php
$testimonials = array(
	'testimonials' => array(
		'custom-post-type_list_table' => array(
			'type'      => 'post_type',
			'post_type' => 'yith_testimonials',
		),
	),
);

return $testimonials;
