<?php

	$post_id_info               = $args[0]['id'];
	$orders                     = array();
	$orders_filtered_by_product = array();
foreach ( $args[1] as $key => $value ) {
	$orders[] = wc_get_order( $value );
}
?>
<input type="text" id="myInput" placeholder="Search for names..">
<div id="accordion">
<h4 id="yith_title_product" class="yith_mjpa_title">#<?php echo( esc_html( $post_id_info ) ); ?> checked in</h4>
	<?php
	foreach ( $orders as $key => $value ) {
		foreach ( $value->get_items() as $index => $item ) {
			if ( intval( $post_id_info ) === intval( $item->get_product_id() ) ) {
				for ( $i = 0; $i < $item->get_quantity(); $i++ ) {
					?>
					<div class="yith_display_none yith_title_product_content">
						<p class="container">
						<?php
						$post_item_order = get_posts(
							array(
								'post_type'   => 'event_ticket',
								'meta_query'  => array(
									'relation' => 'AND',
									array(
										'key'   => 'yith_mjpa_item_id',
										'value' => $item->get_id(),
									),
									array(
										'key'   => 'yith_mjpa_event_ticket_order_id',
										'value' => $value->get_id(),
									),
									array(
										'key'   => 'yith_mjpa_event_ticket_for_index',
										'value' => $i,
									),
								),
								'post_status' => 'any',
							)
						);
						$aux             = array(
							'status_ticket'   => $post_item_order[0]->post_status,
							'event_id'        => $item->get_meta( ( 1 !== $item->get_quantity() ) ? 'Event id-' . $i : 'Event id' ),
							'order_id_ticket' => $value->get_id(),
							'author_name'     => get_the_author_meta( 'nicename', $post_item_order[0]->post_author ),
							'ticket_name'     => $item->get_meta( 'Name ' ),
							'ticket_surname'  => $item->get_meta( 'Surname ' ),
						);
						?>
					<span id="icon<?php echo( esc_html( $aux['event_id'] ) ); ?>" class="yith_mjpa_checkbox dashicons  dashicons-tickets-alt 
					<?php
					if ( 'yith_no_check' === $aux['status_ticket'] ) {
						echo( esc_html( 'yith_mjpa_not_checked' ) );
					} else {
						echo( esc_html( 'yith_mjpa_checked' ) );
					}
					?>
					"></span>
					<span class="yith_mjpa_post_id"><?php echo( esc_html( $aux['event_id'] ) ); ?></span>
					<span class="yith_mjpa_username"><?php echo( esc_html( $aux['author_name'] ) ); ?></span>
					<span id="<?php echo( esc_html( $aux['event_id'] ) ); ?>" class="yith_mjpa_button dashicons dashicons-thumbs-up"></span>
					<span class="dispatcher yith_mjpa_wrap dashicons dashicons-arrow-down-alt2">
						<div class="ui-widget-content ui-corner-all yith_display_none">
							<p>
								Name: <?php echo( esc_html( $aux['ticket_name'] ) ); ?>
							</p>
							<p>
								Surname: <?php echo( esc_html( $aux['ticket_surname'] ) ); ?>
							</p>
						</div>
					</span>
					<?php
				}
			}
		}
	}
	?>
	</p>
  </div>
</div>
