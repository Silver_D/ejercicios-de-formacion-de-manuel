<?php
/*
 * This file belongs to the YITH MJPA Tickets.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Frontend' ) ) {

	class YITH_MJPA_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Frontend Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'woocommerce_event_ticket_add_to_cart', array( $this, 'yith_mjpa_print_add_to_cart_button' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_mjpa_print_ticket' ) );
			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'yith_mjpa_add_to_cart_validation' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_mjpa_get_new_item_data' ), 10, 2 );
			add_action( 'woocommerce_add_to_cart_handler_event_ticket', array( $this, 'yith_mjpa_add_to_cart_handler' ), 10, 1 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_mjpa_create_new_order_line' ), 10, 4 );
			add_action( 'woocommerce_new_order_item', array( $this, 'yith_mjpa_create_order' ), 20, 3 );

			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_scripts' ) );
		}
			/**
			 * Add_input_styles
			 *
			 * @return void
			 */
		public function add_input_styles() {
			wp_register_style( 'yith-mjpa-style', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style-ticket-front.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style' );
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {
			global $post;
			$get_ticket = get_post_meta( $post->ID, 'ticket_info', false );
			wp_register_script( 'yith-mjpa-ticket-front', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-script-ticket-front.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-mjpa-ticket-front' );
			if ( ! empty( $get_ticket ) ) {
				wp_localize_script(
					'yith-mjpa-ticket-front',
					'yith_mjpa_vars',
					array(
						'ticket_info' => $get_ticket,
					)
				);
			}

		}
		/**
		 * Yith_mjpa_print_add_to_cart_button
		 *
		 * @return void
		 */
		public function yith_mjpa_print_add_to_cart_button() {
			wc_get_template( 'single-product/add-to-cart/simple.php' );
		}

		/**
		 * Yith_mjpa_create_order
		 *
		 * @param item_id mixed       $item_id item_id.
		 * @param values mixed        $values values.
		 * @param cart_item_key mixed $cart_item_key cart_item_key.
		 * @return void
		 */
		public function yith_mjpa_create_order( $item_id, $values, $cart_item_key ) {

			$meta_data    = isset( $values->legacy_values ) ? $values->legacy_values : $values;
			$product_type = isset( $meta_data['data'] ) ? $meta_data['data']->get_type() : '';
			if ( 'event_ticket' == $product_type ) {
				wc_add_order_item_meta( $item_id, '_Product Type', $product_type );
			}
		}
		/**
		 * Yith_mjpa_print_ticket
		 *
		 * @return void
		 */
		public function yith_mjpa_print_ticket() {
			global $post;
			$get_ticket = get_post_meta( $post->ID, 'ticket_info', false );
			yith_mjpa_get_template( '/frontend/yith_mjpa_front_ticket.php', $get_ticket );
		}
		/**
		 * Yith_mjpa_add_to_cart_handler
		 *
		 * @param url mixed $url url.
		 * @return bool void
		 */
		public function yith_mjpa_add_to_cart_handler( $url ) {
			$cart_item_data['names_tickets']    = $_REQUEST['yith_mjpa_name'];
			$cart_item_data['surnames_tickets'] = $_REQUEST['yith_mjpa_surname'];
			$product_id                         = $_REQUEST['add-to-cart'];
			$quantity                           = empty( $_REQUEST['quantity'] ) ? 1 : wc_stock_amount( wp_unslash( $_REQUEST['quantity'] ) );
			$passed_validation                  = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
			for ( $i = 0; $i < $quantity; $i++ ) {
				$cart_item_data_unitario = array(
					'names_tickets'   => $_REQUEST['yith_mjpa_name'][ $i ],
					'surname_tickets' => $_REQUEST['yith_mjpa_surname'][ $i ],
				);
				if ( ! $passed_validation || false === WC()->cart->add_to_cart( $product_id, 1, 0, array(), $cart_item_data_unitario ) ) {
					return false;
				};
			}
			wc_add_to_cart_message( array( $product_id => $quantity ), true );
			return true;

		}
		/**
		 * Yith_mjpa_add_to_cart_validation
		 *
		 * @param passed mixed     $passed passed.
		 * @param product_id mixed $product_id product_id.
		 * @param quenatity mixed  $quantity quantity.
		 * @return bool void
		 */
		public function yith_mjpa_add_to_cart_validation( $passed, $product_id, $quantity ) {
			$product_ticket = wc_get_product( $product_id );
			if ( $product_ticket->is_type( 'event_ticket' ) ) {
				if ( $_POST['yith_mjpa_name'] && $_POST['yith_mjpa_surname'] ) {
					foreach ( $_POST['yith_mjpa_name'] as $key => $value ) {
						if ( '' === $value ) {
							$passed = false;
							wc_add_notice( __( 'Name is required field', 'yith-plugin-ticket' ), 'error' );
						}
					}
					foreach ( $_POST['yith_mjpa_surname'] as $key => $value ) {
						if ( '' === $value ) {
							$passed = false;
							wc_add_notice( __( 'Surname is required field', 'yith-plugin-ticket' ), 'error' );
						}
					}
				}
			}
			return $passed;
		}
		/**
		 * Yith_mjpa_get_new_item_data
		 *
		 * @param item_data mixed      $item_data item_data.
		 * @param cart_item_data mixed $cart_item_data cart_item_data.
		 * @return item_data void
		 */
		public function yith_mjpa_get_new_item_data( $item_data, $cart_item_data ) {
			$tickets_names    = $cart_item_data['names_tickets'];
			$tickets_surnames = $cart_item_data['surname_tickets'];
			$item_data[]      = array(
				'key'   => 'Name',
				'value' => $tickets_names,
			);
			$item_data[]      = array(
				'key'   => 'Surname',
				'value' => $tickets_surnames,
			);
			return $item_data;
		}
		/**
		 * Yith_mjpa_create_new_order_line
		 *
		 * @param item mixed          $item item.
		 * @param cart_item_key mixed $cart_item_key cart_item_key.
		 * @param values mixed        $values values.
		 * @param order mixed         $order order.
		 * @return void
		 */
		public function yith_mjpa_create_new_order_line( $item, $cart_item_key, $values, $order ) {
			if ( isset( $values['names_tickets'] ) ) {
				$item->add_meta_data(
					'Name ',
					$values['names_tickets'],
					true
				);
			}
			if ( isset( $values['surname_tickets'] ) ) {
				$item->add_meta_data(
					'Surname ',
					$values['surname_tickets'],
					true
				);
			}
		}

	}
}
