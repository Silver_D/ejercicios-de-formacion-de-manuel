<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Database_Plugin_Structure' ) ) {
	/**
	 * YITH_MJPA_Database_Plugin_Structure
	 */
	class YITH_MJPA_Database_Plugin_Structure {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Database_Plugin_Structure
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_MJPA_Database_Plugin_Structure_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_MJPA_Database_Plugin_Structure_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Database_Plugin_Structure Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Database_Plugin_Structure constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_ps_require_class',
				array(
					'common'   => array(
						'includes/class-yith-mjpa-post-types.php',
						'includes/functions.php',
						'includes/class-yith-mjpa-database.php',
						'includes/class-yith-mjpa-shortcode.php',
						'includes/class-yith-mjpa-wp-list-table.php',
						'includes/class-yith-mjpa-transients.php',
						// 'includes/class-yith-ps-ajax.php',
						// 'includes/class-yith-ps-compatibility.php',
						// 'includes/class-yith-ps-other-class.php',
					),
					'admin'    => array(
						'includes/class-yith-mjpa-admin.php',

					),
					'frontend' => array(
						'includes/class-yith-mjpa-frontend.php',
					),
				)
			);

			$this->_require( $require );
			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/

			$this->init();

		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param  var $main_classes array The require classes file path.
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_MJPA_DIR_PATH_DATABASE_PLUGIN . $class ) ) {
						require_once YITH_MJPA_DIR_PATH_DATABASE_PLUGIN . $class;
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 **/
		public function init_classes() {
			YITH_MJPA_Post_Types::get_instance();
			YITH_MJPA_Shortcode::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_PS_Admin::get_instance();

			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PS_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_MJPA_Database_Plugin_Structure instance
 *
 * @return YITH_MJPA_Database_Plugin_Structure
 */
if ( ! function_exists( 'yith_mjpa_database_plugin_structure' ) ) {
	/**
	 * Yith_mjpa_database_plugin_structure
	 *
	 * @return instance
	 */
	function yith_mjpa_database_plugin_structure() {
		return YITH_MJPA_Database_Plugin_Structure::get_instance();
	}
}
