<?php
	$loop_addon = array_pop( $args );

?>

<div class="mjpa_hidden yith_mjpa_ghost_addon wrapp-accordion addon-variation_<?php echo( esc_html( $loop_addon ) ); ?> ">
		<div class="accordion-title-variation addon-variation_<?php echo( esc_html( $loop_addon ) ); ?> js-accordion-title">
			<div class="wrap-title-checkbox">
				<h4>Accordion Title 1</h4>
				<div class="class_checkbox_wrap">
					<span class="class_checkbox_enable"><input type="checkbox" id="_checkbox_enable" name="_checkbox_enable" class="_checkbox_enable"> Enable/Disable</span>
				</div>
			</div>
		</div>
		<div class="options-group accordion-content addon-variation_<?php echo( esc_html( $loop_addon ) ); ?>">
			<?php YITH_MJPA_Framework_Printer::yith_mjpa_print_input( $args ); ?>
			<div class="delete_addon_button_wrap">
				<button class="delete_addon_button button button-primary button-large" onclick="delete_addon(event)">Eliminar Addon</button>
			</div>
		</div>
</div>
