jQuery(document).ready(function ($) {

    /*let value_data_panel = () => {
        value_array = null;
        var link = ($(location).attr("href"));
        slug = link.substr(29);
        $.ajax({
            async: false,
            type: "post",
            url: dcms_vars.ajaxurl, // Pon aquí tu URL
            data: {
                action: "yith_mjpa_call_ajax",
                product_name: slug
            },
            error: function (response) {
                console.log(response)
            },
            success: function (response) {
                value_array = response;
            }
        })
        return value_array;
    }
    price = value_data_panel().price;
    free_characters = value_data_panel().free_characters;
    type_price = value_data_panel().type_of_pay;
*/
    price = dcms_vars.price_product;
    free_characters = dcms_vars.free_characters;
    type_price = dcms_vars.price_type;
    $('#yith_mjpa_input_personalize_id').on('input', function () {
        switch (type_price) {
            case 'free':
                $('.yith_mjpa_extra_price_personalize').html('0.00$')
                break;
            case 'fixed_price':
                $('.yith_mjpa_extra_price_personalize').html('+' + parseInt(price) + '.00$')
                break;

            case 'price_per_character':
                if (free_characters != 0) {
                    if ($('#yith_mjpa_input_personalize_id').val().length <= free_characters) {
                        $('.yith_mjpa_extra_price_personalize').html('0.00$')
                    }
                    else {
                        $('.yith_mjpa_extra_price_personalize').html('+' + parseInt(price) * (($('#yith_mjpa_input_personalize_id').val().length) - free_characters) + '.00$')
                    }
                }
                break;

            default:
                break;
        }

    });
});
