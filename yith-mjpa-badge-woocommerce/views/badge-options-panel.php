<div class="wrap">
	<h1><?php esc_html( 'Settings' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'mjpa-badge-options-page' );
			do_settings_sections( 'mjpa-badge-options-page' );
			submit_button();
		?>

	</form>
</div>
