<?php
	global $post;
?>
<div class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>">
	<p class="form-field ">
	<label for="<?php echo( esc_html( $args['id_field'] ) ); ?>"><?php echo( esc_html( $args['label_field'] ) ); ?></label>
		<select name="<?php echo( esc_html( $args['id_field'] ) ); ?>" id="<?php echo( esc_html( $args['id_field'] ) ); ?>" class="<?php echo( esc_html( $args['class_field'] ) ); ?>" onchange="show_hide_price_fields_select(this)">
		<?php
		foreach ( $args['options_value'] as $key => $value ) {
			if ( $value === $args['default_input'] ) {
				?>
			<option value="<?php echo( esc_html( $value ) ); ?>" selected><?php echo( esc_html( $value ) ); ?></option>
				<?php
			} else {
				?>
				<option value="<?php echo( esc_html( $value ) ); ?>"><?php echo( esc_html( $value ) ); ?></option>
				<?php
			}
		}
		?>
		</select>
	</p>
</div>
