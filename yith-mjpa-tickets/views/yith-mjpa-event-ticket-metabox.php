<?php

	$date_meta_box    = 'Purchased Date: ' . $args[0]->post_date;
	$name_meta_box    = 'Name: ' . get_post_meta( $args[0]->ID )['yith_mjpa_name'][0];
	$surname_meta_box = 'Surname: ' . get_post_meta( $args[0]->ID )['yith_mjpa_surname'][0];
?>
<p><?php echo( esc_html( $date_meta_box ) ); ?></p>
<h2><strong>Field details</strong></h2>
<p><?php echo( esc_html( $name_meta_box ) ); ?></p>
<p><?php echo( esc_html( $surname_meta_box ) ); ?></p>
