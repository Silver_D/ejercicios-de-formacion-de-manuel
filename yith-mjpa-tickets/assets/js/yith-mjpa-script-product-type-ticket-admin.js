jQuery(document).ready(function ($) {

    jQuery('.yith_mjpa_checkbox_admin_meta_box').on('click', function () {
        let yith_mjpa_post_id_js = $(this).attr('id')
        jQuery.ajax({
            type: "POST",
            url: attr_ajax.ajax_url, // Pon aquí tu URL
            data: {
                action: "variation",
                yith_mjpa_post_id: yith_mjpa_post_id_js
            },
            error: function (response) {
                console.log(response);
            },
            success: function (response) {
                if ($('#' + yith_mjpa_post_id_js)[0].classList.contains('yith_mjpa_not_checked')) {
                    $('#' + yith_mjpa_post_id_js)[0].classList.add('yith_mjpa_checked')
                    $('#' + yith_mjpa_post_id_js)[0].classList.remove('yith_mjpa_not_checked')
                } else {
                    if ($('#' + yith_mjpa_post_id_js)[0].classList.contains('yith_mjpa_checked')) {
                        $('#' + yith_mjpa_post_id_js)[0].classList.add('yith_mjpa_not_checked')
                        $('#' + yith_mjpa_post_id_js)[0].classList.remove('yith_mjpa_checked')
                    }
                }

            }
        })

    });
});