<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'YITH_MP_Database' ) ) {
	/**
	 * YITH_MP_Database
	 */
	class YITH_MP_Database {

		/**
		 * Main version
		 *
		 * @var version
		 * @since 1.0
		 * @access private
		 */
		public static $version = '1.0.0';
		/**
		 * Bdd Name
		 *
		 * @var auction_table
		 * @since 1.0
		 */
		public static $auction_table = 'yith_raffle';
		/**
		 * Main instance
		 *
		 * @var auction_table
		 * @since 1.0
		 */
		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MP_Database Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MP_Database constructor.
		 */
		private function __construct() {}
		/**
		 * Install
		 *
		 * @return void
		 */
		public static function install() {
			self::create_db_table();
		}
		/**
		 * Create_db_table
		 *
		 * @param  bool $force if forced.
		 * @return void
		 */
		public static function create_db_table( $force = false ) {
			global $wpdb;
			$current_version = get_option( 'yith_db_version' );
			if ( $force || $current_version !== self::$version ) {
				$wpdb->hide_errors();

				$table_name      = $wpdb->prefix . self::$auction_table;
				$charset_collate = $wpdb->get_charset_collate();

				$sql
				= "CREATE TABLE $table_name (
                   `id` bigint(20) NOT NULL AUTO_INCREMENT,
                   `user_id` bigint(20) NOT NULL,
                   `name` varchar(255) NOT NULL,
                   `last_name` varchar(255) NOT NULL,
                   `email` varchar(255) NOT NULL ,
                   PRIMARY KEY (id)
                   ) $charset_collate;";

				if ( ! function_exists( 'dbDelta' ) ) {
					require_once ABSPATH . 'wp-admin/includes/upgrade.php';
				}
				dbDelta( $sql );
				update_option( 'yith_db_version', self::$version );
			}
		}


	}
}
