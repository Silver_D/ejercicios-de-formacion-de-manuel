<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}


/**
 * YITH_MPJA_Wp_List_Table
 */
class YITH_MJPA_Wp_List_Table extends WP_List_Table {
	/**
	 * Main Instance
	 *
	 * @var YITH_MJPA_Wp_List_Table
	 * @since 1.0
	 * @access private
	 */

	private static $instance;

	/**
	 * Main plugin Instance
	 *
	 * @return YITH_MJPA_Wp_List_Table Main instance
	 * @author Manuel Jesús Peraza Alonso
	 */
	public static function get_instance() {
		return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
	}
	/**
	 * __construct
	 *
	 * @return void
	 */
	private function __construct() {

		parent::__construct(
			array(
				'singular' => 'user',
				'plural'   => 'users',
				'ajax'     => false,
			)
		);
	}
	/**
	 * Get_columns
	 *
	 * @return array void
	 */
	public function get_columns() {
		$columns = array(
			'name'          => 'Name',
			'last_name'     => 'Last Name',
			'email'         => 'Email',
			'delete_column' => 'Delete',
		);
		return $columns;
	}
	/**
	 * Prepare_items
	 *
	 * @return void
	 */
	public function prepare_items() {
		$columns               = $this->get_columns();
		$hidden                = array();
		$sortable              = array();
		$this->_column_headers = array( $columns, $hidden, $sortable );
		$this->process_bulk_action();
		$this->items  = $this->get_user_list();
		$this->screen = get_current_screen();

	}
	/**
	 * Column_default
	 *
	 * @param  mixed $item item.
	 * @param  mixed $column_name column name.
	 * @return item void
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			// Build delete row action.
			case 'name':
			case 'last_name':
			case 'email':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ); // Show the whole array for troubleshooting purposes
		}
	}

	/**
	 * Get_user_list
	 *
	 * @return user_list void
	 */
	public function get_user_list() {
		global $wpdb;
		$database_user_list = $wpdb->get_results( 'SELECT * FROM wp_yith_raffle ORDER BY id DESC', 'ARRAY_A' );
		return $database_user_list;

	}
	/**
	 * Column_delete_column
	 *
	 * @param  mixed $item item.
	 * @return html void
	 */
	public function column_delete_column( $item ) {
		if ( isset( $_REQUEST['page'] ) ) {
			$actions = array(
				'delete' => sprintf( "<a href='?page=%s&action=%s&id=%s'> Delete </a>", sanitize_text_field( wp_unslash( $_REQUEST['page'] ) ), 'delete', $item['id'] ),
			);
		}
		return ( sprintf( "<button type='button'>" . $this->row_actions( $actions ) . '</button>' ) );

	}
	/**
	 * Process_bulk_action
	 *
	 * @return void
	 */
	public function process_bulk_action() {
		if ( 'delete' === $this->current_action() && isset( $_REQUEST['id'] ) ) {
			$this->delete_user( sanitize_text_field( wp_unslash( $_REQUEST['id'] ) ) );
		}
	}
	/**
	 * Delete_user
	 *
	 * @param  mixed $id id user.
	 * @return void
	 */
	public function delete_user( $id ) {
		global $wpdb;
		$tablename = $wpdb->prefix . 'wp_yith_raffle'; // geting our table name with prefix
		$result    = $wpdb->query( "DELETE FROM wp_yith_raffle WHERE id= $id" );
	}

}
