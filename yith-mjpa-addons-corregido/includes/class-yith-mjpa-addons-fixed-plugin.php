<?php
/*
 * This file belongs to the YITH MJPA Addons fixed.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Addons_Fixed' ) ) {
	class YITH_MJPA_Addons_Fixed {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Addons_Fixed
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_MJPA_Plugin_Ticket_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_MJPA_Plugin_Ticket_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Addons_Fixed Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6
		}

		/**
		 * YITH_MJPA_Addons_Fixed constructor.
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_mjpa_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						 // 'includes/class-yith-mjpa-ajax.php',
						 // 'includes/class-yith-mjpa-compatibility.php',
						 // 'includes/class-yith-mjpa-other-class.php',
					),
					'admin'    => array(
						'includes/class-yith-mjpa-admin-addons.php',
						'includes/class-yith-mjpa-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-mjpa-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/

			// Finally call the init function
			$this->init();

		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_MJPA_DIR_PATH . $class ) ) {
						require_once YITH_MJPA_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 **/
		public function init_classes() {
			// $this->function = YITH_MJPA_Other_Class::get_instance();
			// $this->ajax = YITH_MJPA_Ajax::get_instance();
			// $this->compatibility = YITH_MJPA_Compatibility::get_instance();
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_MJPA_Admin::get_instance();
				YITH_MJPA_Admin_Addons_Fixed::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_MJPA_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_MJPA_Addons_Fixed instance
 *
 * @return YITH_MJPA_Addons_Fixed
 */
if ( ! function_exists( 'yith_mjpa_addons_fixed' ) ) {
	function yith_mjpa_addons_fixed() {
		return YITH_MJPA_Addons_Fixed::get_instance();
	}
}
