<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 * * //phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 */

?>
<?php
$nonce = wp_create_nonce( 'get_message' );

if ( isset( $nonce ) && wp_verify_nonce( $nonce, 'get_message' ) ) {
	?>
	<div class="wrapi">
	<div>
	<ul>
	<?php
	if ( empty( $args_users ) ) {
		foreach ( $args as $key => $value ) {
			echo( '<li>' . esc_html( $value['name'] ) . '</li>' );
		}
	} if ( ! empty( $args_users ) ) {
		foreach ( $args_users as $key => $value ) {
			echo( '<li>' . esc_html( $value['name'] ) . '</li>' );
		}
	}

	?>
	</ul>
	</div>
	<div class="wrap">
	<h1><?php esc_html_e( 'Register Form:', 'yith-mjpa-database-plugin' ); ?></h1>
	<?php

	if ( isset( $_GET['errormsg'] ) ) {
		$errormsg = sanitize_text_field( wp_unslash( $_GET['errormsg'] ) );

		?>
	<div>
		<p class="yith_mjpa_error"><?php echo esc_html( $errormsg ); ?></p>
	</div>
	<?php }; ?>
	<?php
	if ( isset( $_GET['success'] ) ) {
		$successmsg = sanitize_text_field( wp_unslash( $_GET['success'] ) );
		?>
	<div>
		<p class="yith_mjpa_success"><?php echo esc_html( $successmsg ); ?></p>
	</div>
	<?php } ?>
	<form class="yith_mjpa_form_class" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">		
		<label for="txtname"><?php esc_html_e( 'Name:', 'yith-mjpa-database-plugin' ); ?></label>
		<input type="text" id="txtname" name="txtname">
		<label for="txtlastname"><?php esc_html_e( 'Last Name:', 'yith-mjpa-database-plugin' ); ?></label>
		<input type="text" id="txtlastname" name="txtlastname">
		<?php
		if ( isset( $args[0] ) && ! isset( $args[1] ) ) {
			?>
			<label for="txtemail"><?php esc_html_e( 'Email:', 'yith-mjpa-database-plugin' ); ?></label>
			<input class="hide_element" type="text" id="txtemail" name="txtemail" value=<?php echo esc_html( $args[0] ); ?>>
		<?php } ?>
		<?php
		if ( isset( $args[0] ) && isset( $args[1] ) ) {
			?>
			<label for="txtemail"><?php esc_html_e( 'Email:', 'yith-mjpa-database-plugin' ); ?></label>
			<input type="text" id="txtemail" name="txtemail">
		<?php } ?>
		<label><input type="checkbox" id="check_box_form_id" value="check_box_form" onchange="document.getElementById('yith_mjpa_send_form').disabled = !this.checked;">
		<?php
		esc_html_e(
			'I want to participate in the tournament',
			'yith-mjpa-database-plugin'
		);
		?>
		</label>
		<input type="submit" value="<?php echo( esc_html_e( 'Send', 'yith-mjpa-database-plugin' ) ); ?>" id="yith_mjpa_send_form" disabled>
		<input type="hidden" name="action" value="contactform">
		<?php wp_nonce_field( 'yith_mp_form', 'formcontrol' ); ?>
	</form>
</div>
</div>
	<?php } ?>
