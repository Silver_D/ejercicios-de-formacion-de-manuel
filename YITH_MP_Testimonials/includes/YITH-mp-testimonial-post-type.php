<?php
// Register Custom Post Type
function yith_testimony_post_type() {

	$labels = array(
		'name'               => __( 'testimonials','custom-post-type-lenguage' ),
		'singular_name'      => __( 'testimony','custom-post-type-lenguage' ),
		'add_new'            => __( 'Add New testimony','custom-post-type-lenguage' ),
		'add_new_item'       => __( 'Add New testimony','custom-post-type-lenguage' ),
		'edit_item'          => __( 'Edit testimony','custom-post-type-lenguage' ),
		'new_item'           => __( 'Add New testimony','custom-post-type-lenguage' ),
		'view_item'          => __( 'View testimony','custom-post-type-lenguage'),
		'search_items'       => __( 'Search testimony','custom-post-type-lenguage' ),
		'not_found'          => __( 'No testimony found','custom-post-type-lenguage' ),
		'not_found_in_trash' => __( 'No testimonials found in trash','custom-post-type-lenguage' )
	);

	$args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true, 
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-calendar-alt',
        'supports' => array('title','editor','author', 'thumbnail'),
        'register_meta_box_cb' => 'yith_mp_wpt_metabox_info',
	);

    register_post_type( 'yith_testimonials', $args );


}
add_action( 'init', 'yith_testimony_post_type' );

function yith_mp_wpt_metabox_info() {

    add_meta_box(
		'wpt_info',
		'Info',
		'wpt_mp_print_meta_info',
		'yith_testimonials',
		'normal',
		'high',array("info","company","url_company","email","rating","vip","badge","text_badge","color")
    );
}

function wpt_mp_print_meta_info($post,$callbackargs) {

	wp_nonce_field( basename( __FILE__ ), 'info_fields' );

	$info = get_post_meta( $post->ID, $callbackargs['args'][0], true );
	$company = get_post_meta( $post->ID, $callbackargs['args'][1], true );
	$url = get_post_meta( $post->ID, $callbackargs['args'][2], true );
	$email = get_post_meta( $post->ID, $callbackargs['args'][3], true );
	$rating = get_post_meta( $post->ID, $callbackargs['args'][4], true );
	$vip = get_post_meta( $post->ID, $callbackargs['args'][5], true );
	$badge = get_post_meta( $post->ID, $callbackargs['args'][6], true );
	$text_badge =get_post_meta( $post->ID, $callbackargs['args'][7], true );
	$color =get_post_meta( $post->ID, $callbackargs['args'][8], true );
	


	echo "<h1>Rol</h1>";
	echo '<input type="text" name=' . $callbackargs['args'][0] . ' value="' . esc_textarea( $info )  . '" class="widefat">';
	echo "<h1>Company</h1>";
	echo '<input type="text" name=' . $callbackargs['args'][1] . ' value="' . esc_textarea( $company )  . '" class="widefat">';
	echo "<h1>Url Company</h1>";
	echo '<input type="text" name=' . $callbackargs['args'][2] . ' value="' . esc_textarea( $url )  . '" class="widefat">';
	echo "<h1>Email</h1>";
	echo '<input type="text" name=' . $callbackargs['args'][3] . ' value="' . esc_textarea( $email )  . '" class="widefat">';
	echo "<h1>Rating</h1>";
	echo ("<div class='container'>
			<div class='stars__wrapper'>
			<a href='#' class='star star1'>★</a>
			<a href='#' class='star star2'>★</a>
			<a href='#' class='star star3'>★</a>
			<a href='#' class='star star4'>★</a>
			<a href='#' class='star star5'>★</a>
			</div>
			<input type='text' class='invi' id='rating' value='" . esc_textarea( $rating ) . "' name='" . $callbackargs['args'][4] .  "'>
			<script>
			window.onload = stars_loaded($rating);
		    </script>
		 </div>");
		 echo "<h1>VIP</h1>";

		if ($vip=='affirmative') {
			echo ("<input type='radio' id='affirmative' name='" . $callbackargs['args'][5] .  "' value='affirmative' checked>
			<label for='affirmative'>Si</label><br>
			<input type='radio' id='negative' name='" . $callbackargs['args'][5] .  "' value='negative' >
		   <label for='negative'>No</label><br>");
		}
		else{
			echo ("<input type='radio' id='affirmative' name='" . $callbackargs['args'][5] .  "' value='affirmative' >
			<label for='affirmative'>Si</label><br>
			<input type='radio' id='negative' name='" . $callbackargs['args'][5] .  "' value='negative' checked>
		   <label for='negative'>No</label><br>");
		}

		echo "<h1>Badge</h1>";
		if ($badge=='affirmative_badge') {
			echo ("<input type='radio' id='affirmative_badge' name='" . $callbackargs['args'][6] .  "' value='affirmative_badge' checked>
			<label for='affirmative'>Si</label><br>
			<input type='radio' id='negative_badge' name='" . $callbackargs['args'][6] .  "' value='negative_badge' >
		   <label for='negative'>No</label><br>");
		}
		else{
			echo ("<input type='radio' id='affirmative_badge' name='" . $callbackargs['args'][6] .  "' value='affirmative_badge' >
			<label for='affirmative'>Si</label><br>
			<input type='radio' id='negative_badge' name='" . $callbackargs['args'][6] .  "' value='negative_badge' checked>
		   <label for='negative'>No</label><br>");
		}

		if ($badge=='affirmative_badge') {
			echo "<h1>Text Badge</h1>";
			echo ("
			<input type='text' name='" . $callbackargs['args'][7] . "' value='" . esc_textarea( $text_badge )  . "' class='widefat'>
			");
			echo "<h1>Color</h1>";
			echo ("<input type='text' name='" . $callbackargs['args'][8] . "' value='" . esc_textarea( $color ) . "' class='my-color-field' data-default-color='#effeff' />'");
		}


	//error_log ( print_r(  $aux, true ) );

}
function wpt_mp_save_meta_info( $post_id, $post ) {
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['info'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['company'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['url_company'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['email'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['rating'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	
	if ( ! isset( $_POST['vip'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['badge'] ) || ! wp_verify_nonce( $_POST['info_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}

	$info_meta['info'] = esc_textarea( $_POST['info'] );
	$info_meta['company'] = esc_textarea( $_POST['company'] );
	$info_meta['url_company'] = esc_textarea( $_POST['url_company'] );
	$info_meta['email'] = esc_textarea( $_POST['email'] );
	$info_meta['rating'] = esc_textarea( $_POST['rating'] );
	$info_meta['vip'] = esc_textarea( $_POST['vip'] );
	$info_meta['badge'] = esc_textarea( $_POST['badge'] );


	if ($_POST['badge']=='affirmative_badge') {
		if (!isset($_POST['text_badge'])) {
			$_POST['text_badge']="";
		}
		else{
			$info_meta['text_badge'] =  esc_textarea( $_POST['text_badge'] );
		}
		if (!isset($_POST['color'])) {
			$_POST['color']="#effeff";
		}
		else{
			$info_meta['color'] =  esc_textarea( $_POST['color'] );
		}
	}
	foreach ( $info_meta as $key => $value ) :

		if ( 'revision' === $post->post_type ) {
			return;
		}
		
		if ( get_post_meta( $post_id, $key, false ) ) {
			update_post_meta( $post_id, $key, $value );
		} else {
			add_post_meta( $post_id, $key, $value);
		}
		if ( ! $value ) {
			delete_post_meta( $post_id, $key );
		}
	endforeach;

}
add_action( 'save_post', 'wpt_mp_save_meta_info', 1, 2 );