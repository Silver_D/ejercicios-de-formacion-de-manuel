jQuery(document).ready(function ($) {


    //buscamos todos los campos form field, de estos sacamos los data.
    /*$('.yith_mjpa_form_field_selector').each(function () {
        if ($(this).data('depId')) {
            let mjpa_depID = $(this).data('depId')
            let mjpa_depTarget = $(this).data('depTarget')
            let mjpa_depType = $(this).data('depType')
            let mjpa_depValue = $(this).data('depValue')

            $('#' + mjpa_depTarget).on('change', function (e) {
                let targetValue = $('#' + mjpa_depTarget).val();
                if (targetValue === mjpa_depValue) {
                    console.log(mjpa_depType)
                    switch (mjpa_depType) {
                        case 'hide':
                            $('#' + mjpa_depID).parents('.yith_mjpa_form_field_selector').hide()
                            break;

                        default:
                            break;
                    }
                } else {
                    $('#' + mjpa_depID).parents('.yith_mjpa_form_field_selector').show()
                }
            })
        }
    })*/
    $('.yith_mjpa_form_field_selector').each(function () {
        if ($(this).data('depId')) {
            let mjpa_depID = $(this).data('depId')
            let mjpa_depTarget = $(this).data('depTarget')
            let mjpa_depType = $(this).data('depType')
            let mjpa_depValue = $(this).data('depValue')

            if (mjpa_depTarget && mjpa_depID && mjpa_depValue && mjpa_depType) {
                let arrDepTarget = mjpa_depTarget.split(',')
                let arrDepValue = mjpa_depValue.split(',')

                arrDepTarget.forEach(targetId => {
                    $('#' + targetId).on('change', function () {
                        let targetValue = $('#' + targetId).val()

                        if (arrDepValue.find(value => value === targetValue)) {
                            switch (mjpa_depType) {
                                case 'hide':
                                    $('#' + mjpa_depID).parents('.yith_mjpa_form_field_selector').hide()
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            $('#' + mjpa_depID).parents('.yith_mjpa_form_field_selector').show()
                        }
                    })
                });
            }
        }
    })
});
