<?php
/*Incluidos dentro de functions.php*/

class primer_widget extends WP_Widget {
    //declaración y setup del widget
    public function __construct(){
        $widget_ops=array(
            'classname' => 'mi_primer_widget',
            'description' => 'Es mi primer widget'
        );
        parent::__construct('first_widget', 'El mejor widget', $widget_ops);
    }

    //backend display del widget
    function form($instance){
    $instance = wp_parse_args( (array) $instance, array('nombre'=> "defecto",
                                                        'title'=> null,
                                                        'apellidos' => "defecto",
                                                        'direccion'=>"defecto",
                                                        'telefono'=>"defecto"));
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = 'New title';
        }

        $nombre=$instance['nombre'];
        $apellidos=$instance['apellidos'];
        $direccion=$instance['direccion'];
        $telefono=$instance['telefono'];

        ?>
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        <label for="<?php echo $this->get_field_id( 'nombre' ); ?>"><?php _e( 'Nombre:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'nombre' ); ?>" name="<?php echo $this->get_field_name( 'nombre' ); ?>" type="text" value="<?php echo esc_attr( $nombre ); ?>" />
        </p>
        <label for="<?php echo $this->get_field_id( 'apellidos' ); ?>"><?php _e( 'Apellidos:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'apellidos' ); ?>" name="<?php echo $this->get_field_name( 'apellidos' ); ?>" type="text" value="<?php echo esc_attr( $apellidos ); ?>" />
        </p>
        <label for="<?php echo $this->get_field_id( 'direccion' ); ?>"><?php _e( 'Direccion:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'direccion' ); ?>" name="<?php echo $this->get_field_name( 'direccion' ); ?>" type="text" value="<?php echo esc_attr( $direccion ); ?>" />
        </p>
        <label for="<?php echo $this->get_field_id( 'telefono' ); ?>"><?php _e( 'Telefono:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'telefono' ); ?>" name="<?php echo $this->get_field_name( 'telefono' ); ?>" type="text" value="<?php echo esc_attr( $telefono ); ?>" />
        </p>
        <?php 
    }

    //frontend display del widget
    function widget($args,$instance){
        //error_log ( print_r(  $instance, true ) );
        $title = apply_filters( 'widget_title', $instance['title'] );
  
        // before and after widget arguments are defined by themes
        if ( ! empty( $title ) )

        
        // This is where you run the code and display the output
        echo 'Hello, World!';
        echo  "<h1>" ;
        echo "$title</h1>";
        echo "<p>"; 
        echo $instance['nombre']."</p>";
        echo "<p>" . $instance['apellidos']. "</p>";
        echo "<p>" .$instance['direccion']. "</p>";
        echo "<p>" . $instance['telefono'] . "</p>";
    }

    //permite guardar 
    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['nombre'] = ( ! empty( $new_instance['nombre'] ) ) ? strip_tags( $new_instance['nombre'] ) : '';
        $instance['apellidos'] = ( ! empty( $new_instance['apellidos'] ) ) ? strip_tags( $new_instance['apellidos'] ) : '';
        $instance['direccion'] = ( ! empty( $new_instance['direccion'] ) ) ? strip_tags( $new_instance['direccion'] ) : '';
        $instance['telefono'] = ( ! empty( $new_instance['telefono'] ) ) ? strip_tags( $new_instance['telefono'] ) : '';
        return $instance;
         
    }

 

}

add_action('widgets_init',function(){
    register_widget('primer_widget');
});
