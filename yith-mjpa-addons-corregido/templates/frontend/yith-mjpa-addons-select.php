<?php
$name_id_field        = 'select_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];

?>

<div>
	<p class="yith_mjpa_description_addon"><?php print_addons_value( $args[0]['textarea_field'] ); ?></p>
	<select class="yith_mjpa_input_select yith_price_addon" name="<?php echo ( esc_html( $name_id_field ) ); ?>" id="<?php echo ( esc_html( $name_id_field ) ); ?>">
	<?php
	foreach ( $args[0]['name_field_option'] as $key => $value ) {
		echo( '<option value=' . esc_html( $value ) . '>' . esc_html( $value ) . '</option>' );
	}
	?>
	</select>
	<?php
	if ( 'free' === $args[0]['radio_field'] ) {
		echo( '<p class="yith_mjpa_price_select yith_price_addon_value">+0.00$</p>' );
	} else {
		echo( '<p class="yith_mjpa_price_select yith_price_addon_value">+' . $args[0]['price_field_option'][0] . '.00$</p>' );
	}
	?>
</div>
