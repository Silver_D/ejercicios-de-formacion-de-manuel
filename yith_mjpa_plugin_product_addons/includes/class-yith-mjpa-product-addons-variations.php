<?php
/**
 * This file belongs to the YITH MJPA Plugin Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Product_Addons_Variations' ) ) {
	/**
	 * YITH_MJPA_Product_Addons_Variations
	 */
	class YITH_MJPA_Product_Addons_Variations {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Product_Addons_Variations
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Product_Addons_Variations Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'yith_mjpa_add_addons_variations' ), 10, 3 );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'yith_mjpa_save_addons_variations' ) );

		}
		/**
		 * Add_input_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {
			wp_register_style( 'yith-mjpa-style-addons-variations', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style-addons-variations.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style-addons-variations' );
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {

			wp_register_script( 'yith_mjpa_product_addons_variations', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-product-addons-variations.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith_mjpa_product_addons_variations' );
		}

		/**
		 * Yith_mjpa_add_addons_variations
		 *
		 * @return void
		 */
		public function yith_mjpa_add_addons_variations( $loop, $variation_data, $variation ) {
			$arr_attr_input_types = array(
				'name_field'             => array(
					'type'                 => 'text',
					'id_field'             => 'id_name_field_variation',
					'class_field'          => 'class_name_field_variation',
					'label_field'          => 'Name Field',
					'name_field'           => 'id_name_field_variation',
					'class_field_show'     => '',
					'default_text_value'   => '',
					'default_color_picker' => '',
				),
				'textarea_input_type'    => array(
					'type'             => 'textarea',
					'id_field'         => 'id_text_area_variation',
					'default_input'    => '',
					'class_field'      => 'short class_textarea_variation',
					'label_field'      => 'Text area',
					'name_field'       => 'id_text_area_variation',
					'class_field_show' => '',
				),
				'select_input_type'      => array(
					'type'             => 'select-variation',
					'id_field'         => 'id_select_variation',
					'class_field'      => 'short class_select_variation',
					'label_field'      => 'Select Label',
					'default_input'    => '',
					'name_field'       => 'id_select_variation',
					'class_field_show' => '',
					'options_value'    => array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' ),
				),
				'option_select'          => array(
					'type'                    => 'option',
					'id_field'                => 'id_name_option_field_variation',
					'default_text_value'      => '',
					'id_field_option_name'    => 'id_option_select_variation',
					'type_option'             => 'text',
					'class_field'             => 'class_option_select block',
					'class_field_price'       => 'class_option_price_variation class_option_select_variation',
					'class_field_option'      => 'class_option_option_variation class_option_select_variation',
					'class_field_wrap_option' => 'class_flex',
					'name_field_option'       => 'id_option_select_variation_',
					'id_field_price'          => 'id_price_select_variation',
					'name_field_price'        => 'id_price_select_variation',
					'label_field_option'      => 'Name',
					'label_field_price'       => 'Price',
					'class_field_show'        => 'wrap_options_select_variation_ mjpa_hidden',
					'class_group_show'        => 'wrap-option_variation mjpa_hidden',
					'id_add_option'           => 'id_add_new_option_variation',
					'class_class_add_option'  => 'add_new_option_variation taxonomy-add-new',
					'options_stored'          => '',
					'price_stored'            => '',
					'group_option_class'      => 'group_variation',
				),
				'radio_input_type_price' => array(
					'type'             => 'radio-variation',
					'class_field'      => '',
					'class_field_show' => 'class_show_variation',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => '_free_option_variation',
							'name_field'       => '_radio_price_type_variation',
							'value_field'      => 'free',
							'class_field'      => 'class_radio_price_type_free_variation',
							'label_field'      => 'Free',
							'default'          => 'true',
							'class_field_show' => 'class_show_free_variation',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => '_fixed_option_variation',
							'name_field'       => '_radio_price_type_variation',
							'value_field'      => 'fixed_price',
							'label_field'      => 'Fixed Price',
							'class_field'      => 'class_radio_price_type_fixed_variation',
							'default'          => 'false',
							'class_field_show' => '',
						),
						'third'  => array(
							'type'             => 'radio',
							'id_field'         => '_price_per_character_option_variation',
							'name_field'       => '_radio_price_type_variation',
							'value_field'      => 'price_per_character',
							'label_field'      => 'Price Per Character',
							'default'          => 'false',
							'class_field'      => 'class_radio_price_type_character_variation',
							'class_field_show' => 'class_price_per_character_variation',
						),
					),
				),
				'price_field'            => array(
					'type'                 => 'text',
					'id_field'             => 'id_price_field_variation',
					'class_field'          => 'class_price_field_variation',
					'label_field'          => 'Price Field',
					'name_field'           => 'id_price_field_variation',
					'class_field_show'     => 'price_show_class_variation mjpa_hidden',
					'default_text_value'   => '',
					'default_color_picker' => '',
				),
				'onoff'                  => array(
					'type'             => 'onoff',
					'id_field'         => '_onoff_default_value_id_variation',
					'class_field'      => 'class_onoff_variation',
					'label_field'      => 'Default Enabled:',
					'name_field'       => '_onoff_default_value_id_variation',
					'default'          => 'on',
					'class_field_show' => 'onnoff_show_class_variation mjpa_hidden',
				),
				'free_characters'        => array(
					'type'             => 'number',
					'id_field'         => '_free_characters_input_variation',
					'class_field'      => 'class_free_characters_input_variation short',
					'label_field'      => 'Free Characters:',
					'name_field'       => '_free_characters_input_variation',
					'default'          => 0,
					'class_field_show' => 'free_characters_show_class_variation mjpa_hidden',
				),
			);

			array_push( $arr_attr_input_types, $loop );

			?>
			<tr>
				<td>
					<?php yith_mjpa_get_view( '/yith-mjpa-button-addons-variations.php', $arr_attr_input_types ); ?>
				</td>
			</tr>
			<?php
		}
		/**
		 * Yith_mjpa_save_addons_variations
		 *
		 * @return void
		 */
		public function yith_mjpa_save_addons_variations() {
			error_log( print_r( 'Se ejecuta yith_mjpa_save_addons_variations', true ) );
		}
	}
}
