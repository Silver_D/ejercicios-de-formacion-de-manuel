jQuery(document).ready(function ($) {
    //id_option_select_variation_0_0
    $(function () {
        button_click_function = function (event) {
            event.preventDefault();
            target_clone = $(event.target).parent().next().children(".yith_mjpa_ghost_addon");
            target_append = $(event.target).parent().next();
            variation_number = $(event.target).attr('id').split('_')[5]
            index_addon_on_variaton = parseInt($('#_index_hidden_variation_' + variation_number).val());
            jQuery(function ($) {
                yith_mjpa_clone_node_base_addon_variation(target_clone, target_append);
                yith_mjpa_change_names_title_addons();
                yith_mjpa_let_accordion_addon_variation();
                yith_mjpa_add_option_button();
            });
            $('#_index_hidden_variation_' + variation_number).val(index_addon_on_variaton + 1)
        }
        yith_mjpa_clone_node_base_addon_variation = function (target_clone, target_append) {
            let clone = target_clone.clone();
            clone.removeClass("yith_mjpa_ghost_addon");
            clone.removeClass("mjpa_hidden");
            clone.children(".options-group").addClass("addon_variation_" + variation_number + '_' + index_addon_on_variaton)
            clone.children(".options-group").removeClass("addon-variation_" + variation_number)
            clone.children(".options-group").children(".options_group.class_show_variation").addClass("variation_price_options_" + variation_number + '_' + index_addon_on_variaton)
            yith_mjpa_change_field_names_variation(clone);
            clone.children(".options-group").children(".options_group.class_show_variation").find("#_free_option_variation_" + variation_number + '_' + index_addon_on_variaton).prop('checked', true);
            target_append.append(clone);
        }
        yith_mjpa_let_accordion_addon_variation = function () {
            $('.accordion-title-variation.js-accordion-title.addon-variation_' + variation_number + '_' + index_addon_on_variaton).on('click', function () {
                $(this).next().slideToggle(200);
                $(this).toggleClass('open', 200);
            });
        }
        yith_mjpa_change_names_title_addons = function () {
            $('.accordion-title-variation.js-accordion-title.addon-variation_' + variation_number).last().addClass("addon-variation_" + variation_number + '_' + index_addon_on_variaton)
            $('.accordion-title-variation.js-accordion-title.addon-variation_' + variation_number).last().removeClass("addon-variation_" + variation_number)
        }
        yith_mjpa_change_field_names_variation = function (clone) {
            let fields = ["id_name_field_variation", "id_text_area_variation", "id_select_variation", "id_option_select_variation", "id_price_selec_variation", "id_add_new_optio_variation", "_free_option_variation", "_fixed_option_variation", "_price_per_character_option_variation", "id_price_field_variation", "_onoff_default_value_id_variation", "_free_characters_input_variation", "group_variation", "id_add_new_option_variation"];
            fields.forEach(element => {
                aux_id = clone.children(".addon_variation_" + variation_number + '_' + index_addon_on_variaton).find("#" + element).prop('id');
                aux_id = aux_id + '_' + variation_number + '_' + index_addon_on_variaton;
                clone.children(".addon_variation_" + variation_number + '_' + index_addon_on_variaton).find("#" + element).prop('id', aux_id)
                clone.children(".addon_variation_" + variation_number + '_' + index_addon_on_variaton).find("#" + element).prop('name', aux_id)
            });
        }
        yith_mjpa_add_option_button = function () {
            $("#id_add_new_option_variation_" + variation_number + '_' + index_addon_on_variaton).click(function (event) {
                event.preventDefault();
                let clone_option = $(event.target).parent().children(".wrap-option_variation").clone()
                //Crear id para no clonar varias opciones a la vez
                //clone_option.addClass('wrap-option_variation_' + variation_number + '_' + index_addon_on_variaton);
                //clone_option.removeClass('wrap-option_variation');
                clone_option.removeClass('mjpa_hidden')
                console.log(clone_option)
                clone_option.insertBefore($(event.target))
            });

        }
        show_hide_price_fields_select_variation = function (event) {
            let selectObject = $("#id_select_variation_" + variation_number + '_' + index_addon_on_variaton);
            if (($(selectObject).val() == 'text') || ($(selectObject).val() == 'textarea')) {
                $("#group_variation_" + variation_number + '_' + index_addon_on_variaton).addClass("mjpa_hidden")
                $("#_onoff_default_value_id_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().addClass("mjpa_hidden")
                $("#_price_per_character_option_variation_" + variation_number + '_' + index_addon_on_variaton).parent().removeClass("mjpa_hidden")
                if (($("#_fixed_option_variation_" + variation_number + '_' + index_addon_on_variaton).prop('checked') == true) || ($("#_price_per_character_option_variation_" + variation_number + '_' + index_addon_on_variaton).prop('checked') == true)) {
                    $("#id_price_field_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().removeClass("mjpa_hidden")
                    $("#_free_characters_input_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().removeClass("mjpa_hidden")
                } else {
                    $("#id_price_field_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().addClass("mjpa_hidden")
                    $("#_free_characters_input_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().addClass("mjpa_hidden")
                }
            } else {
                $("#_free_characters_input_variation_" + variation_number + '_' + index_addon_on_variaton).parent().addClass("mjpa_hidden")
                $("#_price_per_character_option_variation_" + variation_number + '_' + index_addon_on_variaton).parent().addClass("mjpa_hidden")
                if (($(selectObject).val() == 'onoff') || ($(selectObject).val() == 'checkbox')) {
                    $("#group_variation_" + variation_number + '_' + index_addon_on_variaton).addClass("mjpa_hidden")
                    $("#_onoff_default_value_id_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().removeClass("mjpa_hidden")
                    if (($("#_fixed_option_variation_" + variation_number + '_' + index_addon_on_variaton).prop('checked') == true)) {
                        $("#id_price_field_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().removeClass("mjpa_hidden")
                    } else {
                        $("#id_price_field_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().addClass("mjpa_hidden")
                    }
                }
                if (($(selectObject).val() == 'select') || ($(selectObject).val() == 'radio')) {
                    $("#_onoff_default_value_id_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().addClass("mjpa_hidden")
                    $("#group_variation_" + variation_number + '_' + index_addon_on_variaton).removeClass("mjpa_hidden")
                    if (($("#_fixed_option_variation_" + variation_number + '_' + index_addon_on_variaton).prop('checked') == true)) {
                        $("#id_price_field_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().removeClass("mjpa_hidden")
                    } else {
                        $("#id_price_field_variation_" + variation_number + '_' + index_addon_on_variaton).parent().parent().addClass("mjpa_hidden")
                    }
                }
            }
        }
    })
})
