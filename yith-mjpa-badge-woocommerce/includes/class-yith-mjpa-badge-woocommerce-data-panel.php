<?php
/**
 * This file belongs to the YITH_MJPA_BAGDE_WOOCOMMERCE_EXERCISE
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Data_Panel' ) ) {
	/**
	 * YITH_MJPA_Data_Panel
	 */
	class YITH_MJPA_Data_Panel {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Data_Panel
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Data_Panel Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_mjpa_add_new_tab' ) );
			add_filter( 'woocommerce_product_data_panels', array( $this, 'yith_mjpa_data_panel_format' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_mjpa_save_data_panel' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
		}
		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {

			wp_register_style( 'check', YITH_MJPA_DIR_ASSETS_CSS_URL . '/check.css', array(), true );
			wp_enqueue_style( 'check' );
			wp_enqueue_style( 'wp-color-picker' );

		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {

			wp_register_script( 'data_panel', YITH_MJPA_DIR_ASSETS_JS_URL . '/data-panel.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'data_panel' );
			wp_enqueue_script( 'mjpa-admin-color-picker', YITH_MJPA_DIR_ASSETS_JS_URL . '/mjpa-admin-color-picker.js', array( 'wp-color-picker' ), false, true );

		}
		/**
		 * Yith_mjpa_add_new_tab
		 *
		 * @param tabs mixed $tabs tabs.
		 * @return tabs void
		 */
		public function yith_mjpa_add_new_tab( $tabs ) {

			$tabs['Badge'] = array(
				'label'  => __( 'Badge', 'woocommerce' ),
				'target' => 'badge_options',
				'class'  => array( 'show_if_simple', 'show_if_variable' ),
			);

			return $tabs;

		}

		/**
		 * Yith_mjpa_data_panel_format
		 *
		 * @return void
		 */
		public function yith_mjpa_data_panel_format() {
			$printer              = YITH_MJPA_Framework_Printer::get_instance();
			$arr_attr_input_types = array(
				'checkbox_enable'        => array(
					'type'             => 'checkbox',
					'id_field'         => '_enable_plugin',
					'class_field'      => 'class_checkbox_enable',
					'label_field'      => 'Enable Plugin',
					'name_field'       => '_enable_plugin',
					'class_field_show' => '',
				),
				'text_input_type'        => array(
					'type'                 => 'text',
					'id_field'             => '_note_label',
					'class_field'          => 'class_note_label short',
					'label_field'          => 'Note label',
					'name_field'           => '_note_label',
					'class_field_show'     => 'class_show',
					'default_color_picker' => '',
					'default_text_value'   => 'Note',
				),
				'textarea_input_type'    => array(
					'type'             => 'textarea',
					'id_field'         => '_text_area',
					'class_field'      => 'short',
					'label_field'      => 'Text area',
					'name_field'       => '_text_area',
					'class_field_show' => 'class_show',
				),
				'radio_input_type'       => array(
					'type'             => 'radio',
					'id_field'         => '_radio_text_type',
					'label_field'      => 'Field Type',
					'name_field'       => '_radio_text_type',
					'class_field_show' => 'class_show',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => '_text_option',
							'name_field'       => '_radio_text_type',
							'class_field'      => '',
							'value_field'      => 'text',
							'label_field'      => 'Text',
							'default'          => 'true',
							'class_field_show' => '',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => '_textarea_option',
							'name_field'       => '_radio_text_type',
							'value_field'      => 'textarea',
							'class_field'      => '',
							'label_field'      => 'Textarea',
							'default'          => 'false',
							'class_field_show' => '',
						),
					),
				),
				'radio_input_type_price' => array(
					'type'             => 'radio',
					'class_field'      => '',
					'class_field_show' => 'class_show',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => '_free_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'free',
							'class_field'      => '',
							'label_field'      => 'Free',
							'default'          => 'true',
							'class_field_show' => 'class_show_free',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => '_fixed_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'fixed_price',
							'label_field'      => 'Fixed Price',
							'class_field'      => '',
							'default'          => 'false',
							'class_field_show' => '',
						),
						'third'  => array(
							'type'             => 'radio',
							'id_field'         => '_price_per_character_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'price_per_character',
							'label_field'      => 'Price Per Character',
							'default'          => 'false',
							'class_field'      => '',
							'class_field_show' => '',
						),
					),
				),
				'text_price'             => array(
					'type'                 => 'text',
					'id_field'             => '_price_text',
					'class_field'          => 'class_price_text short',
					'label_field'          => 'Price:',
					'name_field'           => '_price_text',
					'class_field_show'     => 'class_show class_show_price ',
					'default_color_picker' => '',
					'default_text_value'   => '',
				),
				'free_characters'        => array(
					'type'             => 'number',
					'id_field'         => '_free_characters_input',
					'class_field'      => 'class_free_characters_input short',
					'label_field'      => 'Free Characters:',
					'name_field'       => '_free_characters_input',
					'default'          => 0,
					'class_field_show' => 'class_show class_show_price',
				),
				'checkbox_show_badge'    => array(
					'type'             => 'checkbox',
					'id_field'         => '_allow_badge',
					'class_field'      => 'class_checkbox class_badge_enable',
					'label_field'      => 'Allow Badge',
					'name_field'       => '_allow_badge',
					'class_field_show' => 'class_show',
				),
				'badge_text'             => array(
					'type'                 => 'text',
					'id_field'             => '_badge_text',
					'class_field'          => 'class_badge_text short',
					'label_field'          => 'Badge Text:',
					'name_field'           => '_badge_text',
					'class_field_show'     => 'class_show class_show_badge',
					'default_color_picker' => '',
					'default_text_value'   => '',
				),
				'badge_background_color' => array(
					'type'                 => 'text',
					'id_field'             => '_badge_background_color',
					'class_field'          => 'short class_color_picker',
					'label_field'          => 'Badge Background Color:',
					'name_field'           => '_badge_background_color',
					'class_field_show'     => 'class_show class_show_badge',
					'default_color_picker' => '#007694',
					'default_text_value'   => '',
				),
				'badge_text_color'       => array(
					'type'                 => 'text',
					'id_field'             => '_badge_text_color',
					'class_field'          => 'short class_color_picker',
					'label_field'          => 'Badge Text Color:',
					'name_field'           => '_badge_text_color',
					'class_field_show'     => 'class_show class_show_badge',
					'default_color_picker' => '#ffffff',
					'default_text_value'   => '',
				),
			);

			$printer->yith_mjpa_print_input( $arr_attr_input_types );

		}
		/**
		 * Yith_mjpa_save_data_panel
		 *
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function yith_mjpa_save_data_panel( $post_id ) {
			$array_name_fields = array( '_enable_plugin', '_note_label', '_text_area', '_radio_text_type', '_radio_price_type', '_price_text', '_free_characters_input', '_allow_badge', '_badge_text', '_badge_background_color', '_badge_text_color' );

			foreach ( $array_name_fields as $key => $value ) {
				if ( isset( $_POST[ $value ] ) ) {
					update_post_meta( $post_id, $value, $_POST[ $value ] );
				} else {
					update_post_meta( $post_id, $value, 'off' );
				}
			}
		}

	}
}
