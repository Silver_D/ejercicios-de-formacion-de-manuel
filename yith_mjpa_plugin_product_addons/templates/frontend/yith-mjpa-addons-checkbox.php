<?php
$name_id_field        = 'checkbox_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];
?>

<div>
<?php

if ( 'on' === $args[0]['_onoff_default_value_id'] ) {
	echo( '<label><input checked class="yith_mjpa_input_checkbox yith_price_addon" type="checkbox" name="' . esc_html( $name_id_field ) . '" id="' . esc_html( $name_id_field ) . '" value="' . esc_html( $args[0]['id_text_area'] ) . '">' . esc_html( $args[0]['id_text_area'] ) . ' </label><br>' );
} else {
	echo( '<label><input class="yith_mjpa_input_checkbox yith_price_addon" type="checkbox" name="' . esc_html( $name_id_field ) . '" id="' . esc_html( $name_id_field ) . '" value="' . esc_html( $args[0]['id_text_area'] ) . '">' . esc_html( $args[0]['id_text_area'] ) . ' </label><br>' );
}


if ( 'fixed_price' === $args[0]['_radio_price_type'] && ( 'on' === $args[0]['_onoff_default_value_id'] ) ) {
	echo( '<p class="yith_mjpa_price_checkbox yith_price_addon_value">+' . esc_html( $args[0]['id_price_field'] ) . '.00$</p>' );
} else {
	echo( '<p class="yith_mjpa_price_checkbox yith_price_addon_value">+0.00$</p>' );
}
?>
</div>
