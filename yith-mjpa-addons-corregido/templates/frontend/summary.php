<div>
	<div><p>Price Totals</p></div>
	<div><p class="yith_mjpa_product_price" >Product price: +<?php echo( esc_html( $args[0] ) ); ?>.00$</p></div>
	<div><p class="yith_mjpa_total_addon_price" name="options_total_price">Additional options total: +0.00$</p></div>
	<div><p class="yith_mjpa_total_price">Total: +<?php echo( esc_html( $args[0] ) ); ?>.00$</p></div>
	<div class="yith_mjpa_hidden"><input type="text" class="yith_mjpa_hidden_input_price" name="hidden_input_price"></div>
	<div class="yith_mjpa_hidden"><input type="text" class="yith_mjpa_hidden_input_names" name="hidden_input_names"></div>
</div>
