<?php

add_action( 'init', 'bf_register_custom_post_type' );
/**
 * Registro un custom post type 'libro'.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function bf_register_custom_post_type() {
    /* Añado las etiquetas que aparecerán en el escritorio de WordPress */
	$labels = array(
		'name'               => _x( 'Libros', 'post type general name', 'text-domain' ),
		'singular_name'      => _x( 'Libro', 'post type singular name', 'text-domain' ),
		'menu_name'          => _x( 'Libros', 'admin menu', 'text-domain' ),
		'add_new'            => _x( 'Añadir nuevo', 'libro', 'text-domain' ),
		'add_new_item'       => __( 'Añadir nuevo libro', 'text-domain' ),
		'new_item'           => __( 'Nuevo libro', 'text-domain' ),
		'edit_item'          => __( 'Editar libro', 'text-domain' ),
		'view_item'          => __( 'Ver libro', 'text-domain' ),
		'all_items'          => __( 'Todos los libros', 'text-domain' ),
		'search_items'       => __( 'Buscar libros', 'text-domain' ),
		'not_found'          => __( 'No hay libros.', 'text-domain' ),
		'not_found_in_trash' => __( 'No hay libros en la papelera.', 'text-domain' )
	);

    /* Configuro el comportamiento y funcionalidades del nuevo custom post type */
	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Descripción.', 'text-domain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'libro' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'libro', $args );
}



// Lo enganchamos en la acción init y llamamos a la función create_book_taxonomies() cuando arranque
add_action( 'init', 'create_book_taxonomies', 0 );  

// Creamos dos taxonomías, género y autor para el custom post type "libro"
function create_book_taxonomies() {
  /* Configuramos las etiquetas que mostraremos en el escritorio de WordPress */
  $labels = array(
    'name'             => _x( 'Géneros', 'taxonomy general name' ),
    'singular_name'    => _x( 'Género', 'taxonomy singular name' ),
    'search_items'     =>  __( 'Buscar por Género' ),
    'all_items'        => __( 'Todos los Géneros' ),
    'parent_item'      => __( 'Género padre' ),
    'parent_item_colon'=> __( 'Género padre:' ),
    'edit_item'        => __( 'Editar Género' ),
    'update_item'      => __( 'Actualizar Género' ),
    'add_new_item'     => __( 'Añadir nuevo Género' ),
    'new_item_name'    => __( 'Nombre del nuevo Género' ),
  );
  
  /* Registramos la taxonomía y la configuramos como jerárquica (al estilo de las categorías) */
  register_taxonomy( 'generos', array( 'libro' ), array(
    'hierarchical'       => true,
    'labels'             => $labels,
    'show_ui'            => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'genero' ),
  ));
  
  
    // Añado otra taxonomía, esta vez no es jerárquica, como las etiquetas.
    $labels = array(
        'name'          => _x( 'Escritores', 'taxonomy general name' ),
        'singular_name' => _x( 'Escritor', 'taxonomy singular name' ),
        'search_items'  =>  __( 'Buscar Escritores' ),
        'popular_items' => __( 'Escritores populares' ),
        'all_items'     => __( 'Todos los escritores' ),
        'parent_item'   => null,
        'parent_item_colon' => null,
        'edit_item'     => __( 'Editar Escritor' ),
        'update_item'   => __( 'Actualizar Escritor' ),
        'add_new_item'  => __( 'Añadir nuevo Escritor' ),
        'new_item_name' => __( 'Nombre del nuevo Escritor' ),
        'separate_items_with_commas' => __( 'Separar Escritores por comas' ),
        'add_or_remove_items' => __( 'Añadir o eliminar Escritores' ),
        'choose_from_most_used' => __( 'Escoger entre los Escritores más utilizados' )
    );

    register_taxonomy( 'escritores', 'libro', array(
        'hierarchical'  => false,
        'labels'        => $labels, /* ADVERTENCIA: Aquí es donde se utiliza la variable $labels */
        'show_ui'       => true,
        'query_var'     => true,
        'rewrite'       => array( 'slug' => 'escritor' ),
    ));

        // Añado otra taxonomía, esta vez es jerárquica.
        $labels = array(
            'name'          => _x( 'h', 'taxonomy general name' ),
            'singular_name' => _x( 'h', 'taxonomy singular name' ),
            'search_items'  =>  __( 'Buscar h' ),
            'popular_items' => __( 'h populares' ),
            'all_items'     => __( 'Todos los h' ),
            'parent_item'   => null,
            'parent_item_colon' => null,
            'edit_item'     => __( 'Editar h' ),
            'update_item'   => __( 'Actualizar h' ),
            'add_new_item'  => __( 'Añadir nuevo h' ),
            'new_item_name' => __( 'Nombre del nuevo h' ),
            'separate_items_with_commas' => __( 'Separar h por comas' ),
            'add_or_remove_items' => __( 'Añadir o eliminar h' ),
            'choose_from_most_used' => __( 'Escoger entre los h más utilizados' )
        );
    
        register_taxonomy( 'h', 'libro', array(
            'hierarchical'  => true,
            'labels'        => $labels, /* ADVERTENCIA: Aquí es donde se utiliza la variable $labels */
            'show_ui'       => true,
            'query_var'     => true,
            'rewrite'       => array( 'slug' => 'h' ),
        ));
}

?>