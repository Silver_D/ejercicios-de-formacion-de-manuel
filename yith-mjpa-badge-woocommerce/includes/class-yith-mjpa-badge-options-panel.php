<?php
/**
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Badge_Options_Panel' ) ) {
	/**
	 * YITH_MJPA_Badge_Options_Panel
	 */
	class YITH_MJPA_Badge_Options_Panel {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Badge_Options_Panel
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Badge_Options_Panel Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'register_settings' ) );
		}
					/**
					 *  Create menu for general options
					 */
		public function create_menu_for_general_options() {
				add_menu_page(
					esc_html( 'Badge Options' ),
					esc_html( 'Badge Options' ),
					'manage_options',
					'yith_mjpa_badge_options',
					array( $this, 'yith_mjpa_badge_panel_view' ),
					'',
					40
				);
		}


		/**
		 * Yith_mjpa_badge_panel_view
		 *
		 * @return void
		 */
		public function yith_mjpa_badge_panel_view() {
			yith_mjpa_get_view( '/badge-options-panel.php', array() );
		}
		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function register_settings() {
			$page_name      = 'mjpa-badge-options-page';
			$section_name   = 'options_section';
			$setting_fields = array(
				array(
					'id'       => 'yith_mjpa_padding_top',
					'title'    => esc_html( 'Padding Top' ),
					'callback' => 'yith_mjpa_get_padding_top',
				),
				array(
					'id'       => 'yith_mjpa_padding_bottom',
					'title'    => esc_html( 'Padding Bottom' ),
					'callback' => 'yith_mjpa_get_padding_bottom',
				),
				array(
					'id'       => 'yith_mjpa_padding_right',
					'title'    => esc_html( 'Padding Right' ),
					'callback' => 'yith_mjpa_get_padding_right',
				),
				array(
					'id'       => 'yith_mjpa_padding_left',
					'title'    => esc_html( 'Padding Left' ),
					'callback' => 'yith_mjpa_get_padding_left',
				),
				array(
					'id'       => 'yith_mjpa_weight',
					'title'    => esc_html( 'Weight' ),
					'callback' => 'yith_mjpa_get_weight',
				),
				array(
					'id'       => 'yith_mjpa_style',
					'title'    => esc_html( 'Style' ),
					'callback' => 'yith_mjpa_get_style',
				),
				array(
					'id'       => 'yith_mjpa_color_picker',
					'title'    => esc_html( 'Color Picker' ),
					'callback' => 'yith_mjpa_get_color_picker',
				),
				array(
					'id'       => 'yith_mjpa_radius',
					'title'    => esc_html( 'Radius' ),
					'callback' => 'yith_mjpa_get_radius',
				),
				array(
					'id'       => 'yith_mjpa_badge_position_shop',
					'title'    => esc_html( 'Badge Position Shop' ),
					'callback' => 'yith_mjpa_get_badge_position_shop',
				),
				array(
					'id'       => 'yith_mjpa_badge_position_product',
					'title'    => esc_html( 'Badge Position Product' ),
					'callback' => 'yith_mjpa_get_badge_position_product',
				),

			);
			add_settings_section(
				$section_name,
				esc_html( 'Section' ),
				'',
				$page_name
			);
			foreach ( $setting_fields as $field ) {
				extract( $field );
				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);
				register_setting( $page_name, $id );
			}
		}


		/**
		 * Yith_mjpa_get_padding_top
		 * Refactorizar en una única llamada.
		 *
		 * @return void
		 */
		public function yith_mjpa_get_padding_top() {

			if ( empty( get_option( 'yith_mjpa_padding_top' ) ) ) {
				update_option( 'yith_mjpa_padding_top', 20 );
			}
			$yith_mjpa_aux_option = intval( get_option( 'yith_mjpa_padding_top' ) );
			?>
			<input type="number" id="yith_mjpa_padding_top" name="yith_mjpa_padding_top"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_padding_bottom
		 *
		 * @return void
		 */
		public function yith_mjpa_get_padding_bottom() {
			if ( empty( get_option( 'yith_mjpa_padding_bottom' ) ) ) {
				update_option( 'yith_mjpa_padding_bottom', 25 );
			}
			$yith_mjpa_aux_option = intval( get_option( 'yith_mjpa_padding_bottom' ) );
			?>
			<input type="number" id="yith_mjpa_padding_bottom" name="yith_mjpa_padding_bottom"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_padding_right
		 *
		 * @return void
		 */
		public function yith_mjpa_get_padding_right() {
			if ( empty( get_option( 'yith_mjpa_padding_right' ) ) ) {
				update_option( 'yith_mjpa_padding_right', 25 );
			}
			$yith_mjpa_aux_option = intval( get_option( 'yith_mjpa_padding_right' ) );
			?>
			<input type="number" id="yith_mjpa_padding_right" name="yith_mjpa_padding_right"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_padding_left
		 *
		 * @return void
		 */
		public function yith_mjpa_get_padding_left() {

			if ( empty( get_option( 'yith_mjpa_padding_left' ) ) ) {
				update_option( 'yith_mjpa_padding_left', 25 );
			}
			$yith_mjpa_aux_option = intval( get_option( 'yith_mjpa_padding_left' ) );
			?>
			<input type="number" id="yith_mjpa_padding_left" name="yith_mjpa_padding_left"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_weight
		 *
		 * @return void
		 */
		public function yith_mjpa_get_weight() {

			if ( empty( get_option( 'yith_mjpa_weight' ) ) ) {
				update_option( 'yith_mjpa_weight', 1 );
			}
			$yith_mjpa_aux_option = intval( get_option( 'yith_mjpa_weight' ) );
			?>
			<input type="number" id="yith_mjpa_weight" name="yith_mjpa_weight"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_style
		 *
		 * @return void
		 */
		public function yith_mjpa_get_style() {

			if ( empty( get_option( 'yith_mjpa_style' ) ) ) {
				update_option( 'yith_mjpa_style', 'solid' );
			}
			$yith_mjpa_aux_option = get_option( 'yith_mjpa_style' );
			?>
			<select id="yith_mjpa_style" name="yith_mjpa_style"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
				<option 			
				<?php
				if ( 'solid' === $yith_mjpa_aux_option ) {
					echo ( 'selected' );
				}
				?>
				value="solid">Solid</option>
				<option 				
				<?php
				if ( 'dotted' === $yith_mjpa_aux_option ) {
					echo ( 'selected' );
				}
				?>
				value="dotted">Dotted</option>
				<option 				
				<?php
				if ( 'double' === $yith_mjpa_aux_option ) {
					echo ( 'selected' );
				}
				?>
				value="double">Double</option>
			</select>
			<?php
		}
		/**
		 * Yith_mjpa_get_color_picker
		 *
		 * @return void
		 */
		public function yith_mjpa_get_color_picker() {

			if ( empty( get_option( 'yith_mjpa_color_picker' ) ) ) {
				update_option( 'yith_mjpa_color_picker', '#d8d8d8' );
			}
			$yith_mjpa_aux_option = get_option( 'yith_mjpa_color_picker' );
			?>
			<input type="text" class="class_color_picker" id="yith_mjpa_color_picker" name="yith_mjpa_color_picker"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_radius
		 *
		 * @return void
		 */
		public function yith_mjpa_get_radius() {

			if ( empty( get_option( 'yith_mjpa_radius' ) ) ) {
				update_option( 'yith_mjpa_radius', 7 );
			}
			$yith_mjpa_aux_option = intval( get_option( 'yith_mjpa_radius' ) );
			?>
			<input type="number" id="yith_mjpa_radius" name="yith_mjpa_radius"
				value="<?php echo ( esc_html( $yith_mjpa_aux_option ) ); ?>">
			<?php
		}
		/**
		 * Yith_mjpa_get_badge_position_shop
		 *
		 * @return void
		 */
		public function yith_mjpa_get_badge_position_shop() {

			if ( empty( get_option( 'yith_mjpa_badge_position_shop' ) ) ) {
				update_option( 'yith_mjpa_badge_position_shop', 'top_right' );
			}
			$yith_mjpa_aux_option = get_option( 'yith_mjpa_badge_position_shop' );
			?>
			<label><input 				
			<?php
			if ( 'top_left' === $yith_mjpa_aux_option ) {
				echo ( 'checked' );
			}
			?>
				type="radio" name="yith_mjpa_badge_position_shop" value="top_left"> Top Left</label>
			<label><input 				
			<?php
			if ( 'top_right' === $yith_mjpa_aux_option ) {
				echo ( 'checked' );
			}
			?>
				type="radio" name="yith_mjpa_badge_position_shop" value="top_right"> Top Right</label>
			<?php
		}
		/**
		 * Yith_mjpa_get_badge_position_product
		 *
		 * @return void
		 */
		public function yith_mjpa_get_badge_position_product() {

			if ( empty( get_option( 'yith_mjpa_badge_position_product' ) ) ) {
				update_option( 'yith_mjpa_badge_position_product', 'top_right' );
			}
			$yith_mjpa_aux_option = get_option( 'yith_mjpa_badge_position_product' );
			?>
			<label><input 				
			<?php
			if ( 'top_left' === $yith_mjpa_aux_option ) {
				echo ( 'checked' );
			}
			?>
				type="radio" name="yith_mjpa_badge_position_product" value="top_left"> Top Left</label>
			<label><input 				
			<?php
			if ( 'top_right' === $yith_mjpa_aux_option ) {
				echo ( 'checked' );
			}
			?>
				type="radio" name="yith_mjpa_badge_position_product" value="top_right"> Top Right</label>
			<?php
		}
	}
}
