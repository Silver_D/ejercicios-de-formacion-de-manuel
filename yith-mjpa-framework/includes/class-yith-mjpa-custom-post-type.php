<?php
/**
 * This file belongs to the YITH MJPA framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Custom_Post_Type' ) ) {
	/**
	 * YITH_MJPA_Custom_Post_Type
	 */
	class YITH_MJPA_Custom_Post_Type {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Custom_Post_Type
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Custom_Post_Type Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ), 5 );
		}
		/**
		 * Setup the 'Skeleton' custom post type
		 */
		public function setup_post_type() {
			$labels = array(
				'name'               => __( 'testimonials', 'yith-plugin-skeleton' ),
				'singular_name'      => __( 'testimony', 'yith-plugin-skeleton' ),
				'add_new'            => __( 'Add New testimony', 'yith-plugin-skeleton' ),
				'add_new_item'       => __( 'Add New testimony', 'yith-plugin-skeleton' ),
				'edit_item'          => __( 'Edit testimony', 'yith-plugin-skeleton' ),
				'new_item'           => __( 'Add New testimony', 'yith-plugin-skeleton' ),
				'view_item'          => __( 'View testimony', 'yith-plugin-skeleton' ),
				'search_items'       => __( 'Search testimony', 'yith-plugin-skeleton' ),
				'not_found'          => __( 'No testimony found', 'yith-plugin-skeleton' ),
				'not_found_in_trash' => __( 'No testimonials found in trash', 'yith-plugin-skeleton' ),
			);

			$args = array(
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => false,
				'query_var'          => true,
				'rewrite'            => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => null,
				'menu_icon'          => 'dashicons-calendar-alt',
				'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
			);

			register_post_type( 'yith_testimonials', $args );
			register_taxonomy(
				'taxonomy_no_dep',
				array( 'yith_testimonials' ),
				array(
					'hierarchical' => false,
					'labels'       => $labels,
					'show_ui'      => true,
					'query_var'    => true,
					'rewrite'      => array( 'slug' => 'taxonomy_no_dep' ),
				)
			);
			register_taxonomy(
				'taxonomy_dep',
				array( 'yith_testimonials' ),
				array(
					'hierarchical' => true,
					'labels'       => $labels,
					'show_ui'      => true,
					'query_var'    => true,
					'rewrite'      => array( 'slug' => 'taxonomy_dep' ),
				)
			);
		}
	}
}
