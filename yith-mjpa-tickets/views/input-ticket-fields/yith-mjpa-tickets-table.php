<?php
if ( $args ) {
	if ( ! isset( $args[0]['name_required'] ) ) {
		$args[0]['name_required'] = 'off';
	}
	if ( ! isset( $args[0]['surname_required'] ) ) {
		$args[0]['surname_required'] = 'off';
	}
	?>
	<div>
		<table class="default">
			<tr>
				<th class="yith-mjpa-text-table"><?php echo( esc_html( __( 'Name', 'yith-plugin-ticket' ) ) ); ?></th>
				<th class="yith-mjpa-checkbox-table"><?php echo( esc_html( __( 'Required', 'yith-plugin-ticket' ) ) ); ?></th>
			</tr>
			<tr>
				<td class="yith-mjpa-text-table"><input type="text" class="yith-mjpa-input-text-class" name="yith_mjpa_ticket[0][name]" value="<?php echo( esc_html( $args[0]['name'] ) ); ?>"></td>
				<td class="yith-mjpa-checkbox-table yith_mjpa_flex"><input type="checkbox" value="on" name="yith_mjpa_ticket[0][name_required]" 				
				<?php
				if ( 'on' === $args[0]['name_required'] ) {
					echo( esc_html( 'checked' ) );
				}
				?>
				></td>
			</tr>
			<tr>
				<th class="yith-mjpa-text-table"><?php echo( esc_html( __( 'Surname', 'yith-plugin-ticket' ) ) ); ?></th>
				<th class="yith-mjpa-checkbox-table"><?php echo( esc_html( __( 'Required', 'yith-plugin-ticket' ) ) ); ?></th>
			</tr>
			<tr>
				<td class="yith-mjpa-text-table"><input type="text" class="yith-mjpa-input-text-class" name="yith_mjpa_ticket[0][surname]" value="<?php echo( esc_html( $args[0]['surname'] ) ); ?>"></td>
				<td class="yith-mjpa-checkbox-table yith_mjpa_flex"><input  type="checkbox" value="on" name="yith_mjpa_ticket[0][surname_required]" 
				<?php
				if ( 'on' === $args[0]['surname_required'] ) {
					echo( esc_html( 'checked' ) );
				}
				?>
				></td>
			</tr>
		</table>
	</div>
	<?php
} else {
	?>
		<div>
			<table class="default">
				<tr>
					<th class="yith-mjpa-text-table"><?php echo( esc_html( __( 'Name', 'yith-plugin-ticket' ) ) ); ?></th>
					<th class="yith-mjpa-checkbox-table"><?php echo( esc_html( __( 'Required', 'yith-plugin-ticket' ) ) ); ?></th>
				</tr>
				<tr>
					<td class="yith-mjpa-text-table"><input type="text" class="yith-mjpa-input-text-class" name="yith_mjpa_ticket[0][name]"></td>
					<td class="yith-mjpa-checkbox-table yith_mjpa_flex"><input type="checkbox" value="on" name="yith_mjpa_ticket[0][name_required]"></td>
				</tr>
				<tr>
					<th class="yith-mjpa-text-table"><?php echo( esc_html( __( 'Surname', 'yith-plugin-ticket' ) ) ); ?></th>
					<th class="yith-mjpa-checkbox-table"><?php echo( esc_html( __( 'Required', 'yith-plugin-ticket' ) ) ); ?></th>
				</tr>
				<tr>
					<td class="yith-mjpa-text-table"><input type="text" class="yith-mjpa-input-text-class" name="yith_mjpa_ticket[0][surname]"></td>
					<td class="yith-mjpa-checkbox-table yith_mjpa_flex"><input type="checkbox" value="on" name="yith_mjpa_ticket[0][surname_required]"></td>
				</tr>
			</table>
		</div>
	<?php
} ?>
