<?php

/*
Plugin Name: Ejercicio8
Plugin URI: 
Description: Ejercicio8
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/
/*
 Crear una caja de información en los Post,
 con los datos del author del post y la fecha 
 del post. Dicha caja aparecerá cerrada al
 cargar la página, y se podrá abrir y cerrar
 al hacer click en un botón. El estilo de la 
 caja y contenido es libre, pero se valora que
 se añadan colores al texto, fondo, etc
*/

function add_theme_scripts() {
  if (is_single()) {
    wp_register_script('script', plugin_dir_url( __FILE__ ) . "assets/miscript.js");
    wp_register_style('style', plugin_dir_url( __FILE__ ) . "assets/style.css" );
    wp_enqueue_style( 'style' );
    wp_enqueue_script( 'script');
  }
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function caja($content){
  if (is_single()) {
    $content.=
    "<div class='class-box'>
      <div class='class-container'>
        <button onclick='change_display()'>Click me!</button>
        <div class='class-data' id='data'>
          <p>";
        $content.=get_the_author();
        $content.="</p>";
        $content.="<p>";
        $content.=get_the_date();
        $content.="</p>";
    $content.=
      "</div>
      </div>
    </div>";
  return $content;
  }
}
add_filter('the_content','caja');

?>