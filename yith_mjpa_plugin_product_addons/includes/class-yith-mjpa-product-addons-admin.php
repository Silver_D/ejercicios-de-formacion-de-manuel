<?php
/*
 * This file belongs to the YITH MJPA Plugin Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Plugin_Products_Addons_Admin' ) ) {

	class YITH_MJPA_Plugin_Products_Addons_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Plugin_Products_Addons_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Plugin_Products_Addons_Admin Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Plugin_Products_Addons_Admin constructor.
		 */
		private function __construct() {
			// actions here
		}
	}
}
