<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */


if ( ! function_exists( 'print_addons_value' ) ) {
	/**
	 * print_addons_value
	 *
	 * @return void
	 */
	function print_addons_value( $attr ) {
		echo ( esc_html( $attr ) );
	}
}
if ( ! function_exists( 'print_addons_type' ) ) {
	/**
	 * print_addons_type
	 *
	 * @return void
	 */
	function print_addons_type( $args ) {
		switch ( $args[0]['id_select'] ) {
			case 'text':
				yith_mjpa_get_template( '/frontend/yith-mjpa-addons-text.php', $args );
				break;
			case 'textarea':
				yith_mjpa_get_template( '/frontend/yith-mjpa-addons-textarea.php', $args );
				break;
			case 'select':
				yith_mjpa_get_template( '/frontend/yith-mjpa-addons-select.php', $args );
				break;
			case 'radio':
				yith_mjpa_get_template( '/frontend/yith-mjpa-addons-radio.php', $args );
				break;
			case 'onoff':
				yith_mjpa_get_template( '/frontend/yith-mjpa-addons-onoff.php', $args );
				break;
			case 'checkbox':
				yith_mjpa_get_template( '/frontend/yith-mjpa-addons-checkbox.php', $args );
				break;
			default:
				// code...
				break;
		}
	}
}
?>

 <div class="addons">
	<p class="yith_mjpa_title_addon"><?php print_addons_value( $args[0]['id_name_field'] ); ?></p>
	<?php print_addons_type( $args ); ?>
 </div>
