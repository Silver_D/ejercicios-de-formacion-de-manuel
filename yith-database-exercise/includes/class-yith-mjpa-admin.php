<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 **/

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PS_Admin' ) ) {
	/**
	 * YITH_PS_Admin
	 */
	class YITH_PS_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_PS_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PS_Admin constructor.
		 */
		private function __construct() {

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );
			add_action( 'admin_menu', array( $this, 'my_add_menu_items' ) );

		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box(
				'yith-ps-additional-information',
				__( 'Additional information', 'yith-plugin-skeleton' ),
				array(
					$this,
					'view_meta_boxes',
				),
				YITH_MJPA_Post_Types::$post_type
			);
		}

		/**
		 * View_meta_boxes
		 *
		 * @param  mixed $post variable post.
		 *
		 * @return void
		 */
		public function view_meta_boxes( $post ) {
			yith_ps_get_view( '/metaboxes/plugin-skeleton-info-metabox.php', array( 'post' => $post ) );
		}
		/**
		 * Save_meta_box
		 *
		 * @param  mixed $post_id id post.
		 * @return void
		 */
		public function save_meta_box( $post_id ) {

			if ( get_post_type( $post_id ) !== YITH_MJPA_Post_Types::$post_type ) {
				return;
			}

			if ( isset( $_POST['_yith_ps_role'] ) && ( isset( $_POST['yith_mp_control_meta'] ) && wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['yith_mp_control_meta'] ) ), 'control_meta' ) !== false ) ) {
				$postmeta_rate = sanitize_text_field( wp_unslash( $_POST['_yith_ps_role'] ) );
				update_post_meta(
					$post_id,
					'_yith_ps_role',
					$postmeta_rate
				);
			}
		}


		public function my_add_menu_items() {
			add_menu_page( 'My Plugin List Table', 'User List', 'activate_plugins', 'User_List', array( $this, 'my_render_list_page' ) );
		}
		public function my_render_list_page() {
			$myListTable = YITH_MJPA_Wp_List_Table::get_instance();
			echo '<div class="wrap"><h2>My User List Table</h2>';
			$myListTable->prepare_items();
			$myListTable->display();
			echo '</div>';
		}
	}
}
