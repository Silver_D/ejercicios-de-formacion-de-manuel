<?php
	global $post;
	$printer = YITH_MJPA_Admin_Addons_Fixed::get_instance();

?>
<div class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>" onchange="show_hide_price_fields(this)">
	<?php
	foreach ( $args['options'] as $key => $option ) {
		$printer->yith_mjpa_print_radio_options( $option );
	}
	?>
</div>
