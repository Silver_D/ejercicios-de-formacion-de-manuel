
function stars_loaded(param) {
    if (param == null) param = 0;
    let stars = document.querySelectorAll('.star');
    if (param != null && param != 0) {
        for (let j = 0; j < param; j++) {
            let choosenStar = stars[(stars.length - j - 1)];
            choosenStar.classList.add('choosen');
        };
    }

    for (let i = 0; i < stars.length; i++) {
        // calculate rating
        let rating = stars.length - i;

        stars[i].addEventListener("click", () => {

            for (let i = 0; i < stars.length; i++) {
                stars[i].classList.remove('choosen');
            };

            // marking choosen stars
            for (let j = 0; j < rating; j++) {
                let choosenStar = stars[(stars.length - j - 1)];
                choosenStar.classList.add('choosen');
            };
            document.getElementById("rating").value = rating;
        });

    };

}
function paint_stars(param) {
    param = param.toString();
    let rating = param.substr(0, 1);
    let id = param.substr(1, param.length)
    setTimeout(function () {
        let string = ".star" + id
        let stars = document.querySelectorAll(string)
        if (rating != null) {
            for (let j = 0; j < rating; j++) {
                let choosenStar = stars[(stars.length - j - 1)];
                choosenStar.classList.add('choosen');
            };
        }
    }, 100);

}
