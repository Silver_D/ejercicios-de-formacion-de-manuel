<?php
	global $post;
?>
<div class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>">
		<p class="form-field ">
			<label for="<?php echo( esc_html( $args['id_field'] ) ); ?>" class=""><?php echo( esc_html( $args['label_field'] ) ); ?></label>
			<input 
			<?php
			if ( get_post_meta( $post->ID, $args['id_field'], true ) == 'on' ) {
				echo( 'checked' );}
			?>
			type="checkbox" id="<?php echo( esc_html( $args['id_field'] ) ); ?>" name="<?php echo( esc_html( $args['name_field'] ) ); ?>" class="<?php echo( esc_html( $args['class_field'] ) ); ?>">
		</p>
	</div>
