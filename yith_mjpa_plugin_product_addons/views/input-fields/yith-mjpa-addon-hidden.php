<?php
if ( count( $args ) === 9 ) {
	$addons_value  = array_pop( $args );
	$addons_number = count( $addons_value );
}
?>
<div class="wrapp-accordion mjpa_hidden general_addons">
		<div class="accordion-title js-accordion-title">
			<div class="wrap-title-checkbox">
				<h4>Accordion Title 1</h4>
				<div class="class_checkbox_wrap">
					<span class="class_checkbox_enable"><input type="checkbox" id="_checkbox_enable" name="_checkbox_enable" class="_checkbox_enable"> Enable/Disable</span>
				</div>
			</div>
		</div>
		<div class="accordion-content options-group">
			<?php YITH_MJPA_Framework_Printer::yith_mjpa_print_input( $args ); ?>
			<div class="delete_addon_button_wrap">
				<button class="delete_addon_button button button-primary button-large" onclick="delete_addon(event)">Eliminar Addon</button>
			</div>
		</div>
</div>
	<?php
	if ( isset( $addons_number ) ) {
		for ( $i = 0; $i < $addons_number; $i++ ) {
			$class_accordion = 'accordion-title js-accordion-title index_' . $i;
			?>
		<div class="wrapp-accordion">
			<div class="<?php echo( $class_accordion ); ?>">
				<div class="wrap-title-checkbox">
					<h4>Accordion Title 1</h4>
					<div class="class_checkbox_wrap">
						<span class="class_checkbox_enable"><input type="checkbox" id="<?php echo( '_checkbox_enable_' . $i ); ?>" name="<?php echo( '_checkbox_enable_' . $i ); ?>" class="_checkbox_enable" 
																							<?php
																							if ( 'on' === $addons_value[ $i ]['_checkbox_enable'] ) {
																								echo( 'checked' );
																							}
																							?>
						> Enable/Disable</span>
					</div>
				</div>
			</div>
			<div class="accordion-content options-group">	
					<?php
					$new_args = array(
						'name_field'             => array(
							'type'                 => 'text',
							'id_field'             => 'id_name_field_' . $i,
							'class_field'          => 'class_name_field',
							'label_field'          => 'Name Field',
							'name_field'           => 'id_name_field_' . $i,
							'class_field_show'     => '',
							'default_text_value'   => $addons_value[ $i ]['id_name_field'],
							'default_color_picker' => '',
						),
						'textarea_input_type'    => array(
							'type'             => 'textarea',
							'id_field'         => 'id_text_area_' . $i,
							'class_field'      => 'short class_textarea',
							'label_field'      => 'Text area',
							'default_input'    => $addons_value[ $i ]['id_text_area'],
							'name_field'       => 'id_text_area_' . $i,
							'class_field_show' => '',
						),
						'select_input_type'      => array(
							'type'             => 'select',
							'id_field'         => 'id_select_' . $i,
							'class_field'      => 'short class_select',
							'label_field'      => 'Select Label',
							'name_field'       => 'id_select_' . $i,
							'default_input'    => $addons_value[ $i ]['id_select'],
							'class_field_show' => '',
							'options_value'    => array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' ),
						),
						'option_select'          => array(
							'type'                    => 'option',
							'id_field'                => 'id_name_option_field_' . $i,
							'default_text_value'      => 'a',
							'id_field_option_name'    => 'id_option_select_' . $i,
							'type_option'             => 'text',
							'class_field'             => 'class_option_select block',
							'class_field_price'       => 'class_option_price class_option_select',
							'class_field_option'      => 'class_option_option class_option_select',
							'class_field_wrap_option' => 'class_flex',
							'name_field_option'       => 'id_option_select',
							'id_field_price'          => 'id_price_select_' . $i,
							'name_field_price'        => 'id_price_select',
							'options_stored'          => $addons_value[ $i ]['id_option_select'],
							'price_stored'            => $addons_value[ $i ]['id_price_select'],
							'label_field_option'      => 'Name',
							'label_field_price'       => 'Price',
							'class_field_show'        => 'wrap_options_select mjpa_hidden',
							'class_group_show'        => 'wrap-options mjpa_hidden',
							'id_add_option'           => 'id_add_new_option_' . $i,
							'class_class_add_option'  => 'add_new_option taxonomy-add-new',
							'group_option_class'      => 'group_' . $i,
						),
						'radio_input_type_price' => array(
							'type'             => 'radio',
							'class_field'      => '',
							'class_field_show' => 'class_show',
							'options'          => array(
								'first'  => array(
									'type'             => 'radio',
									'id_field'         => '_free_option_' . $i,
									'name_field'       => '_radio_price_type_' . $i,
									'value_field'      => 'free',
									'class_field'      => 'class_radio_price_type_free',
									'label_field'      => 'Free',
									'default'          => $addons_value[ $i ]['_radio_price_type'],
									'class_field_show' => 'class_show_free',
								),
								'second' => array(
									'type'             => 'radio',
									'id_field'         => '_fixed_option_' . $i,
									'name_field'       => '_radio_price_type_' . $i,
									'value_field'      => 'fixed_price',
									'label_field'      => 'Fixed Price',
									'class_field'      => 'class_radio_price_type_fixed',
									'default'          => $addons_value[ $i ]['_radio_price_type'],
									'class_field_show' => '',
								),
								'third'  => array(
									'type'             => 'radio',
									'id_field'         => '_price_per_character_option_' . $i,
									'name_field'       => '_radio_price_type_' . $i,
									'value_field'      => 'price_per_character',
									'label_field'      => 'Price Per Character',
									'default'          => $addons_value[ $i ]['_radio_price_type'],
									'class_field'      => 'class_radio_price_type_character',
									'class_field_show' => 'class_price_per_character',
								),
							),
						),
						'price_field'            => array(
							'type'                 => 'text',
							'id_field'             => 'id_price_field_' . $i,
							'class_field'          => 'class_price_field',
							'label_field'          => 'Price Field',
							'name_field'           => 'id_price_field_' . $i,
							'class_field_show'     => 'price_show_class mjpa_hidden',
							'default_text_value'   => $addons_value[ $i ]['id_price_field'],
							'default_color_picker' => '',
						),
						'onoff'                  => array(
							'type'             => 'onoff',
							'id_field'         => '_onoff_default_value_id_' . $i,
							'class_field'      => 'class_onoff',
							'label_field'      => 'Default Enabled:',
							'name_field'       => '_onoff_default_value_id_' . $i,
							'default'          => $addons_value[ $i ]['_onoff_default_value_id'],
							'class_field_show' => 'onnoff_show_class mjpa_hidden',
						),
						'free_characters'        => array(
							'type'             => 'number',
							'id_field'         => '_free_characters_input_' . $i,
							'class_field'      => 'class_free_characters_input short',
							'label_field'      => 'Free Characters:',
							'name_field'       => '_free_characters_input_' . $i,
							'default'          => $addons_value[ $i ]['_free_characters_input'],
							'class_field_show' => 'free_characters_show_class mjpa_hidden',
						),
					);
					YITH_MJPA_Framework_Printer::yith_mjpa_print_input( $new_args );

					?>
			<div class="delete_addon_button_wrap">
				<button class="delete_addon_button button button-primary button-large" onclick="delete_addon(event)">Eliminar Addon</button>
			</div>
			</div>

		</div>
				<?php

		}
	}
	?>
