<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Transients' ) ) {
	/**
	 * YITH_PS_Frontend
	 */
	class YITH_MJPA_Transients {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Transients
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Transients Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * Call_db_before_transient
		 *
		 * @return array void
		 */
		public static function call_db_before_transient() {
			global $wpdb;
			$database_user_list_5 = $wpdb->get_results( 'SELECT name FROM wp_yith_raffle ORDER BY id DESC LIMIT 5', 'ARRAY_A' );
			return $database_user_list_5;
		}
		/**
		 * Check_transient
		 *
		 * @return transient void
		 */
		public static function check_transient() {
			$aux_transient = get_transient( 'users_list' );
			if ( false === $aux_transient ) {
				set_transient( 'users_list', self::call_db_before_transient(), 300 );
				$aux_transient = get_transient( 'users_list' );
			}
			return $aux_transient;

		}
	}
}
