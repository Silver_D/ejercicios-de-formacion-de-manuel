<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 * phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 */

if ( ! function_exists( 'get_data_from_user_form' ) ) {
	/**
	 * Get_data_from_user_form
	 *
	 * @return void
	 */
	function get_data_from_user_form() {
		if ( isset( $_POST['formcontrol'] ) || wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['formcontrol'] ) ), 'yith_mp_form' ) ) {
			if ( yith_check_exist_user_bd() == null ) {
				if ( empty( $_POST ) && ( isset( $_POST['yith_mp_form'] ) ) && isset( $_POST['_wp_http_referer'] ) ) {
					wp_safe_redirect( add_query_arg( array( 'errormsg' => 'Error' ), get_home_url() . sanitize_text_field( wp_unslash( $_POST['_wp_http_referer'] ) ) ) );
					exit;
				} else {
					collect_store_form_data();
					wp_safe_redirect(
						add_query_arg(
							array(
								'success' => esc_html__(
									'Your data has been sent correctly, good luck',
									'yith-mjpa-database-plugin'
								),
							),
							get_home_url() . sanitize_text_field( wp_unslash( $_POST['_wp_http_referer'] ) )
						)
					);
					exit;
				}
			} else {
				wp_safe_redirect( add_query_arg( array( 'errormsg' => 'Error' ), get_home_url() . sanitize_text_field( wp_unslash( $_POST['_wp_http_referer'] ) ) ) );
				exit;
			}
		}
	}
}

if ( ! function_exists( 'collect_store_form_data' ) ) {
	/**
	 * Collect_store_form_data
	 * Mandar verify nonce
	 *
	 * @return void
	 */
	function collect_store_form_data() {
		if ( isset( $_POST['txtname'] ) && isset( $_POST['txtlastname'] ) && isset( $_POST['txtemail'] ) && isset( $_POST['formcontrol'] ) || wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['formcontrol'] ) ), 'yith_mp_form' ) ) {
			global $wpdb;
			$table   = 'wp_yith_raffle';
			$data    = array(
				'name'      => sanitize_text_field( wp_unslash( $_POST['txtname'] ) ),
				'last_name' => sanitize_text_field( wp_unslash( $_POST['txtlastname'] ) ),
				'email'     => sanitize_text_field( wp_unslash( $_POST['txtemail'] ) ),
			);
			$format  = array(
				'%s',
				'%s',
				'%s',
			);
			$success = $wpdb->insert( $table, $data, $format );
		}
	}
}

/**
 * Yith_check_exist_user_bd
 *
 * @return bool void
 */
function yith_check_exist_user_bd() {
	if ( isset( $_POST['txtemail'] ) && isset( $_POST['formcontrol'] ) || wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['formcontrol'] ) ), 'yith_mp_form' ) ) {
		global $wpdb;
		$email  = sanitize_text_field( wp_unslash( $_POST['txtemail'] ) );
		$mylink = $wpdb->get_results( "SELECT * FROM wp_yith_raffle WHERE email='" . $email . "'", 'ARRAY_N' );
		if ( count( $mylink ) !== 0 ) {
			return true;
		} else {
			return false;
		}
	}
}

add_action( 'admin_post_nopriv_contactform', 'get_data_from_user_form' );
add_action( 'admin_post_contactform', 'get_data_from_user_form' );
