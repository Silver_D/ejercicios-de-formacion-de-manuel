<?php

/*
Plugin Name: Ejercicios-CustomPostType
Plugin URI: 
Description: Ejercicios-CustomPostType
Version: 1.0
Author: Manuel
Author URI: 
Text Domain: custom-post-type-lenguage
Domain Path: /languages
License: 
*/

/**
 * Registro un custom post type 'libro'.
 *
 * @link https://www.sitepoint.com/adding-meta-boxes-post-types-wordpress/#:~:text=To%20add%20a%20meta%20box%20to%20a%20number%20of%20post,the%20meta%20box%20to%20them.
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://justintadlock.com/archives/2010/04/29/custom-post-types-in-wordpress
 * @link https://www.dariobf.com/custom-post-types-wordpress/
 * @link https://generatewp.com/post-type/
 * @link https://wptheming.com/2010/08/custom-metabox-for-post-type/
 */

function my_plugin_load_plugin_textdomain() {
    load_plugin_textdomain( 'custom-post-type-lenguage', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'my_plugin_load_plugin_textdomain' );


function wpt_book_post_type() {

	$labels = array(
		'name'               => __( 'Books','custom-post-type-lenguage' ),
		'singular_name'      => __( 'Book','custom-post-type-lenguage' ),
		'add_new'            => __( 'Add New Book','custom-post-type-lenguage' ),
		'add_new_item'       => __( 'Add New Book','custom-post-type-lenguage' ),
		'edit_item'          => __( 'Edit Book','custom-post-type-lenguage' ),
		'new_item'           => __( 'Add New Book','custom-post-type-lenguage' ),
		'view_item'          => __( 'View Book','custom-post-type-lenguage'),
		'search_items'       => __( 'Search Book','custom-post-type-lenguage' ),
		'not_found'          => __( 'No Book found','custom-post-type-lenguage' ),
		'not_found_in_trash' => __( 'No Books found in trash','custom-post-type-lenguage' )
	);

	$args = array(
		'labels'               => $labels,
		'public'               => true,
		'capability_type'      => 'post',
		'rewrite'              => array( 'slug' => 'books' ),
		'has_archive'          => true,
		'menu_position'        => 30,
		'menu_icon'            => 'dashicons-calendar-alt',
		'register_meta_box_cb' => 'wpt_add_book_metaboxes',
	);

    register_post_type( 'books', $args );


}
add_action( 'init', 'wpt_book_post_type' );

function wpt_add_book_metaboxes() {

    add_meta_box(
		'wpt_books_ISBN',
		'ISBN',
		'wpt_books_print_front',
		'books',
		'normal',
		'high',array("ISBN")
    );
   add_meta_box(
		'wpt_books_Price',
		'Price',
		'wpt_books_print_front',
		'books',
		'normal',
		'high',array("Price")
    );/*
    add_meta_box(
		'wpt_libros_Tapa',
		'Tapa',
		'wpt_libros_Tapa',
		'libros',
		'normal',
		'high'
    );
    add_meta_box(
		'wpt_libros_Idioma',
		'Idioma',
		'wpt_libros_Idioma',
		'libros',
		'normal',
		'high'
	);*/
}
//hacer funciones genéricas.
/**
 * Output the HTML for the metabox.
 */
function wpt_books_print_front($post,$callbackargs) {
    //error_log ( print_r(  $callbackargs['args'][0], true ) );

	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'book_fields' );

	// Get the location data if it's already been entered
	$ISBN = get_post_meta( $post->ID, $callbackargs['args'][0], true );

	// Output the field
	echo '<input type="text" name=' . $callbackargs['args'][0] . ' value="' . esc_textarea( $ISBN )  . '" class="widefat">';

}

/**
 * Save the metabox data
 */
function wpt_save_books_meta( $post_id, $post ) {

	// Return if the user doesn't have edit permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	// Verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times.
	if ( ! isset( $_POST['ISBN'] ) || ! wp_verify_nonce( $_POST['book_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	if ( ! isset( $_POST['Price'] ) || ! wp_verify_nonce( $_POST['book_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}

	// Now that we're authenticated, time to save the data.
	// This sanitizes the data from the field and saves it into an array $libros_meta.
	$books_meta['ISBN'] = esc_textarea( $_POST['ISBN'] );
	$books_meta['Price'] = esc_textarea( $_POST['Price'] );

	// Cycle through the $libros_meta array.
	// Note, in this example we just have one item, but this is helpful if you have multiple.
	foreach ( $books_meta as $key => $value ) :

		// Don't store custom data twice
		if ( 'revision' === $post->post_type ) {
			return;
		}

		if ( get_post_meta( $post_id, $key, false ) ) {
			// If the custom field already has a value, update it.
			update_post_meta( $post_id, $key, $value );
		} else {
			// If the custom field doesn't have a value, add it.
			add_post_meta( $post_id, $key, $value);
		}

		if ( ! $value ) {
			// Delete the meta key if there's no value
			delete_post_meta( $post_id, $key );
		}

	endforeach;

}
add_action( 'save_post', 'wpt_save_books_meta', 1, 2 );


add_shortcode( 'print_post', 'getting_print_post' );

    function inicializar_shortcode3(){
        apply_filters( 'the_content', 'getting_print_post' );
        function getting_print_post($attr) {
            $argumentos = array(
                'numberposts' => 10,
                'post_type'   => 'books'
              );
               
            $loop = get_posts( $argumentos );
            error_log ( print_r(  $loop, true ) );
            echo "<h2>". __("Post Costum Post Title",'custom-post-type-lenguage') . "</h2>";
            echo "<p>" . $loop[0]->post_title . "</p>";
            echo "<h3>". __("Post Costum Content",'custom-post-type-lenguage') . "</h3>";
            echo "<p>" . $loop[0]->post_content . "</p>";
            echo "<h4>". __("ISBN",'custom-post-type-lenguage') . "</h4>";
			echo "<p>" . get_post_meta($loop[0]->ID,'ISBN',false)[0] . "</p>";
			echo "<h4>". __("Price",'custom-post-type-lenguage') . "</h4>";
            echo "<p>" . get_post_meta($loop[0]->ID,'Price',false)[0] . "</p>";
        }
    }
add_action('init', 'inicializar_shortcode3');

?>


