<?php
	global $post;
?>
<div class="yith_mjpa_global_wrap_personalize yith_mjpa_padding yith_mjpa_border">
	<div class="yith_mjpa_wrap_personalize">
		<p class="yith_mjpa_title_personalize">Personalize your device</p>
		<p class="yith_mjpa_text_personalize">Engrave your name,initials or phone number.</p>
		<?php
		if ( get_post_meta( $post->ID, '_radio_text_type', true ) === 'text' ) {
			echo( '<input type="text" value="" class="yith_mjpa_input_personalize" id="yith_mjpa_input_personalize_id" name="yith_mjpa_input_personalize_id">' );
		} else {
			echo( '<textarea class="yith_mjpa_input_personalize" id="yith_mjpa_input_personalize_id" placeholder name="yith_mjpa_input_personalize_id" rows="2" cols="20"></textarea>' );
		}
		?>
	</div>
	<p class="yith_mjpa_extra_price_personalize">0.00$</p>
</div>
