<?php
 $loop_addon = array_pop( $args );
 array_push( $args, $loop_addon )
?>
<div >
	<input type="number" id='<?php echo( '_index_hidden_variation_' . esc_html( $loop_addon ) ); ?>' name='<?php echo( '_index_hidden_variation_' . esc_html( $loop_addon ) ); ?>' class="class_index_hidden_variation mjpa_hidden" value="0">
	<input type="number" id='<?php echo( '_index_total_variation_' . esc_html( $loop_addon ) ); ?>' name='<?php echo( '_index_total_variation_' . esc_html( $loop_addon ) ); ?>' class="class_index_total_variation mjpa_hidden" value="">
	<input type="text" id='<?php echo( '_index_order_variation_' . esc_html( $loop_addon ) ); ?>' name='<?php echo( '_index_order_variation_' . esc_html( $loop_addon ) ); ?>' class="class_index_order_variation mjpa_hidden" value="">
	<button class='button_primary button' id='<?php echo( 'yith_mjpa_addon_id_variation_' . esc_html( $loop_addon ) ); ?>' onclick="button_click_function(event)"> Add new</button>
</div>

<div id='<?php echo( 'wrap_addons_order_' . esc_html( $loop_addon ) ); ?>'>
	<?php return yith_mjpa_get_view( '/yith-mjpa-ghost-addon-variation.php', $args ); ?>
</div>
