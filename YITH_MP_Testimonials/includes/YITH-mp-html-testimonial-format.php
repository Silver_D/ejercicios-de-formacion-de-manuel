<?php

function formatting_html_testimonials($attr,$show_image,$hover_effect) {
    /**
     * Recogemos los últimos 6 testimonios.
     */$rating_number=get_post_meta($attr->ID,"rating",'yes');
       $testimonial_id=$attr->ID;
       $vip = get_post_meta($attr->ID,"vip",'yes');
       $badge =get_post_meta($attr->ID,"badge",'yes');

       if ($vip=="affirmative") {
        echo (
        "<div class='yith_testimonial_mjpa_container vip ");
       }
       else{
        echo (
            "<div class='yith_testimonial_mjpa_container ");
       }
    
        if ($hover_effect=="default") {
            echo "hover_effects_default'>";
        }
        if ($hover_effect=="higlight") {
            echo "hover_effects_higlight'>";
        }
        if ($hover_effect=="zoom") {
            echo "hover_effects_zoom'>";
        }
        echo ("<div class= 'yith_testimonial_mjpa_pair_container' >");
        if ($show_image=="yes") {
            echo "<div class='yith_testimonial_mjpa_image'>" . get_the_post_thumbnail($attr) . "</div>";
        }
        echo("
        <div class='yith_testimonial_mjpa_name'>");
        if ($badge=="affirmative_badge") {
            echo ("
                <div class = 'special_guest". $attr->ID ."' >" . get_post_meta($attr->ID,"text_badge",'yes') . "</div>
                <style type='text/css'>
                    .special_guest". $attr->ID ." { background-color:". get_post_meta($attr->ID,"color",'yes') . "; }
                </style>
            ");
        }

        echo("<p>" . $attr->post_title . $attr->ID . "</p>
            <p>" .get_post_meta($attr->ID,"info",'yes') . 
            "<p>    </p><a href='" . get_post_meta($attr->ID,"url_company",'yes') . "'>" . get_post_meta($attr->ID,"company",'yes') . "</a>
            <p>" . get_post_meta($attr->ID,"email",'yes') . "</p>
            <div class='stars__wrapper'>
                <a href='#' class='star star". $attr->ID ."'>★</a>
                <a href='#' class='star star". $attr->ID ."'>★</a>
                <a href='#' class='star star". $attr->ID ."'>★</a>
                <a href='#' class='star star". $attr->ID ."'>★</a>
                <a href='#' class='star star". $attr->ID ."'>★</a>
            </div>
            <script>
                paint_stars($rating_number$testimonial_id );
            </script>
        </div>
        </div>
        <div class='yith_testimonial_mjpa_content'>
            <p>" . $attr->post_content . "</p>
        </div>
    </div>");    
}
