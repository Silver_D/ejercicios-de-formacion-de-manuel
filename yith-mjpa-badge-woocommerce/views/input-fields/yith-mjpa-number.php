<?php
function get_value( $args ) {
	global $post;
	$aux = get_post_meta( $post->ID, $args['id_field'], true );
	if ( ( ( '' === $aux ) && ( 'false' !== $args['default'] ) ) ) {
		echo( esc_html( $args['default'] ) );
	} else {
		echo( esc_html( $aux ) );
	}
}
?>
<div class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>">
	<p class="form-field ">
		<label for="<?php echo( esc_html( $args['id_field'] ) ); ?>" class=""><?php echo( esc_html( $args['label_field'] ) ); ?></label>
		<input value="<?php get_value( $args ); ?>" type="<?php echo( esc_html( $args['type'] ) ); ?>" name="<?php echo( esc_html( $args['name_field'] ) ); ?>" id="<?php echo( esc_html( $args['id_field'] ) ); ?>" class="<?php echo( esc_html( $args['class_field'] ) ); ?>" onclick="input_checkbox_status()">
	</p>
</div>
