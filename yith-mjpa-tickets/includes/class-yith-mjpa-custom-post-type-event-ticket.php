<?php
/**
 * This file belongs to the YITH MJPA Ticket .
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Custom_Post_Type' ) ) {
	/**
	 * YITH_MJPA_Custom_Post_Type
	 */
	class YITH_MJPA_Custom_Post_Type {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Custom_Post_Type
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Custom_Post_Type Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_mjpa_custom_post_type' ) );

			add_action( 'woocommerce_order_status_processing', array( $this, 'yith_mjpa_order_processing' ) );
			add_action( 'woocommerce_order_status_completed', array( $this, 'yith_mjpa_order_processing' ) );
			add_action( 'add_meta_boxes', array( $this, 'yith_mjpa_event_ticket_meta_box' ), 10, 2 );
			add_action( 'wp_ajax_nopriv_variation', array( $this, 'yith_mjpa_receive_ajax_call' ) );
			add_action( 'wp_ajax_variation', array( $this, 'yith_mjpa_receive_ajax_call' ) );
		}

		/**
		 * Global_notice_meta_box
		 *
		 * @return void
		 */
		public function yith_mjpa_event_ticket_meta_box( $post_type, $post ) {

			$screens        = array( 'event_ticket' );
			$title_meta_box = 'Event #' . $post->ID . '  ' . $post->post_title;
			foreach ( $screens as $screen ) {
				add_meta_box(
					'',
					$title_meta_box,
					array( $this, 'yith_mjpa_event_ticket_metabox_callback' ),
					$screen
				);
			}
		}
		/**
		 * Global_notice_meta_box_callback
		 *
		 * @param post mixed $post post.
		 * @return void
		 */

		public function yith_mjpa_event_ticket_metabox_callback( $post ) {
			$arr_attr_meta_box = array( $post );
			yith_mjpa_get_view( '/yith-mjpa-event-ticket-metabox.php', $arr_attr_meta_box );
		}

		/**
		 * Yith_mjpa_custom_post_type
		 *
		 * @return void
		 */
		public function yith_mjpa_custom_post_type() {
			$labels = array(
				'name'                  => _x( 'Event Tickets', 'yith-plugin-ticket' ),
				'singular_name'         => _x( 'Event Ticket', 'yith-plugin-ticket' ),
				'edit_item'             => __( 'Edit Event Ticket', ' yith-plugin-ticket' ),
				'view_item'             => __( 'View Event Ticket', ' yith-plugin-ticket' ),
				'search_items'          => __( 'Search Event Tickets', ' yith-plugin-ticket' ),
				'not_found'             => __( 'No Event Tickets found', ' yith-plugin-ticket' ),
				'set_featured_image'    => __( 'No Event Tickets found in the Trash', ' yith-plugin-ticket' ),
				'remove_featured_image' => __( 'No Event Tickets found in the Trash', ' yith-plugin-ticket' ),
				'use_featured_image'    => __( 'No Event Tickets found in the Trash', ' yith-plugin-ticket' ),
			);
			$args   = array(
				'labels'             => $labels,
				'description'        => 'Displays Event Tickets',
				'public'             => false,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'menu_icon'          => 'dashicons-clipboard',
				'capability_type'    => 'event_ticket',
				'capabilities'       => array( 'create_posts' => 'do_not_allow' ),
				'has_archive'        => true,
				'menu_position'      => 24,
				'supports'           => array( 'none' ),

			);
			register_post_type( 'event_ticket', $args );
		}
		/**
		 * Yith_mjpa_order_processing
		 *
		 * @param $order_id mixed $order_id orderid.
		 * @return void
		 */
		public function yith_mjpa_order_processing( $order_id ) {
			$order = wc_get_order( $order_id );
			foreach ( $order->get_items() as $item_id => $item ) {
				for ( $i = 0; $i < $item->get_quantity(); $i++ ) {
					$post_id_event_ticket = get_posts(
						array(
							'post_type'   => 'event_ticket',
							'meta_query'  => array(
								'relation' => 'AND',
								array(
									'key'   => 'yith_mjpa_item_id',
									'value' => $item->get_id(),
								),
								array(
									'key'   => 'yith_mjpa_event_ticket_order_id',
									'value' => $order_id,
								),
								array(
									'key'   => 'yith_mjpa_event_ticket_for_index',
									'value' => $i,
								),
							),
							'post_status' => 'any',
						)
					);
					if ( ! empty( $post_id_event_ticket ) ) {
						$yith_mjpa_status = ( 'yith_no_check' !== $post_id_event_ticket[0]->post_status ) ? 'yith_check' : 'yith_no_check';
					}
					$post_id_event_ticket = isset( $post_id_event_ticket[0]->ID ) ? $post_id_event_ticket[0]->ID : 0;
					$name                 = $item->get_name();
					$type                 = $item->get_type();
					$product_id           = $item->get_product_id();
					$product              = wc_get_product( $product_id );

					if ( 'event_ticket' === $product->get_type() ) {
						$new_post = array(
							'ID'           => $post_id_event_ticket,
							'post_title'   => $name,
							'post_content' => $order_id,
							'post_status'  => ( isset( $yith_mjpa_status ) ) ? $yith_mjpa_status : 'yith_no_check',
							'post_type'    => 'event_ticket',
							'meta_input'   => array(
								'yith_mjpa_name'    => $item->get_meta( 'Name ', true ),
								'yith_mjpa_surname' => $item->get_meta( 'Surname ', true ),
								'yith_mjpa_status'  => $order->get_status(),
								'yith_mjpa_item_id' => $item->get_id(),
								'yith_mjpa_event_ticket_order_id' => $order_id,
								'yith_mjpa_event_ticket_for_index' => $i,
							),
						);
						$post_id  = wp_insert_post( $new_post );
						if ( ! wc_get_order_item_meta( $item_id, ( 1 !== $item->get_quantity() ) ? 'Event id-' . $i : 'Event id', true ) ) {
							wc_add_order_item_meta( $item_id, ( 1 !== $item->get_quantity() ) ? 'Event id-' . $i : 'Event id', ( 0 === $post_id_event_ticket ) ? $post_id : $post_id_event_ticket );
						} else {
							wc_delete_order_item_meta( $item_id, ( 1 !== $item->get_quantity() ) ? 'Event id-' . $i : 'Event id' );
							wc_add_order_item_meta( $item_id, ( 1 !== $item->get_quantity() ) ? 'Event id-' . $i : 'Event id', ( 0 === $post_id_event_ticket ) ? $post_id : $post_id_event_ticket );
						}
					}
				}
			}
		}
				/**
				 * Yith_mjpa_receive_ajax_call
				 *
				 * @return void
				 */
		public function yith_mjpa_receive_ajax_call() {
			$yith_mjpa_post_id = isset( $_POST['yith_mjpa_post_id'] ) ? $_POST['yith_mjpa_post_id'] : false;//phpcs:ignore
			if ( 'yith_no_check' === get_post_status( $yith_mjpa_post_id ) ) {
				wp_update_post(
					array(
						'ID'          => $yith_mjpa_post_id,
						'post_status' => 'yith_check',
					)
				);
			} else {
				wp_update_post(
					array(
						'ID'          => $yith_mjpa_post_id,
						'post_status' => 'yith_no_check',
					)
				);
			}
			wp_die();
		}
	}

}
