<?php
/**
 * This file belongs to the YITH MJPA Addons fixed.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Admin_Addons_Fixed' ) ) {
	/**
	 * YITH_MJPA_Admin_Addons_Fixed
	 */
	class YITH_MJPA_Admin_Addons_Fixed {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Admin_Addons_Fixed
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Admin_Addons_Fixed Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_mjpa_add_new_tab' ) );
			add_filter( 'woocommerce_product_data_panels', array( $this, 'yith_mjpa_tab_panel_format' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_mjpa_save_tab_panel' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
		}

		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {
			wp_register_style( 'yith-mjpa-style-admin', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style-admin.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style-admin' );
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {
			global $post;
			wp_register_script( 'yith-mjpa-script-admin', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-script-admin.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-mjpa-script-admin' );
			if ( $post ) {
				if ( get_post_meta( $post->ID, 'addons', false ) ) {
					$addons_value  = get_post_meta( $post->ID, 'addons', false )[0];
					$addons_length = count( $addons_value );
					wp_localize_script( 'yith-mjpa-script-admin', 'attr', array( 'index_post' => $addons_length ) );
				}
			}

		}
		/**
		 * Yith_mjpa_add_new_tab
		 *
		 * @param tabs mixed $tabs tabs.
		 * @return tabs void
		 */
		public function yith_mjpa_add_new_tab( $tabs ) {

			$tabs['Badge'] = array(
				'label'  => __( 'Badge', 'woocommerce' ),
				'target' => 'badge_options',
				'class'  => array( 'show_if_simple', 'show_if_variable' ),
			);

			return $tabs;

		}

		/**
		 * Yith_mjpa_tab_panel_format
		 *
		 * @return void
		 */
		public static function yith_mjpa_tab_panel_format() {
			global $post;
			$arr_attr_input_types = array(
				'name_field'             => array(
					'type'                 => 'text',
					'id_field'             => 'yith-mjpa-addons-name-{{INDEX}}',
					'class_field'          => 'class_name_field',
					'label_field'          => 'Name Field',
					'name_field'           => 'yith-mjpa-addons[{{INDEX}}][name_field]',
					'class_field_show'     => '',
					'default_text_value'   => '',
					'default_color_picker' => '',
				),
				'textarea_input_type'    => array(
					'type'             => 'textarea',
					'id_field'         => 'yith-mjpa-addons-textarea-{{INDEX}}',
					'default_input'    => '',
					'class_field'      => 'short class_textarea',
					'label_field'      => 'Text area',
					'name_field'       => 'yith-mjpa-addons[{{INDEX}}][textarea_field]',
					'class_field_show' => '',
				),
				'select_input_type'      => array(
					'type'             => 'select',
					'id_field'         => 'yith-mjpa-addons-select-{{INDEX}}',
					'class_field'      => 'short class_select radio-group-[{{INDEX}}]',
					'label_field'      => 'Select Label',
					'default_input'    => '',
					'name_field'       => 'yith-mjpa-addons[{{INDEX}}][select_field]',
					'class_field_show' => '',
					'options_value'    => array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' ),
				),
				'option_select'          => array(
					'type'                    => 'option',
					'id_field'                => 'id_field',
					'default_text_value'      => '',
					'id_field_option_name'    => 'id_field_option_GHOST',
					'type_option'             => 'text',
					'class_field'             => 'class_option_select block',
					'class_field_price'       => 'class_option_price class_option_select',
					'class_field_option'      => 'class_option_option class_option_select',
					'class_field_wrap_option' => 'class_flex class_option_price_show class_option_price_show_{{INDEX}}',
					'name_field_option'       => 'yith-mjpa-addons[{{INDEX}}][name_field_option][]',
					'id_field_price'          => 'id_field_price_GHOST',
					'name_field_price'        => 'yith-mjpa-addons[{{INDEX}}][price_field_option][]',
					'label_field_option'      => 'Name',
					'label_field_price'       => 'Price',
					'class_field_show'        => 'wrap_options_select mjpa_hidden',
					'class_group_show'        => 'wrap-options mjpa_hidden GHOST_{{INDEX}}',
					'id_add_option'           => 'id_add_option_{{INDEX}}',
					'class_class_add_option'  => 'add_new_option taxonomy-add-new',
					'options_stored'          => '',
					'price_stored'            => '',
					'group_option_class'      => 'group_{{INDEX}}',
				),
				'radio_input_type_price' => array(
					'type'             => 'radio',
					'class_field'      => '',
					'class_field_show' => 'class_show radio-group-[{{INDEX}}]',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => 'yith-mjpa-addons-radio-free-{{INDEX}}',
							'name_field'       => 'yith-mjpa-addons[{{INDEX}}][radio_field]',
							'value_field'      => 'free',
							'class_field'      => 'class_radio_price_type_free',
							'label_field'      => 'Free',
							'default'          => 'true',
							'class_field_show' => 'class_show_free',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => 'yith-mjpa-addons-radio-fixed-{{INDEX}}',
							'name_field'       => 'yith-mjpa-addons[{{INDEX}}][radio_field]',
							'value_field'      => 'fixed_price',
							'label_field'      => 'Fixed Price',
							'class_field'      => 'class_radio_price_type_fixed',
							'default'          => 'false',
							'class_field_show' => '',
						),
						'third'  => array(
							'type'             => 'radio',
							'id_field'         => 'yith-mjpa-addons-radio-price-per-character-{{INDEX}}',
							'name_field'       => 'yith-mjpa-addons[{{INDEX}}][radio_field]',
							'value_field'      => 'price_per_character',
							'label_field'      => 'Price Per Character',
							'default'          => 'false',
							'class_field'      => 'class_radio_price_type_character',
							'class_field_show' => 'class_price_per_character',
						),
					),
				),
				'price_field'            => array(
					'type'                 => 'text',
					'id_field'             => 'yith-mjpa-addons-price-field-{{INDEX}}',
					'class_field'          => 'class_price_field',
					'label_field'          => 'Price Field',
					'name_field'           => 'yith-mjpa-addons[{{INDEX}}][price_field]',
					'class_field_show'     => 'price_show_class mjpa_hidden',
					'default_text_value'   => '',
					'default_color_picker' => '',
				),
				'onoff'                  => array(
					'type'             => 'onoff',
					'id_field_wrap'    => 'yith-mjpa-addons-onoff-wrapp-{{INDEX}}',
					'id_field'         => 'yith-mjpa-addons-onoff-{{INDEX}}',
					'class_field'      => 'class_onoff',
					'label_field'      => 'Default Enabled:',
					'name_field'       => 'yith-mjpa-addons[{{INDEX}}][onoff_field]',
					'default'          => 'on',
					'class_field_show' => 'onnoff_show_class mjpa_hidden',
				),
				'free_characters'        => array(
					'type'             => 'number',
					'id_field'         => 'yith-mjpa-addons-free-characters-input-{{INDEX}}',
					'class_field'      => 'class_free_characters_input short',
					'label_field'      => 'Free Characters:',
					'name_field'       => 'yith-mjpa-addons[{{INDEX}}][free-characters-field]',
					'default'          => 0,
					'class_field_show' => 'free_characters_show_class mjpa_hidden',
				),
			);
			array_push( $arr_attr_input_types, $post->ID )
			?>
			<div id='badge_options' class='panel woocommerce_options_panel'>
				<div id="wrap_addons">
				<input type="number" id='yith_mjpa_addons_index_hidden_id' name='yith_mjpa_addons_index_hidden' class="class_index_hidden mjpa_hidden" value="0">
					<button class='button_primary button' id="yith_mjpa_addon_id" onclick="button_click_function(event)"> Add new</button>
				</div>
				<div id="wrap_addons_order">
					<?php yith_mjpa_get_view( '/input-fields/yith-mjpa-addon-hidden.php', $arr_attr_input_types ); ?>
				</div>
			</div>
			<?php
		}
		/**
		 * Yith_mjpa_save_tab_panel
		 *
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function yith_mjpa_save_tab_panel( $post_id ) {
			array_shift( $_POST['yith-mjpa-addons'] );
			foreach ( $_POST['yith-mjpa-addons'] as $key => $value ) {
				array_shift( $_POST['yith-mjpa-addons'][ $key ]['name_field_option'] );
				array_shift( $_POST['yith-mjpa-addons'][ $key ]['price_field_option'] );
			}
			update_post_meta( $post_id, 'addons', $_POST['yith-mjpa-addons'] );
		}
		/**
		 * Yith_mjpa_print_input
		 *
		 * @param array mixed $arr_attr_input_types array.
		 * @return void
		 */
		public static function yith_mjpa_print_input( $arr_attr_input_types ) {
			foreach ( $arr_attr_input_types as $key => $value ) {
				yith_mjpa_get_view( '/input-fields/yith-mjpa-' . $value['type'] . '.php', $value );
			}
		}
		/**
		 * Yith_mjpa_print_radio_options
		 *
		 * @param option mixed $option args.
		 * @return void
		 */
		public function yith_mjpa_print_radio_options( $option ) {
			yith_mjpa_get_view( '/input-fields/yith-mjpa-radio-options.php', $option );
		}
	}
}
