<?php
$name_id_field        = 'checkbox_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];
error_log( print_r( $args, true ) );
if ( ! isset( $args[0]['onoff_field'] ) ) {
	$args[0]['onoff_field'] = 'off';
}
?>

<div>
<?php

if ( 'on' === $args[0]['onoff_field'] ) {
	echo( '<label><input checked class="yith_mjpa_input_checkbox yith_price_addon" type="checkbox" name="' . esc_html( $name_id_field ) . '" id="' . esc_html( $name_id_field ) . '" value="' . esc_html( $args[0]['textarea_field'] ) . '">' . esc_html( $args[0]['textarea_field'] ) . ' </label><br>' );
} else {
	echo( '<label><input class="yith_mjpa_input_checkbox yith_price_addon" type="checkbox" name="' . esc_html( $name_id_field ) . '" id="' . esc_html( $name_id_field ) . '" value="' . esc_html( $args[0]['textarea_field'] ) . '">' . esc_html( $args[0]['textarea_field'] ) . ' </label><br>' );
}


if ( 'fixed_price' === $args[0]['radio_field'] && ( 'on' === $args[0]['onoff_field'] ) ) {
	echo( '<p class="yith_mjpa_price_checkbox yith_price_addon_value">+' . esc_html( $args[0]['price_field'] ) . '.00$</p>' );
} else {
	echo( '<p class="yith_mjpa_price_checkbox yith_price_addon_value">+0.00$</p>' );
}
?>
</div>
