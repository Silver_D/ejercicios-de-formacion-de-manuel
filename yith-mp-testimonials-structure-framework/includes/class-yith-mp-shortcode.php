<?php

if ( ! class_exists( 'YITH_MP_Shortcode' ) ) {

	class YITH_MP_Shortcode {

		/**
		 * Main Instance
		 *
		 * @var YITH_MP_Shortcode
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MP_Shortcode Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		private function __construct() {
			add_action( 'init', array( $this, 'yith_mp_add_shortcode' ) );
		}

		public function yith_mp_add_shortcode() {
			add_shortcode( 'yith_mp_show_shortcode', array( $this, 'yith_mp_setup_testimonials' ) );
		}
		/**
		 * attr = atributos del shortcode
		 */

		public function yith_mp_setup_testimonials( $attr ) {

			if ( isset( $attr['hover_effect'] ) ) {
				$hover_effect = $attr['hover_effect'];
				update_option( 'yith_ps_shortcode_hover_effect', $attr['hover_effect'] );
			} else {
				$hover_effect = get_option( 'yit_yith-mjpa-plugin-framework_options', 'default' )['yith-mjpa-select'];
			}
			if ( isset( $attr['show_image'] ) ) {
				$show_image = $attr['show_image'];
				update_option( 'yith_ps_shortcode_show_image', $show_image );
			} else {
				$show_image = get_option( 'yit_yith-mjpa-plugin-framework_options', 'yes' )['yith-mjpa-checkbox'];
			}
			if ( isset( $attr['ids'] ) ) {
				$aux                    = $attr['ids'];
				$ids                    = explode( ',', $aux );
				$number_of_testimonials = sizeof( $ids );
			}
			if ( isset( $attr['number'] ) && ! isset( $attr['ids'] ) ) {
				$number_of_testimonials = $attr['number'];
			}
			if ( ! isset( $attr['number'] ) && ! isset( $attr['ids'] ) ) {
				$number_of_testimonials = 6;
			}

			if ( isset( $attr['ids'] ) ) {
				$arguments              = array(
					'numberposts' => $number_of_testimonials,
					'post_type'   => 'yith_testimonials',
					'include'     => $ids,
				);
					$testimonials_posts = get_posts( $arguments );
			} else {
				if ( isset( $attr['tax_ids'] ) ) {
					$tax_ids        = explode( ',', $attr['tax_ids'] );
					$taxonomy_array = array();
					foreach ( $tax_ids as  $value ) {
						$aux = get_object_vars( get_term_by( 'term_taxonomy_id', $value ) );
						array_push( $taxonomy_array, $aux['taxonomy'] );
					}
					$i                  = 0;
					$aux_array          = array();
					$testimonials_posts = array();
					foreach ( $taxonomy_array as $key => $value ) {
						$arguments = array(
							'post_type'   => 'yith_testimonials',
							'numberposts' => $number_of_testimonials,
							'tax_query'   => array(
								array(
									'field'    => 'term_id',
									'taxonomy' => $value,
									'terms'    => $tax_ids[ $i ],
								),
							),
						);
						$aux_array = get_posts( $arguments );

						foreach ( $aux_array as $key => $value ) {
							if ( ! in_array( $value, $testimonials_posts ) ) {
								array_push( $testimonials_posts, $value );
							}
						}
						$i = $i + 1;
					}
				} else {
					$arguments              = array(
						'numberposts' => $number_of_testimonials,
						'post_type'   => 'yith_testimonials',
					);
						$testimonials_posts = get_posts( $arguments );
				}
			}
			$atributos = array( $testimonials_posts, $show_image, $hover_effect );
			ob_start();
			yith_ps_get_template( '/frontend/testimonials.php', $atributos );
			return ob_get_clean();
		}
	}
}
