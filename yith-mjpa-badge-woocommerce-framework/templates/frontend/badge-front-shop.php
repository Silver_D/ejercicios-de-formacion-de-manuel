<?php
global $post;
$text        = get_post_meta( $post->ID, '_badge_text', true );
$color_text  = get_post_meta( $post->ID, '_badge_text_color', true );
$color_badge = get_post_meta( $post->ID, '_badge_background_color', true );
?>

</style>
<div class="yith_mjpa_wrap_badge">
	<p class="yith_mjpa_badge_text_shop onsale" id="badge_text_shop" style="color:<?php echo( esc_html( $color_text ) ); ?>;background-color:<?php echo( esc_html( $color_badge ) ); ?>; " > <?php echo( esc_html( $text ) ); ?> </p>
</div>
