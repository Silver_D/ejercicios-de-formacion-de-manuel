<?php
/**
 * This file belongs to the YITH MJPA Plugin Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Tab_Panel' ) ) {
	/**
	 * YITH_MJPA_Tab_Panel
	 */
	class YITH_MJPA_Tab_Panel {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Tab_Panel
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Tab_Panel Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_mjpa_add_new_tab' ) );
			add_filter( 'woocommerce_product_data_panels', array( $this, 'yith_mjpa_tab_panel_format' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_mjpa_save_tab_panel' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
		}
		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {

			// wp_register_style( 'check', YITH_MJPA_DIR_ASSETS_CSS_URL . '/check.css', array(), true );
			// wp_enqueue_style( 'check' );
			// wp_enqueue_style( 'wp-color-picker' );
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {

			// wp_register_script( 'tab_panel', YITH_MJPA_DIR_ASSETS_JS_URL . '/data-panel.js', array( 'jquery' ), 1, false );
			// wp_enqueue_script( 'tab_panel' );
			// wp_enqueue_script( 'mjpa-admin-color-picker', YITH_MJPA_DIR_ASSETS_JS_URL . '/mjpa-admin-color-picker.js', array( 'wp-color-picker' ), false, true );
		}
		/**
		 * Yith_mjpa_add_new_tab
		 *
		 * @param tabs mixed $tabs tabs.
		 * @return tabs void
		 */
		public function yith_mjpa_add_new_tab( $tabs ) {

			$tabs['Badge'] = array(
				'label'  => __( 'Badge', 'woocommerce' ),
				'target' => 'badge_options',
				'class'  => array( 'show_if_simple', 'show_if_variable' ),
			);

			return $tabs;

		}

		/**
		 * Yith_mjpa_tab_panel_format
		 *
		 * @return void
		 */
		public static function yith_mjpa_tab_panel_format() {
			global $post;
			$printer              = YITH_MJPA_Framework_Printer::get_instance();
			$arr_attr_input_types = array(
				'name_field'             => array(
					'type'                 => 'text',
					'id_field'             => 'id_name_field',
					'class_field'          => 'class_name_field',
					'label_field'          => 'Name Field',
					'name_field'           => 'id_name_field',
					'class_field_show'     => '',
					'default_text_value'   => '',
					'default_color_picker' => '',
				),
				'textarea_input_type'    => array(
					'type'             => 'textarea',
					'id_field'         => 'id_text_area',
					'default_input'    => '',
					'class_field'      => 'short class_textarea',
					'label_field'      => 'Text area',
					'name_field'       => 'id_text_area',
					'class_field_show' => '',
				),
				'select_input_type'      => array(
					'type'             => 'select',
					'id_field'         => 'id_select',
					'class_field'      => 'short class_select',
					'label_field'      => 'Select Label',
					'default_input'    => '',
					'name_field'       => 'id_select',
					'class_field_show' => '',
					'options_value'    => array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' ),
				),
				'option_select'          => array(
					'type'                    => 'option',
					'id_field'                => 'id_name_option_field',
					'default_text_value'      => '',
					'id_field_option_name'    => 'id_option_select',
					'type_option'             => 'text',
					'class_field'             => 'class_option_select block',
					'class_field_price'       => 'class_option_price class_option_select',
					'class_field_option'      => 'class_option_option class_option_select',
					'class_field_wrap_option' => 'class_flex',
					'name_field_option'       => 'id_option_select',
					'id_field_price'          => 'id_price_select',
					'name_field_price'        => 'id_price_select',
					'label_field_option'      => 'Name',
					'label_field_price'       => 'Price',
					'class_field_show'        => 'wrap_options_select mjpa_hidden',
					'class_group_show'        => 'wrap-options mjpa_hidden',
					'id_add_option'           => 'id_add_new_option',
					'class_class_add_option'  => 'add_new_option taxonomy-add-new',
					'options_stored'          => '',
					'price_stored'            => '',
					'group_option_class'      => 'group_',
				),
				'radio_input_type_price' => array(
					'type'             => 'radio',
					'class_field'      => '',
					'class_field_show' => 'class_show prueba',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => '_free_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'free',
							'class_field'      => 'class_radio_price_type_free',
							'label_field'      => 'Free',
							'default'          => 'true',
							'class_field_show' => 'class_show_free',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => '_fixed_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'fixed_price',
							'label_field'      => 'Fixed Price',
							'class_field'      => 'class_radio_price_type_fixed',
							'default'          => 'false',
							'class_field_show' => '',
						),
						'third'  => array(
							'type'             => 'radio',
							'id_field'         => '_price_per_character_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'price_per_character',
							'label_field'      => 'Price Per Character',
							'default'          => 'false',
							'class_field'      => 'class_radio_price_type_character',
							'class_field_show' => 'class_price_per_character',
						),
					),
				),
				'price_field'            => array(
					'type'                 => 'text',
					'id_field'             => 'id_price_field',
					'class_field'          => 'class_price_field',
					'label_field'          => 'Price Field',
					'name_field'           => 'id_price_field',
					'class_field_show'     => 'price_show_class mjpa_hidden',
					'default_text_value'   => '',
					'default_color_picker' => '',
				),
				'onoff'                  => array(
					'type'             => 'onoff',
					'id_field'         => '_onoff_default_value_id',
					'class_field'      => 'class_onoff',
					'label_field'      => 'Default Enabled:',
					'name_field'       => '_onoff_default_value_id',
					'default'          => 'on',
					'class_field_show' => 'onnoff_show_class mjpa_hidden',
				),
				'free_characters'        => array(
					'type'             => 'number',
					'id_field'         => '_free_characters_input',
					'class_field'      => 'class_free_characters_input short',
					'label_field'      => 'Free Characters:',
					'name_field'       => '_free_characters_input',
					'default'          => 0,
					'class_field_show' => 'free_characters_show_class mjpa_hidden',
				),
			);
			if ( count( get_post_meta( $post->ID, 'addons', false ) ) !== 0 ) {
				$addons_value = get_post_meta( $post->ID, 'addons', false )[0];
				array_push( $arr_attr_input_types, $addons_value );
			}
			$index_hidden_value = ( count( get_post_meta( $post->ID, 'index_hidden', false ) ) !== 0 ) ? get_post_meta( $post->ID, 'index_hidden', false )[0] : 0;
			$index_total_value  = ( count( get_post_meta( $post->ID, 'index_total', false ) ) !== 0 ) ? get_post_meta( $post->ID, 'index_total', false )[0] : 0;
			?>
			<div id='badge_options' class='panel woocommerce_options_panel'>
				<div id="wrap_addons">
					<button class='button_primary button' id="yith_mjpa_addon_id"> Add new</button>
					<input type="number" id='_index_hidden' name='_index_hidden' class="class_index_hidden mjpa_hidden" value="<?php echo( $index_hidden_value ); ?>">
					<input type="number" id='_index_total' name='_index_total' class="class_index_total mjpa_hidden" value="<?php echo( $index_total_value ); ?>">
					<input type="text" id='_index_order' name='_index_order' class="class_index_order mjpa_hidden" value="">
				</div>
				<div id="wrap_addons_order">
					<?php return yith_mjpa_get_view( '/input-fields/yith-mjpa-addon-hidden.php', $arr_attr_input_types ); ?>
				</div>
			</div>
			<?php
		}
		/**
		 * Yith_mjpa_save_tab_panel
		 *
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function yith_mjpa_save_tab_panel( $post_id ) {
			$array_name_fields = array( '_checkbox_enable', 'id_name_field', 'id_text_area', 'id_select', 'id_option_select', 'id_price_select', '_radio_price_type', '_free_characters_input', 'id_price_field', '_onoff_default_value_id' );
			$post_object       = array();
			$prueba            = $_POST['_index_hidden'] + $_POST['_index_total'];
			$addons_ready      = array();
			for ( $x = 0; $x < $prueba; $x++ ) {
				if ( isset( $_POST[ 'id_name_field_' . $x ] ) ) {
					$addons_ready[] = $x;
				}
			}
			$addons_ready_length   = count( $addons_ready );
			$correct_order         = explode( ',', $_POST['_index_order'] );
			$orden_correcto_length = count( $correct_order );
			if ( 1 === $orden_correcto_length ) {
				for ( $i = 0; $i < $addons_ready_length; $i++ ) {
					$aux_array = array();
					foreach ( $array_name_fields as $key => $value ) {
						if ( isset( $_POST[ $value . '_' . $addons_ready[ $i ] ] ) ) {
							if ( 'id_option_select' === $value || 'id_price_select' === $value ) {
								$aux_array[ $value ] = $_POST[ $value . '_' . $addons_ready[ $i ] ];
							} else {
								$aux_array[ $value ] = $_POST[ $value . '_' . $addons_ready[ $i ] ];
							}
						} else {
							$aux_array[ $value ] = '';
						}
					}
					array_push( $post_object, $aux_array );
				}
			} else {
				for ( $i = 0; $i < $addons_ready_length; $i++ ) {
					$aux_array = array();
					foreach ( $array_name_fields as $key => $value ) {
						if ( isset( $_POST[ $value . '_' . $correct_order[ $i ] ] ) ) {
								$aux_array[ $value ] = $_POST[ $value . '_' . $correct_order[ $i ] ];
						} else {
							$aux_array[ $value ] = '';
						}
					}
					array_push( $post_object, $aux_array );
				}
			}

			update_post_meta( $post_id, 'addons', $post_object );
			update_post_meta( $post_id, 'index_hidden', $addons_ready_length );
			update_post_meta( $post_id, '_index_total', $_POST['_index_total'] );
		}

	}
}
