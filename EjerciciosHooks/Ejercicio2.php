<?php

/*
Plugin Name: Ejercicio2
Plugin URI: 
Description: Ejercicio2
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/
/*
Partimos del mismo if del ejercicio anterior, el único añadido es el uso de la función get_the_author(), que permite la obtención del post actual.
*/
function insertaNotaFinalConAutor($content) {
    if(!is_feed() && !is_home()) {
        $autor= get_the_author();
        $content.= "<p>Este post fue escrito por Manuel</p>";
        $content.=$autor;

    }
    return $content;
}
add_filter ('the_content', 'insertaNotaFinalConAutor');


?>