<?php
/*
 * This file belongs to the YITH MJPA Ticket .
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Admin' ) ) {

	class YITH_MJPA_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Admin Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Admin constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'ticket_product_tab' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'event_ticket_tab_product_tab_content' ) );
			add_action( 'woocommerce_admin_process_product_object', array( $this, 'save_event_ticket_product_settings' ), 10, 1 );
			add_action( 'admin_init', array( $this, 'yith_mjpa_add_role' ) );

			// Update the columns shown on the custom post type edit.php view - so we also have custom columns
			add_filter( 'manage_event_ticket_posts_columns', array( $this, 'yith_mjpa_custom_post_type_columns' ) );
			add_action( 'manage_event_ticket_posts_custom_column', array( $this, 'yith_mjpa_fill_custom_post_type_columns' ), 10, 2 );
		}

		/**
		 * Event_ticket_tab_product_tab_content
		 *
		 * @return void
		 */
		public function event_ticket_tab_product_tab_content() {
			global $post;
			?>
			<div id='event_ticket_product_options' class='panel woocommerce_options_panel'>
				<div class='options_group'>
			<?php
			if ( get_post_meta( $post->ID, 'ticket_info', true ) ) {
				$arr_attr = get_post_meta( $post->ID, 'ticket_info', true );
				yith_mjpa_get_view( '/input-ticket-fields/yith-mjpa-tickets-table.php', $arr_attr );
			} else {
				yith_mjpa_get_view( '/input-ticket-fields/yith-mjpa-tickets-table.php' );
			}

			?>
				</div>
			</div>
			<?php
		}
		/**
		 * Yith_mjpa_add_role
		 *
		 * @return void
		 */
		public function yith_mjpa_add_role() {

			$admin_role = get_role( 'administrator' );
			if ( $admin_role ) {
				$capabilities = array(
					'edit_event_tickets',
					'edit_event_ticket',
					'read_event_ticket',
					'delete_event_tickets',
					'delete_event_ticket',
					'publish_event_tickets',
					'read_private_event_tickets',
				);
				foreach ( $capabilities as $key => $value ) {
					if ( ! $admin_role->has_cap( $value ) ) {
						$admin_role->add_cap( $value );
					}
				}
			}
		}
		/**
		 * Save_event_ticket_product_settings
		 *
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function save_event_ticket_product_settings( $product ) {
			global $post;
			$product->update_meta_data( 'ticket_info', $_POST['yith_mjpa_ticket'] );
		}
		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {

			wp_register_style( 'yith-mjpa-style-product-type-ticket-admin', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style-product-type-ticket-admin.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style-product-type-ticket-admin' );

		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {
			wp_register_script( 'yith-mjpa-script-product-type-ticket-admin', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-script-product-type-ticket-admin.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-mjpa-script-product-type-ticket-admin' );
			wp_localize_script(
				'yith-mjpa-script-product-type-ticket-admin',
				'attr_ajax',
				array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
				)
			);

		}
		/**
		 * Ticket_product_tab
		 *
		 * @param Tabs mixed $tabs tabs.
		 * @return tabs void
		 */
		public function ticket_product_tab( $tabs ) {
			global $post;
			$tabs['event_ticket'] = array(
				'label'  => __( 'Event Ticket', 'yith-plugin-ticket' ),
				'target' => 'event_ticket_product_options',
				'class'  => array( 'show_if_event_ticket' ),
			);
			return $tabs;
		}
		/**
		 * Yith_mjpa_custom_post_type_columns
		 *
		 * @param columns mixed $columns columns.
		 * @return array void
		 */
		public function yith_mjpa_custom_post_type_columns( $columns ) {
			// Remove Author and Comments from Columns and Add custom column 1, custom column 2 and Post Id
			unset(
				$columns['wpseo-score'],
				$columns['wpseo-title'],
				$columns['wpseo-metadesc'],
				$columns['wpseo-focuskw']
			);
			return array(
				'cb'                 => '<input type="checkbox" />',
				'yith_mjpa_received' => __( 'Received', 'yith-plugin-ticket' ),
				'yith_mjpa_title'    => __( 'Ticket', 'yith-plugin-ticket' ),
				'yith_mjpa_order'    => __( 'Order', 'yith-plugin-ticket' ),
				'yith_mjpa_status'   => __( 'Purchase status', 'yith-plugin-ticket' ),
				'yith_mjpa_actions'  => __( 'Actions', 'yith-plugin-ticket' ),
			);
			// return $columns;
		}
		/**
		 * Yith_mjpa_fill_custom_post_type_columns
		 *
		 * @param column mixed  $column column.
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function yith_mjpa_fill_custom_post_type_columns( $column, $post_id ) {
			// Fill in the columns with meta box info associated with each post
			$post_data = get_post( $post_id );

			// comprobar si está registrado el email, sino pones el nombre
			switch ( $column ) {
				case 'yith_mjpa_received':
					if ( 'yith_no_check' === get_post_status( $post_id ) ) {
						echo( '<span id="' . esc_html( $post_id ) . '" class="yith_mjpa_not_checked dashicons  dashicons-tickets-alt"></span>' );
					} else {
						echo( '<span id="' . esc_html( $post_id ) . '" class="yith_mjpa_checked dashicons  dashicons-tickets-alt"></span>' );
					}
					break;
				case 'yith_mjpa_title':
					echo ( '<p>#' . esc_html( $post_id ) . '   ' . esc_html( $post_data->post_title ) . '</p>' );
					break;
				case 'yith_mjpa_order':
					echo ( '<p>Order Id ' . esc_html( $post_data->post_content ) . ' by ' . esc_html( get_the_author_meta( 'nicename', $post_data->post_author ) ) . '</p>' );
					break;
				case 'yith_mjpa_status':
					echo ( '<p>' . esc_html( ucfirst( get_post_meta( $post_id )['yith_mjpa_status'][0] ) ) . ' on <strong>' . esc_html( get_the_date( 'F j, Y', $post_id ) ) . '</strong></p>' );
					break;
				case 'yith_mjpa_actions':
					echo ( '<a href="' . esc_html( get_edit_post_link( $post_id ) ) . '"><span id="yith_mjpa_icon_edit_id" class="dashicons dashicons-visibility"></span></a><a><span id="' . esc_html( $post_id ) . '" class="yith_mjpa_checkbox_admin_meta_box dashicons dashicons-thumbs-up"></span></a>' );
					break;
			}
		}
	}

}
