<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

 ?>

    <div class='format_columns_testimonials'>
        <?php
           // error_log ( print_r(  $args[], true ) );
           $show_image = $args[1];
           $hover_effect = $args[2];
           $args_taxo = array(
            'object_type'  => array( 'yith_testimonials' ),
            );

        $taxonomies_type = get_taxonomies( $args_taxo );
        foreach ($args[0] as $key => $value) {

            $rating_number=get_post_meta($value->ID,"rating",'yes');
            $testimonial_id=$value->ID;
            $vip = get_post_meta($value->ID,"vip",'yes');
            $badge =get_post_meta($value->ID,"badge",'yes');

            if ($vip=="affirmative") {
                echo (
                "<div class='yith_testimonial_mjpa_container vip ");
               }
               else{
                echo (
                    "<div class='yith_testimonial_mjpa_container ");
               }
            
                if ($hover_effect=="default") {
                    echo "hover_effects_default'>";
                }
                if ($hover_effect=="highlight") {
                    echo "hover_effects_highlight'>";
                }
                if ($hover_effect=="zoom") {
                    echo "hover_effects_zoom'>";
                }
                echo ("<div class= 'yith_testimonial_mjpa_pair_container' >");
                if ($show_image=="yes") {
                    echo "<div class='yith_testimonial_mjpa_image'>" . get_the_post_thumbnail($value) . "</div>";
                }
                echo("
                <div class='yith_testimonial_mjpa_name'>");
                if ($badge=="affirmative_badge") {
                    echo ("
                        <div class = 'special_guest". $value->ID ."' >" . get_post_meta($value->ID,"text_badge",'yes') . "</div>
                        <style type='text/css'>
                            .special_guest". $value->ID ." { background-color:". get_post_meta($value->ID,"color",'yes') . "; }
                        </style>
                    ");
                }
        
                echo("<p>" . $value->post_title . $value->ID . "</p>
                    <p>" .get_post_meta($value->ID,"info",'yes') . 
                    "<p>    </p><a class='url_company_color' href='" . get_post_meta($value->ID,"url_company",'yes') . "'>" . get_post_meta($value->ID,"company",'yes') . "</a>
                    <p>" . get_post_meta($value->ID,"email",'yes') . "</p>
                    <div class='stars__wrapper'>
                        <a href='#' class='star star". $value->ID ."'>★</a>
                        <a href='#' class='star star". $value->ID ."'>★</a>
                        <a href='#' class='star star". $value->ID ."'>★</a>
                        <a href='#' class='star star". $value->ID ."'>★</a>
                        <a href='#' class='star star". $value->ID ."'>★</a>
                    </div>
                    <script>
                        paint_stars($rating_number$testimonial_id );
                    </script>
                </div>
                </div>
                <div class='yith_testimonial_mjpa_content'>
                    <p>" . $value->post_content . "</p>");
                    foreach ($taxonomies_type as $key => $taxo) {
                        $terms = get_the_terms ($value->ID, $taxo);
                        $skills_links= null;
                        if(!empty($terms)){
                            $skills_links = wp_list_pluck($terms, 'name');
                        } 
                        if(!empty($skills_links)){
                            $array_to_string_taxonomy = implode(', ', $skills_links);
                            echo("<p>" . $array_to_string_taxonomy . "</p>");
                        }
                      } 
                    echo("
                </div>
            </div>");
        }
        ?>        
    </div>

