<?php
/**
 * This file belongs to the YITH_MJPA_BAGDE_WOOCOMMERCE_EXERCISE
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Data_Panel' ) ) {
	/**
	 * YITH_MJPA_Data_Panel
	 */
	class YITH_MJPA_Data_Panel {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Data_Panel
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Data_Panel Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_mjpa_add_new_tab' ) );
			add_filter( 'woocommerce_product_data_panels', array( $this, 'add_product_data_panels' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_mjpa_save_data_panel' ) );

		}

		/**
		 * Yith_mjpa_add_new_tab
		 *
		 * @param tabs mixed $tabs tabs.
		 * @return tabs void
		 */
		public function yith_mjpa_add_new_tab( $tabs ) {

			$tabs['Badge'] = array(
				'label'    => __( 'Badge', 'woocommerce' ),
				'target'   => 'badge_options',
				'class'    => array( 'show_if_simple', 'show_if_variable' ),
				'priority' => 15,
			);

			return $tabs;

		}
		public function add_product_data_panels() {
			global $post;
			$tabs = array(
				'loquesea' => 'badge_options',
			);
			foreach ( $tabs as $key => $tab_id ) {
				echo "<div id='{$tab_id}' class='panel woocommerce_options_panel'>";
				include YITH_MJPA_DIR_TEMPLATES_PATH . '/admin/tab-product-panel.php';
				echo '</div>';
			}
		}
		/**
		 * Yith_mjpa_data_panel_format
		 *
		 * @return void
		 */
		public function yith_mjpa_data_panel_format() {
			$printer              = YITH_MJPA_Framework_Printer::get_instance();
			$arr_attr_input_types = array(
				'checkbox_enable'        => array(
					'type'             => 'checkbox',
					'id_field'         => '_enable_plugin',
					'class_field'      => 'class_checkbox_enable',
					'label_field'      => 'Enable Plugin',
					'name_field'       => '_enable_plugin',
					'class_field_show' => '',
				),
				'text_input_type'        => array(
					'type'                 => 'text',
					'id_field'             => '_note_label',
					'class_field'          => 'class_note_label short',
					'label_field'          => 'Note label',
					'name_field'           => '_note_label',
					'class_field_show'     => 'class_show',
					'default_color_picker' => '',
					'default_text_value'   => 'Note',
				),
				'textarea_input_type'    => array(
					'type'             => 'textarea',
					'id_field'         => '_text_area',
					'class_field'      => 'short',
					'label_field'      => 'Text area',
					'name_field'       => '_text_area',
					'class_field_show' => 'class_show',
				),
				'radio_input_type'       => array(
					'type'             => 'radio',
					'id_field'         => '_radio_text_type',
					'label_field'      => 'Field Type',
					'name_field'       => '_radio_text_type',
					'class_field_show' => 'class_show',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => '_text_option',
							'name_field'       => '_radio_text_type',
							'class_field'      => '',
							'value_field'      => 'text',
							'label_field'      => 'Text',
							'default'          => 'true',
							'class_field_show' => '',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => '_textarea_option',
							'name_field'       => '_radio_text_type',
							'value_field'      => 'textarea',
							'class_field'      => '',
							'label_field'      => 'Textarea',
							'default'          => 'false',
							'class_field_show' => '',
						),
					),
				),
				'radio_input_type_price' => array(
					'type'             => 'radio',
					'class_field'      => '',
					'class_field_show' => 'class_show',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => '_free_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'free',
							'class_field'      => '',
							'label_field'      => 'Free',
							'default'          => 'true',
							'class_field_show' => 'class_show_free',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => '_fixed_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'fixed_price',
							'label_field'      => 'Fixed Price',
							'class_field'      => '',
							'default'          => 'false',
							'class_field_show' => '',
						),
						'third'  => array(
							'type'             => 'radio',
							'id_field'         => '_price_per_character_option',
							'name_field'       => '_radio_price_type',
							'value_field'      => 'price_per_character',
							'label_field'      => 'Price Per Character',
							'default'          => 'false',
							'class_field'      => '',
							'class_field_show' => '',
						),
					),
				),
				'text_price'             => array(
					'type'                 => 'text',
					'id_field'             => '_price_text',
					'class_field'          => 'class_price_text short',
					'label_field'          => 'Price:',
					'name_field'           => '_price_text',
					'class_field_show'     => 'class_show class_show_price ',
					'default_color_picker' => '',
					'default_text_value'   => '',
				),
				'free_characters'        => array(
					'type'             => 'number',
					'id_field'         => '_free_characters_input',
					'class_field'      => 'class_free_characters_input short',
					'label_field'      => 'Free Characters:',
					'name_field'       => '_free_characters_input',
					'default'          => 0,
					'class_field_show' => 'class_show class_show_price',
				),
				'checkbox_show_badge'    => array(
					'type'             => 'checkbox',
					'id_field'         => '_allow_badge',
					'class_field'      => 'class_checkbox class_badge_enable',
					'label_field'      => 'Allow Badge',
					'name_field'       => '_allow_badge',
					'class_field_show' => 'class_show',
				),
				'badge_text'             => array(
					'type'                 => 'text',
					'id_field'             => '_badge_text',
					'class_field'          => 'class_badge_text short',
					'label_field'          => 'Badge Text:',
					'name_field'           => '_badge_text',
					'class_field_show'     => 'class_show class_show_badge',
					'default_color_picker' => '',
					'default_text_value'   => '',
				),
				'badge_background_color' => array(
					'type'                 => 'text',
					'id_field'             => '_badge_background_color',
					'class_field'          => 'short class_color_picker',
					'label_field'          => 'Badge Background Color:',
					'name_field'           => '_badge_background_color',
					'class_field_show'     => 'class_show class_show_badge',
					'default_color_picker' => '#007694',
					'default_text_value'   => '',
				),
				'badge_text_color'       => array(
					'type'                 => 'text',
					'id_field'             => '_badge_text_color',
					'class_field'          => 'short class_color_picker',
					'label_field'          => 'Badge Text Color:',
					'name_field'           => '_badge_text_color',
					'class_field_show'     => 'class_show class_show_badge',
					'default_color_picker' => '#ffffff',
					'default_text_value'   => '',
				),
			);

			$printer->yith_mjpa_print_input( $arr_attr_input_types );

		}
		/**
		 * Yith_mjpa_save_data_panel
		 *
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function yith_mjpa_save_data_panel( $post_id ) {
			$array_name_fields = array( '_yith_mjpa_onoff_enable', '_yith_mjpa_note_label', '_yith_mjpa_textarea', '_yith_mjpa_radio', '_yith_mjpa_radio_price', '_yith_mjpa_number_price', '_yith_mjpa_number_free_characters', '_yith_mjpa_checkbox_allow_badge', '_badge_text', '_yith_mjpa_badge_text', '_yith_mjpa_color_picker_background_badge_color', '_yith_mjpa_color_picker_text_badge_color' );

			foreach ( $array_name_fields as $key => $value ) {
				if ( isset( $_POST[ $value ] ) ) {
					update_post_meta( $post_id, $value, $_POST[ $value ] );
				} else {
					update_post_meta( $post_id, $value, 'off' );
				}
			}
		}

	}
}
