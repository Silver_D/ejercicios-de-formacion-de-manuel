<?php
$name_id_field        = 'textarea_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];
?>

<div>
  <p class="yith_mjpa_description_addon"><?php print_addons_value( $args[0]['id_text_area'] ); ?></p>
  <textarea class="yith_mjpa_input_text yith_price_addon" id="<?php echo ( esc_html( $name_id_field ) ); ?>" name="<?php echo ( esc_html( $name_id_field ) ); ?>" rows="4" cols="50">
  </textarea>
  <p class=" yith_price_addon_value <?php echo ( esc_html( $yith_mjpa_price_type ) ); ?>">+0.00$</p>
</div>
