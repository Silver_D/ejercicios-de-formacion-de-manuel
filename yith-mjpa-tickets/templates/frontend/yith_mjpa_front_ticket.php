<?php
// Añadir fantasma con título de índice cambiante
// falta tener en cuenta lo de required
?>
<div id="yith_mjpa_wrapp_all_tickets">
	<div class="yith_mjpa_wrap_ticket mjpa_hidden GHOST">
		<div class="yith_mjpa_ticket_margin">
		<h4>Title_Ticket_INDEX</h4>
		<label for="yith_mjpa_name_id_INDEX"><?php echo( esc_html( $args[0][0]['name'] ) ); ?>*</label>
		<input type="text" id="yith_mjpa_name_id_INDEX" class="yith_mjpa_input_text"/>
		<label for="yith_mjpa_surname_id_INDEX"><?php echo( esc_html( $args[0][0]['surname'] ) ); ?>*</label>
		<input type="text" id="yith_mjpa_surname_id_INDEX" class="yith_mjpa_input_text"/>
		</div>
	</div>
</div>
