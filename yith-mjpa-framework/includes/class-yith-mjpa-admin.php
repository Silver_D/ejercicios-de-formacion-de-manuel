<?php
/*
 * This file belongs to the YITH MJPA framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Admin' ) ) {

	class YITH_MJPA_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 *
		 * Panel
		 *
		 * @var Panel page
		 */
		protected $panel_page = 'yith_mjpa_panel';
		private $_panel; // phpcs:ignore
		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Admin Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Admin constructor.
		 */
		private function __construct() {
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_MJPA_DIR_PATH . '/' . basename( YITH_MJPA_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'admin_init', array( $this, 'add_metabox' ), 10 );
			add_action( 'yith_framework_plugin_print_custom_field', array( $this, 'print_custom_field' ), 10, 1 );

		}
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings'      => __( 'Settings', 'yith-mjpa-plugin-framework' ),
				'testimonials'  => __( 'Testimonials', 'yith-mjpa-plugin-framework' ),
				'taxonomies'    => __( 'Taxonomies', 'yith-mjpa-plugin-framework' ),
				'taxonomiesdep' => __( 'Taxonomiesdep', 'yith-mjpa-plugin-framework' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH MJPA Plugin', // this text MUST be NOT translatable
				'menu_title'         => 'YITH MJPA Plugin', // this text MUST be NOT translatable
				'plugin_description' => __( 'Put here your plugin description', 'yith-mjpa-plugin-framework' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith-mjpa-plugin-framework',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_mjpa_plugin_panel',
				'admin-tabs'         => $admin_tabs,
				'plugin-url'         => YITH_MJPA_DIR_PATH,
				'options-path'       => YITH_MJPA_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel( $args );
		}
		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_MJPA_SLUG );
		}
		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_MJPA_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_MJPA_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}
		/**
		 * Add_metabox
		 *
		 * @return void
		 */
		public function add_metabox() {
			$args = array(
				'label'    => __( 'Metabox Label', 'yith-mjpa-plugin-framework' ),
				'pages'    => 'yith_testimonials',
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'settings' => array( // tab
						'label'  => __( 'Settings', 'yith-mjpa-plugin-framework' ),
						'fields' => array(
							'meta_text_rol'         => array(
								'label'   => __( 'Rol', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox rol field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'meta_text_company'     => array(
								'label'   => __( 'Company', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox text company field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'meta_text_url_company' => array(
								'label'   => __( 'Url Company', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox url company field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'meta_text_email'       => array(
								'label'   => __( 'Email', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox email field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
							'meta_custom_star'      => array(
								'label'   => __( 'Star', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox stars field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'custom',
								'action'  => 'yith_framework_plugin_print_custom_field',
								'private' => false,
								'std'     => '',
							),
							'meta_checkbox_vip'     => array(
								'label'   => __( 'VIP', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox VIP field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'checkbox',
								'private' => false,
								'std'     => '',
							),
							'meta_checkbox_badge'   => array(
								'label'   => __( 'Badge', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox badge field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'checkbox',
								'private' => true,
								'std'     => '',
							),
							'meta_text_badge'       => array(
								'label'   => __( 'Text badge', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox text badge field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
								'deps'    => array(
									'id'    => '_meta_checkbox_badge',
									'value' => 'yes',
									'type'  => 'hide',
								),
							),
							'meta_colorpicker'      => array(
								'label'   => __( 'Color Badge', 'yith-mjpa-plugin-framework' ),
								'desc'    => __( 'This is a description for the metabox color badge field.', 'yith-mjpa-plugin-framework' ),
								'type'    => 'colorpicker',
								'private' => false,
								'std'     => '',
								'deps'    => array(
									'id'    => '_meta_checkbox_badge',
									'value' => 'yes',
									'type'  => 'hide',
								),
							),
						),
					),
				),
			);

			$metabox1 = YIT_Metabox( 'yith_metabox_test' );
			$metabox1->init( $args );
		}
		/**
		 * Print_custom_field
		 *
		 * @param field mixed $field field.
		 * @return void
		 */
		public function print_custom_field( $field ) {
			$html  = "<div class='yith-test-plugin-custom-field-container' >";
			$html .= "<div class='yith-test-plugin-custom-field__field' >";
			$html .= "<input type='number' name='yit_metaboxes[meta_stars]' value='' />";
			$html .= '</div>';
			$html .= '</div>';
			echo( $html );
		}
	}
}
