<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PS_Frontend' ) ) {
	/**
	 * YITH_PS_Frontend
	 */
	class YITH_PS_Frontend {
		/**
		 * Main Instance
		 *
		 * @var YITH_PS_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PS_Frontend Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PS_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'add_frontend_styles' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_frontend_scripts' ) );
		}
		/**
		 * Add_frontend_scripts
		 *
		 * @return void
		 */
		public function add_frontend_scripts() {
			wp_register_script( 'script', YITH_MJPA_DIR_ASSETS_JS_URL_DATABASE_PLUGIN . '/frontend.js', array(), true );
			wp_enqueue_script( 'script' );
		}
		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_frontend_styles() {

			wp_register_style( 'style', YITH_MJPA_DIR_ASSETS_CSS_URL_DATABASE_PLUGIN . '/frontend.css', array(), true );
			wp_enqueue_style( 'style' );

		}

	}
}
