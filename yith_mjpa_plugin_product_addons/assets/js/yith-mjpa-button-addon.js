
jQuery(document).ready(function ($) {
    let index_order = [];
    $(function () {
        let_general_accordion_addon = function (value) {
            $('.js-accordion-title.accordion-title.index_' + value).on('click', function () {
                $(this).next().slideToggle(200);
                $(this).toggleClass('open', 200);
            });
        }
        add_new_option_general_addon = function (value) {
            let create_new_addon_field = $('#id_add_new_option_' + value);
            create_new_addon_field.on('click', function () {
                array_separator_id = $(this)[0].id.split('_')
                array_separator_length = array_separator_id.length
                actual_id = array_separator_id[array_separator_length - 1]
                let id_change = $(this)[0].parentNode;
                aux_separator = id_change.id.split('_');
                id_change.id = aux_separator[0] + '_' + actual_id;
                let clone = id_change.children[0].cloneNode(true);
                clone.classList.remove("mjpa_hidden")
                clone.getElementsByClassName('class_option_option')[0].name = clone.getElementsByClassName('class_option_option')[0].name + '_' + actual_id + '[]';
                clone.getElementsByClassName('class_option_price')[0].name = clone.getElementsByClassName('class_option_price')[0].name + '_' + actual_id + '[]';
                document.getElementById(id_change.id).insertBefore(clone, $(this)[0]);
                if (id_change.parentElement.getElementsByClassName('class_radio_price_type_free')[0].checked) {
                    hide_show_price_field_fixed(actual_id)
                } else {
                    hide_show_price_field(actual_id)
                }


            })
        }
        reset_order = function () {
            if (index_order.length > $("#_index_hidden").val()) index_order = [0]
            console.log(index_order)
        }
    })

    let addons = 0
    $('#wrap_addons_order').sortable({
        stop: function (e) {
            let order_addons_array = $.map($('#wrap_addons_order').find('.wrapp-accordion').not('.ui-sortable-placeholder,.mjpa_hidden'), function (value, index) {
                return [value];
            })
            index_order = [];
            order_addons_array.forEach(element => {
                index_sorted = element.children[0].classList[2].split('_')
                index_order.push(index_sorted[1]);
            });
            console.log(index_order)
            document.getElementById("_index_order").value = index_order;

        }
    });


    for (let index = 1; index < document.getElementsByClassName("class_select").length; index++) {
        show_hide_price_fields_select(document.getElementsByClassName("class_select")[index])
        let prepare_added_addons = $('#id_add_new_option_' + addons);
        prepare_added_addons.on('click', function () {
            array_separator_id = $(this)[0].id.split('_')
            array_separator_length = array_separator_id.length
            actual_id = array_separator_id[array_separator_length - 1]
            let id_change = $(this)[0].parentNode;
            aux_separator = id_change.id.split('_');
            id_change.id = aux_separator[0] + '_' + actual_id;
            let clone = id_change.children[0].cloneNode(true);
            clone.classList.remove("mjpa_hidden")
            clone.getElementsByClassName('class_option_option')[0].name = clone.getElementsByClassName('class_option_option')[0].name + '_' + actual_id + '[]';
            clone.getElementsByClassName('class_option_price')[0].name = clone.getElementsByClassName('class_option_price')[0].name + '_' + actual_id + '[]';
            document.getElementById(id_change.id).insertBefore(clone, $(this)[0]);
            hide_show_price_field(actual_id)
        })
        ++addons;

    }
    if (document.getElementById("_index_hidden")) {
        if (parseInt(document.getElementById("_index_hidden").value) !== 0) {
            for (let index = 0; index < parseInt(document.getElementById("_index_hidden").value); index++) {
                $('.js-accordion-title.accordion-title.index_' + index).on('click', function () {
                    $(this).next().slideToggle(200);
                    $(this).toggleClass('open', 200);
                });
            }
        }
    }

    $('#yith_mjpa_addon_id').on('click', function (event) {
        event.preventDefault();
        jQuery(function ($) {
            clone_node_base();
            add_index_title_addon(parseInt(document.getElementById("_index_hidden").value));
            fix_check(parseInt(document.getElementById("_index_hidden").value));
            document.getElementById("_index_hidden").value = ++document.getElementById("_index_hidden").value
        });
    })

})
function fix_check() {
    //no entiendo cómo es checked pero no se marca checked
    //revisar al final del desarrollo

}
function hide_show_price_field(actual_id) {

    for (let index = 1; index < document.getElementById('group_' + actual_id).getElementsByClassName('wrap-options').length; index++) {
        document.getElementById('group_' + actual_id).getElementsByClassName('class_option_price_show')[index].classList.remove('mjpa_hidden')
    }

}
function hide_show_price_field_fixed(actual_id) {
    console.log(document.getElementById('group_' + actual_id).getElementsByClassName('wrap-options').length)
    for (let index = 1; index < document.getElementById('group_' + actual_id).getElementsByClassName('wrap-options').length; index++) {
        document.getElementById('group_' + actual_id).getElementsByClassName('class_option_price_show')[index].classList.add('mjpa_hidden')
    }

}

function clone_node_base() {
    let clone = document.querySelector('.wrapp-accordion.mjpa_hidden.general_addons').cloneNode(true);
    clone.classList.remove("mjpa_hidden");
    document.getElementById("wrap_addons_order").appendChild(clone);
}

function add_index_title_addon(index_aux) {
    let undefined_addons = parseInt(document.getElementById("_index_total").value);
    let temp = null;
    if (undefined_addons != 0) {
        suma = index_aux + 1
        for (let index = 0; index < suma; index++) {
            if (document.getElementsByClassName("index_" + index)[0] == undefined) {
                temp = index
            }
        }
    }
    if (temp != null) {
        document.getElementsByClassName("accordion-title js-accordion-title")[index_aux + 1].classList.add("index_" + temp)
        change_fields(index_aux, temp)
        change_group(index_aux, temp)
    }
    else {
        document.getElementsByClassName("accordion-title js-accordion-title")[index_aux + 1].classList.add("index_" + index_aux)
        change_fields(index_aux, null)
        change_group(index_aux, null)
    }
}

function change_fields(index_aux, temp) {
    let fields = ['_checkbox_enable', 'class_checkbox_enable', 'class_name_field', 'class_textarea', 'class_select', 'class_price_field', 'class_free_characters_input', 'class_radio_price_type_character', 'class_radio_price_type_fixed', 'class_radio_price_type_free', 'add_new_option', 'class_onoff'];
    let value = (temp != null) ? temp : index_aux
    fields.forEach(element => {
        let node_field = document.getElementsByClassName(element)[index_aux + 1];
        node_field.id = node_field.id + '_' + value;
        node_field.name = node_field.name + '_' + value;
    });
    let_general_accordion_addon(value);
}
function change_group(index_aux, temp) {
    let node_field = document.getElementsByClassName("options_group wrap_options_select")[index_aux + 1];
    temp = (temp != null) ? temp : index_aux;
    node_field.id = node_field.id + temp;
    add_new_option_general_addon(temp)
}

function show_hide_price_fields(selectObjt) {
    aux_separator = selectObjt.children[0].children[0].name.split('_');
    aux_separator_length = aux_separator.length;
    total_addons_number = parseInt(aux_separator[aux_separator_length - 1]) - document.getElementById("_index_total").value
    aux_index2 = parseInt(aux_separator[aux_separator_length - 1])
    switch (document.getElementById('id_select_' + aux_index2).value) {
        case 'onoff':
            selectObjt.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_show_free')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('fixed_price_field_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
            }
            else {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            }
            break;
        case 'checkbox':
            selectObjt.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_show_free')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('fixed_price_field_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
            }
            else {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            }
            break;
        case 'select':
            selectObjt.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('wrap_options_select')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                hide_show_price_field(aux_index2);
            } else {
                hide_show_price_field_fixed(aux_index2);

            }
            break;
        case 'radio':
            selectObjt.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('wrap_options_select')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                hide_show_price_field(aux_index2);
            } else {
                hide_show_price_field_fixed(aux_index2);
            }
            break;
        case 'text':
            if (true === document.getElementsByClassName('class_radio_price_type_free')[total_addons_number + 1].checked) {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
                selectObjt.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            } else {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
                selectObjt.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.remove("mjpa_hidden")
            }
            selectObjt.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_price_per_character')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            break;
        case 'textarea':
            if (true === document.getElementsByClassName('class_radio_price_type_free')[total_addons_number + 1].checked) {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
                selectObjt.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            } else {
                selectObjt.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
                selectObjt.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.remove("mjpa_hidden")
            }
            selectObjt.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('class_price_per_character')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            break;
        default:
            break;
    }

}
//cambios en el select
function show_hide_price_fields_select(selectObjt) {
    aux_separator = selectObjt.name.split('_');
    aux_separator_length = aux_separator.length;
    total_addons_number = parseInt(aux_separator[aux_separator_length - 1]) - document.getElementById("_index_total").value
    aux_index2 = parseInt(aux_separator[aux_separator_length - 1])
    switch (document.getElementById('id_select_' + aux_index2).value) {
        case 'onoff':
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_show_free')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('fixed_price_field_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
            }
            else {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            }
            break;
        case 'checkbox':
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_show_free')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('fixed_price_field_class')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
            }
            else {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            }
            break;
        case 'select':
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('wrap_options_select')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")

            if (true === selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                hide_show_price_field(aux_index2);

            } else {
                hide_show_price_field_fixed(aux_index2);
            }
            break;
        case 'radio':
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('wrap_options_select')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_price_per_character')[0].classList.add("mjpa_hidden")
            if (true === selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_radio_price_type_fixed')[0].checked) {
                hide_show_price_field(aux_index2);
            } else {
                hide_show_price_field_fixed(aux_index2);
            }
            break;
        case 'text':
            if (true === document.getElementsByClassName('class_radio_price_type_free')[total_addons_number + 1].checked) {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            } else {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.remove("mjpa_hidden")
            }
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_price_per_character')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            break;
        case 'textarea':
            if (true === document.getElementsByClassName('class_radio_price_type_free')[total_addons_number + 1].checked) {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.add("mjpa_hidden")
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.add("mjpa_hidden")
            } else {
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('price_show_class')[0].classList.remove("mjpa_hidden")
                selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('free_characters_show_class')[0].classList.remove("mjpa_hidden")
            }
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('onnoff_show_class')[0].classList.add("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('class_price_per_character')[0].classList.remove("mjpa_hidden")
            selectObjt.parentElement.parentElement.parentElement.getElementsByClassName('wrap_options_select')[0].classList.add("mjpa_hidden")
            break;
        default:
            break;
    }
}
function trash_event_delete(event) {
    event.target.parentElement.parentElement.remove()
}
function delete_addon(event) {
    event.preventDefault();
    document.getElementById("_index_hidden").value = --document.getElementById("_index_hidden").value
    document.getElementById("_index_total").value = ++document.getElementById("_index_total").value
    reset_order()
    event.target.parentElement.parentElement.parentElement.remove()

}