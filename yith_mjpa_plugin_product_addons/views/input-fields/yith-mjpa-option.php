<?php
		global $post;
?>

<div id="<?php echo( esc_html( $args['group_option_class'] ) ); ?>" class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>">
	<div class="<?php echo( esc_html( $args['class_group_show'] ) ); ?>">
		<div class="<?php echo( esc_html( $args['class_field_wrap_option'] ) ); ?>">
			<label class="margin" for="<?php echo( esc_html( $args['id_field_option_name'] ) ); ?>" ><?php echo( esc_html( $args['label_field_option'] ) ); ?></label>
			<input value="" type="<?php echo( esc_html( $args['type_option'] ) ); ?>" name="<?php echo( esc_html( $args['name_field_option'] ) ); ?>" id="<?php echo( esc_html( $args['id_field_option_name'] ) ); ?>" class="<?php echo( esc_html( $args['class_field_option'] ) ); ?>">
		</div>
		<div class="class_option_price_show <?php echo( esc_html( $args['class_field_wrap_option'] ) ); ?>">
			<label class="margin" for="<?php echo( esc_html( $args['id_field_price'] ) ); ?>" ><?php echo( esc_html( $args['label_field_price'] ) ); ?></label>
			<input value="" type="<?php echo( esc_html( $args['type_option'] ) ); ?>" name="<?php echo( esc_html( $args['name_field_price'] ) ); ?>" id="<?php echo( esc_html( $args['id_field_price'] ) ); ?>" class="<?php echo( esc_html( $args['class_field_price'] ) ); ?>">
		</div>
		<div class="class_flex class_option_trash">
			<span class="dashicons dashicons-trash class_span_option_trash" onclick="trash_event_delete(event)"></span>
		</div>
	</div>
	<?php
	if ( ! empty( $options_stored ) ) {
		$options_length      = count( $options_stored );
		$price_stored_length = count( $price_stored );
		for ( $i = 0; $i < $options_length; $i++ ) {
			?>
				<div class="wrap-options">
					<div class="class_flex">
						<label class="margin" for="<?php echo( esc_html( $args['id_field_option_name'] ) ); ?>" ><?php echo( esc_html( $args['label_field_option'] ) ); ?></label>
						<input value="<?php echo( esc_html( $options_stored[ $i ] ) ); ?>" type="<?php echo( esc_html( $args['type_option'] ) ); ?>" name="<?php echo( esc_html( $args['id_field_option_name'] ) ) . '[]'; ?> " id="<?php echo( esc_html( $args['id_field_option_name'] ) ); ?>" class="<?php echo( esc_html( $args['class_field_option'] ) ); ?>">
					</div>
					<div class="class_flex class_option_price_show">
						<label class="margin" for="<?php echo( esc_html( $args['id_field_price'] ) ); ?>" ><?php echo( esc_html( $args['label_field_price'] ) ); ?></label>
						<input value=
						<?php
						if ( ( ! empty( $price_stored ) ) && ( $i <= $price_stored_length - 1 ) ) {

							if ( '' !== $price_stored[ $i ] ) {
								echo( esc_html( $price_stored[ $i ] ) );
							} else {
								echo( "''" );
							}
						} else {
							echo( "''" );}
						?>
						type="<?php echo( esc_html( $args['type_option'] ) ); ?>" name="<?php echo( esc_html( $args['id_field_price'] ) ) . '[]'; ?>" id="<?php echo( esc_html( $args['id_field_price'] ) ); ?>"  class="<?php echo( esc_html( $args['class_field_price'] ) ); ?>">
					</div>
					<div class="class_flex class_option_trash">
						<span class="dashicons dashicons-trash class_span_option_trash" onclick="trash_event_delete(event)"></span>
					</div>
				</div>
				<?php
		}
	}
	?>
	<a href="#" id="<?php echo( esc_html( $args['id_add_option'] ) ); ?>" name="<?php echo( esc_html( $args['id_add_option'] ) ); ?>" class="<?php echo( esc_html( $args['class_class_add_option'] ) ); ?>" >Add new option+</a>
</div>
