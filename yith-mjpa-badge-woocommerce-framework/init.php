<?php

/*
 * Plugin Name: YITH Plugin Badge WooCommerce Framework
 * Description: Skeleton for YITH Plugins
 * Version: 1.0.0
 * Author: Manuel Jesús Peraza Alonso
 * Author URI: https://yithemes.com/
 * Text Domain: yith-mjpa-text-domain
 */


! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	define( 'YITH_MJPA_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_MJPA_DIR_URL' ) ) {
	define( 'YITH_MJPA_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_URL' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_URL', YITH_MJPA_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_CSS_URL', YITH_MJPA_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_JS_URL', YITH_MJPA_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_MJPA_DIR_PATH' ) ) {
	define( 'YITH_MJPA_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_MJPA_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_MJPA_DIR_INCLUDES_PATH', YITH_MJPA_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_MJPA_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_MJPA_DIR_TEMPLATES_PATH', YITH_MJPA_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_MJPA_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_MJPA_DIR_VIEWS_PATH', YITH_MJPA_DIR_PATH . '/views' );
}
! defined( 'YITH_MJPA_FILE' ) && define( 'YITH_MJPA_FILE', __FILE__ );

! defined( 'YITH_MJPA_SLUG' ) && define( 'YITH_MJPA_SLUG', 'yith-woocommerce-auctions' );

! defined( 'YITH_MJPA_INIT' ) && define( 'YITH_MJPA_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_MJPA_SECRETKEY' ) && define( 'YITH_MJPA_SECRETKEY', 'aquiiraotracosa' );

if ( ! function_exists( 'yit_maybe_plugin_fw_loader' ) && file_exists( YITH_MJPA_DIR_PATH . 'plugin-fw/init.php' ) ) {
	require_once YITH_MJPA_DIR_PATH . 'plugin-fw/init.php';
}

yit_maybe_plugin_fw_loader( YITH_MJPA_DIR_PATH );
if ( ! function_exists( 'yith_plugin_registration_hook' ) ) {
	require_once 'plugin-fw/yit-plugin-registration-hook.php';
}
if ( ! function_exists( 'yit_deactive_free_version' ) ) {
	require_once 'plugin-fw/yit-deactive-plugin.php';
}

yit_deactive_free_version( 'MY_PLUGIN_FREE_INIT', plugin_basename( __FILE__ ) );

register_activation_hook( __FILE__, 'yith_plugin_registration_hook' );

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_mjpa_init_classes' ) ) {

	function yith_mjpa_init_classes() {

		load_plugin_textdomain( 'yith-mjpa-text-domain', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example
		require_once YITH_MJPA_DIR_INCLUDES_PATH . '/class-yith-mjpa-plugin-badge-woocommerce.php';

		if ( class_exists( 'YITH_MJPA_Badge_Woocommerce' ) ) {
			/*
			*	Call the main function
			*/
			yith_mjpa_badge_woocommerce();
		}
	}
}


add_action( 'plugins_loaded', 'yith_mjpa_init_classes', 11 );
