<?php
/*Incluidos dentro de functions.php*/

class segundo_widget extends WP_Widget {
    //declaración y setup del widget
    public function __construct(){
        $widget_ops=array(
            'classname' => 'mi_segundo_widget',
            'description' => 'Es mi segundo widget'
        );
        parent::__construct('second_widget', 'El mejor widgetx2', $widget_ops);
    }

    //backend display del widget
    function form($instance){
        $instance = wp_parse_args( (array) $instance, array('numero_post'=> 1));
        if ( isset( $instance[ 'numero_post' ] ) ) {
            $numero_post = $instance[ 'numero_post' ];
        }
        else {
            $numero_post = 1;
        }

        ?>
        <p>
        <label for="<?php echo $this->get_field_id( 'numero_post' ); ?>"><?php _e( 'Numero_post:' ); ?></label> 
        <input class="widefat" id="<?php echo $this->get_field_id( 'numero_post' ); ?>" name="<?php echo $this->get_field_name( 'numero_post' ); ?>" type="text" value="<?php echo esc_attr( $numero_post ); ?>" />
        </p>

        <?php 
        
    }

    //frontend display del widget
    function widget($args,$instance){
        $instance = wp_get_recent_posts(array('numberposts'=> $instance[ 'numero_post' ]));
        

        foreach ($instance as $value) {
            echo "<p>".$value['post_title']."</p>";
        }

    }

    //permite guardar 
    function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['numero_post'] = ( ! empty( $new_instance['numero_post'] ) ) ? strip_tags( $new_instance['numero_post'] ) : '';

        return $instance;
         
    }

 

}

add_action('widgets_init',function(){
    register_widget('segundo_widget');
});
