<?php
/*
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

 // HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions

 /**
  * Include templates
  *
  * @param $file_name name of the file you want to include.
  * @param array $args (array) (optional) Arguments to retrieve.
  */
if ( ! function_exists( 'yith_mjpa_get_template' ) ) {
	function yith_mjpa_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_MJPA_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


// Example of use
/*
yith_mjpa_get_template( '/frontend/testimonials.php', array(
	'testimonial_ids' => $testimonial_ids,
	'show_image'      => $args['show_image'],
	'hover_effect'    => $args['hover_effect'],
	'posts_number'    => $args['number']
) );
*/

/**
 * Include views
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_mjpa_get_view' ) ) {
	function yith_mjpa_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_MJPA_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


function my_custom_status_creation() {
	register_post_status(
		'yith_no_check',
		array(
			'label'                     => _x( 'Yith_No_Check', 'post' ),
			'label_count'               => _n_noop( 'Yith_No_Check <span class="count">(%s)</span>', 'Yith_No_Check <span class="count">(%s)</span>' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
		)
	);
	register_post_status(
		'yith_check',
		array(
			'label'                     => _x( 'Yith_Check', 'post' ),
			'label_count'               => _n_noop( 'Yith_Check <span class="count">(%s)</span>', 'Yith_Check <span class="count">(%s)</span>' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
		)
	);
}
add_action( 'init', 'my_custom_status_creation' );

function my_custom_status_add_in_quick_edit() {
	echo "<script>
	jQuery(document).ready( function() {
		jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"yith_no_check\">Yith_No_Check</option>' );
		jQuery( 'select[name=\"_status\"]' ).append( '<option value=\"yith_check\">Yith_Check</option>' );      
	}); 
	</script>";
}
add_action( 'admin_footer-edit.php', 'my_custom_status_add_in_quick_edit' );
function my_custom_status_add_in_post_page() {
	echo "<script>
	jQuery(document).ready( function() {        
		jQuery( 'select[name=\"post_status\"]' ).append( '<option value=\"yith_no_check\">Yith_No_Check</option>' );
		jQuery( 'select[name=\"post_status\"]' ).append( '<option value=\"yith_check\">Yith_Check</option>' );
	});
	</script>";
}
add_action( 'admin_footer-post.php', 'my_custom_status_add_in_post_page' );
add_action( 'admin_footer-post-new.php', 'my_custom_status_add_in_post_page' );
