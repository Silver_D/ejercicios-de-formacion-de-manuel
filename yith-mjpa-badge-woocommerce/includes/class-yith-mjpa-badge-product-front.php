<?php
/**
 * This file belongs to the YITH MJPA Plugin Skeleton
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Badge_Product_Front' ) ) {
	/**
	 * YITH_MJPA_Badge_Product_Front
	 */
	class YITH_MJPA_Badge_Product_Front {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Badge_Product_Front
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Badge_Product_Front Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_scripts' ) );

			add_action( 'woocommerce_product_thumbnails', array( $this, 'yith_mjpa_print_badge' ) );
			add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'yith_mjpa_print_badge_shop' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_mjpa_print_personalization' ) );
			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'yith_mjpa_add_to_cart_validation' ), 10, 3 );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'yith_mjpa_add_to_cart_new_item' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_mjpa_get_new_item_data' ), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_mjpa_create_new_order_line' ), 10, 4 );
			// add_action( 'wp_ajax_nopriv_yith_mjpa_call_ajax', array( $this, 'yith_mjpa_call_ajax' ) );
			// add_action( 'wp_ajax_yith_mjpa_call_ajax', array( $this, 'yith_mjpa_call_ajax' ) );

			// add_filter( 'woocommerce_product_get_price ', array( $this, 'yith_mjpa_custom_price' ), 99, 2 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_mjpa_custom_price' ), 99 );
		}
		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {
			if ( get_option( 'yith_mjpa_badge_position_shop' ) === 'top_left' ) {
				$position_right_shop = 60;
				$position_left_shop  = 0;
			} else {
				$position_right_shop = 0;
				$position_left_shop  = 60;
			}
			if ( get_option( 'yith_mjpa_badge_position_product' ) === 'top_left' ) {
				$position_right = 60;
				$position_left  = 0;
			} else {
				$position_right = 0;
				$position_left  = 60;
			}
			wp_register_style( 'front_badge', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-front-badge.css', array(), true );
			wp_enqueue_style( 'front_badge' );
			$custom_css = '.yith_mjpa_padding {
				padding-top: ' . get_option( 'yith_mjpa_padding_top', 20 ) . 'px;
				padding-bottom: ' . get_option( 'yith_mjpa_padding_bottom', 25 ) . 'px;
				padding-right: ' . get_option( 'yith_mjpa_padding_right', 25 ) . 'px;
				padding-left: ' . get_option( 'yith_mjpa_padding_left', 25 ) . 'px;
			}
			.yith_mjpa_border {
				border-style: ' . get_option( 'yith_mjpa_style', 'solid' ) . ';
				border-width: ' . get_option( 'yith_mjpa_weight', 1 ) . 'px;
				border-radius: ' . get_option( 'yith_mjpa_radius', 7 ) . 'px;
				border-color: ' . get_option( 'yith_mjpa_color_picker', '#d8d8d8' ) . ';
			}
			.yith_mjpa_badge_text_shop#badge_text_shop {
				right:' . $position_right_shop . '%;
				left:' . $position_left_shop . '%;
			}
			.yith_mjpa_badge_text#badge_text {
				right:' . $position_right . '%;
				left:' . $position_left . '%;
			}
			';
			wp_add_inline_style( 'front_badge', $custom_css );
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {
			global $post;
			wp_register_script( 'dynamic_characters', YITH_MJPA_DIR_ASSETS_JS_URL . '/mjpa-frontend-dynamic-characters.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'dynamic_characters' );
			// pasar por parámetros php a js.
			$id_product = $post->ID;
			if ( ! is_bool( wc_get_product( $id_product ) ) ) {
				$free_characters = wc_get_product( $id_product )->get_meta_data()[6]->get_data()['value'];
				$price_product   = wc_get_product( $id_product )->get_meta_data()[5]->get_data()['value'];
				$price_type      = wc_get_product( $id_product )->get_meta_data()[4]->get_data()['value'];
			} else {
				$free_characters = '';
				$price_product   = '';
				$price_type      = '';
			}

			wp_localize_script(
				'dynamic_characters',
				'dcms_vars',
				array(
					// 'ajaxurl'       => admin_url( 'admin-ajax.php' ),
					'price_product'   => $price_product,
					'price_type'      => $price_type,
					'free_characters' => $free_characters,
				)
			);
		}
		/**
		 * Yith_mjpa_print_badge
		 *
		 * @return void
		 */
		public function yith_mjpa_print_badge() {
			global $post;
			if ( get_post_meta( $post->ID, '_allow_badge', true ) === 'on' ) {
				yith_mjpa_get_template( '/frontend/badge-front.php' );
			}
		}
		/**
		 * Yith_mjpa_print_badge_shop
		 *
		 * @return void
		 */
		public function yith_mjpa_print_badge_shop() {
			global $post;
			if ( get_post_meta( $post->ID, '_allow_badge', true ) === 'on' ) {
				yith_mjpa_get_template( '/frontend/badge-front-shop.php' );
			}
		}
		/**
		 * Yith_mjpa_print_personalization
		 *
		 * @return void
		 */
		public function yith_mjpa_print_personalization() {
			global $post;
			if ( get_post_meta( $post->ID, '_allow_badge', true ) === 'on' ) {
				yith_mjpa_get_template( '/frontend/badge-personalization.php' );
			}
		}

		/**
		 * Yith_mjpa_add_to_cart_validation
		 *
		 * @param true mixed       $true validation passed.
		 * @param product_id mixed $product_id product_id.
		 * @param quantity mixed   $quantity quantity.
		 * @return true void
		 */
		public function yith_mjpa_add_to_cart_validation( $true, $product_id, $quantity ) {
			if ( empty( $_POST['yith_mjpa_input_personalize_id'] ) ) {
				$true = false;
				wc_add_notice( 'Your personalization is a required field.', 'error' );
			}
			return $true;
		}
		/**
		 * Yith_mjpa_add_to_cart_new_item
		 *
		 * @param cart_item_data mixed $cart_item_data cart_item_data.
		 * @param product_id mixed     $product_id product_id.
		 * @param variation_id mixed   $variation_id variation_id.
		 * @return cart_item_data void
		 */
		public function yith_mjpa_add_to_cart_new_item( $cart_item_data, $product_id, $variation_id ) {
			$personal_text = $_POST['yith_mjpa_input_personalize_id'];
			$type_of_pay   = get_post_meta( $product_id, '_radio_price_type', true );
			if ( 'free' !== $type_of_pay ) {
				$price           = get_post_meta( $product_id, '_price_text', true );
				$free_characters = get_post_meta( $product_id, '_free_characters_input', true );
			}
			$added_price;
			switch ( $type_of_pay ) {
				case 'free':
					$added_price = 0;
					break;
				case 'fixed_price':
					$added_price = $price;
					break;
				case 'price_per_character':
					$added_price = $price * ( strlen( $personal_text ) - $free_characters );
					break;
				default:
					$added_price = 0;
					break;
			}
			if ( isset( $_POST['yith_mjpa_input_personalize_id'] ) ) {
				$cart_item_data['yith_mjpa_input_personalize_id'] = sanitize_text_field( $_POST['yith_mjpa_input_personalize_id'] );
				$cart_item_data['added_price']                    = $added_price;
			}
			return $cart_item_data;
		}
		/**
		 * Yith_mjpa_get_new_item_data
		 *
		 * @param item_data mixed      $item_data item_data.
		 * @param cart_item_data mixed $cart_item_data cart_item_data.
		 * @return item_data void
		 */
		public function yith_mjpa_get_new_item_data( $item_data, $cart_item_data ) {
			if ( isset( $cart_item_data['yith_mjpa_input_personalize_id'] ) ) {

				$item_data[] = array(
					'key'   => 'Custom Text',
					'value' => wc_clean( $cart_item_data['yith_mjpa_input_personalize_id'] ),
				);
				$item_data[] = array(
					'key'   => 'Price added',
					'value' => wc_clean( $cart_item_data['added_price'] ),
				);
			}
			return $item_data;
		}

		/**
		 * Yith_mjpa_create_new_order_line
		 *
		 * @param item mixed          $item item.
		 * @param cart_item_key mixed $cart_item_key cart_item_key.
		 * @param values mixed        $values values.
		 * @param order mixed         $order order.
		 * @return void
		 */
		public function yith_mjpa_create_new_order_line( $item, $cart_item_key, $values, $order ) {
			if ( isset( $values['yith_mjpa_input_personalize_id'] ) ) {
				$item->add_meta_data(
					'Your name',
					$values['yith_mjpa_input_personalize_id'],
					true
				);
			}
			if ( isset( $values['added_price'] ) ) {
				$item->add_meta_data(
					'Added Price',
					$values['added_price'],
					true
				);
			}
		}

		/**
		 * Yith_mjpa_call_ajax
		 *
		 * @return void
		 */

		/*
		Public function yith_mjpa_call_ajax() {
			$args = array();
			$product_slug;
			$product_id;
			// cambiar todo el código para la siguiente vez, no utilizar este tipo de llamadas, localize script te permite parámetros.
			$result = array( wc_get_products( $args ) );
			foreach ( $result[0] as $key => $product ) {
				if ( $_POST['product_name'] === $product->get_slug() ) {
					$product_slug = $product->get_slug();
					$product_id   = $product->get_id();
				}
			}
			$type_of_pay = get_post_meta( $product_id, '_radio_price_type', true );
			if ( 'free' !== $type_of_pay ) {
				$price           = get_post_meta( $product_id, '_price_text', true );
				$free_characters = get_post_meta( $product_id, '_free_characters_input', true );
			}
			$product_name = isset( $_POST['product_name'] ) ? $_POST['product_name'] : false;
			if ( ! $product_name ) {
				wp_send_json( array( 'product_name' => __( 'product_name not received :(', 'wpduf' ) ) );
			} else {
				wp_send_json(
					array(
						'type_of_pay'     => $type_of_pay,
						'price'           => $price,
						'price'           => $price,
						'free_characters' => $free_characters,
						'wpduf',
					)
				);
			}
		}*/
		/**
		 * Yith_mjpa_custom_price
		 *
		 * @param cart_object mixed $cart_object cart_object.
		 * @return void
		 */
		public function yith_mjpa_custom_price( $cart_object ) {
			foreach ( WC()->cart->get_cart() as $key => $value ) {
				$added_price = $value['added_price'];
				$original_price        = floatval( $value['data']->get_price() );
				$value['data']->set_price( $original_price + $added_price );
			}
		}

	}
}
