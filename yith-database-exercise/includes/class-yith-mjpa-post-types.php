<?php
/**
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Post_Types' ) ) {
	/**
	 * YITH_MJPA_Post_Types
	 */
	class YITH_MJPA_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_MJPA_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'mjpa-skeleton';


		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Post_Types Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );

		}

		/**
		 * Setup the 'Skeleton' custom post type
		 */
		public function setup_post_type() {
			$args = array(
				'label'        => __( 'Skeleton', 'yith-plugin-skeleton' ),
				'description'  => __( 'Skeleton post type', 'yith-plugin-skeleton' ),
				'public'       => false,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => true,
				'supports'     => array( 'title', 'editor', 'author', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}

	}
}
