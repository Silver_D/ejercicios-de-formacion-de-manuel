console.log("estoy cargando")

jQuery(document).ready(function ($) {
    $('#wrap_addons_order').sortable();
    $(function () {
        prepare_saved_addons = function (index) {
            $("#yith_mjpa_addons_index_hidden_id").val(index)
            for (let index_for = 0; index_for < index; ++index_for) {
                let new_id = "yith_mjpa_addons_" + index_for;
                $("#" + new_id).children(".accordion-title.js-accordion-title").children(".wrap-title-checkbox").children(".yith_title_addon").on('click', function () {
                    $(this).parent().parent().next().slideToggle(200);
                    $(this).parent().parent().toggleClass('open', 200);
                });
            }

        }
        button_click_function = function (event) {
            event.preventDefault();
            let yith_mjpa_pre_value_index_hidden = parseInt($("#yith_mjpa_addons_index_hidden_id").val());

            jQuery(function ($) {
                yith_mjpa_clone_node_base_addon_variation();
                $("#yith_mjpa_addons_index_hidden_id").val(yith_mjpa_pre_value_index_hidden + 1);
            });
        }
        yith_mjpa_clone_node_base_addon_variation = function () {
            let index_hidden_val = $("#yith_mjpa_addons_index_hidden_id").val();
            clone = $("#yith_mjpa_addons_INDEX").clone();
            clone.removeClass('mjpa_hidden');
            let new_id = "yith_mjpa_addons_" + index_hidden_val;
            clone.attr("id", new_id);
            clone.html($(clone.html().replaceAll('{{INDEX}}', index_hidden_val)));
            $('#wrap_addons_order').append(clone);
            $("#" + new_id).children(".accordion-title.js-accordion-title").children(".wrap-title-checkbox").children(".yith_title_addon").on('click', function () {
                $(this).parent().parent().next().slideToggle(200);
                $(this).parent().parent().toggleClass('open', 200);
            });
            yith_mjpa_add_new_option_base(index_hidden_val);
        }
        show_hide_price_fields = function (event) {
            if (typeof (event) == 'number') {
                var index_addon = parseInt(event)
            } else {
                var index_addon = event.classList[2].slice(13, event.classList[2].length - 1)
            }
            let name = "#yith-mjpa-addons-select-" + index_addon;
            let selectObject = $(name);


            if (($(selectObject).val() == 'text') || ($(selectObject).val() == 'textarea')) {
                $("#group_" + index_addon).addClass("mjpa_hidden")
                $("#yith-mjpa-addons-onoff-wrapp-" + index_addon).addClass("mjpa_hidden")
                $("#yith-mjpa-addons-radio-price-per-character-" + index_addon).parent().removeClass("mjpa_hidden")
                if (($("#yith-mjpa-addons-radio-fixed-" + index_addon).prop('checked') == true) || ($("#yith-mjpa-addons-radio-price-per-character-" + index_addon).prop('checked') == true)) {
                    $("#yith-mjpa-addons-price-field-" + index_addon).parent().parent().removeClass("mjpa_hidden")
                    $("#yith-mjpa-addons-free-characters-input-" + index_addon).parent().parent().removeClass("mjpa_hidden")
                } else {
                    $("#yith-mjpa-addons-price-field-" + index_addon).parent().parent().addClass("mjpa_hidden")
                    $("#yith-mjpa-addons-free-characters-input-" + index_addon).parent().parent().addClass("mjpa_hidden")
                }
            } else {
                $("#yith-mjpa-addons-free-characters-input-" + index_addon).parent().parent().addClass("mjpa_hidden")
                $("#yith-mjpa-addons-radio-price-per-character-" + index_addon).parent().addClass("mjpa_hidden")
                if (($(selectObject).val() == 'onoff') || ($(selectObject).val() == 'checkbox')) {
                    $("#group_" + index_addon).addClass("mjpa_hidden")
                    $("#yith-mjpa-addons-onoff-wrapp-" + index_addon).removeClass("mjpa_hidden")
                    if (($("#yith-mjpa-addons-radio-fixed-" + index_addon).prop('checked') == true)) {
                        $("#yith-mjpa-addons-price-field-" + index_addon).parent().parent().removeClass("mjpa_hidden")
                    } else {
                        $("#yith-mjpa-addons-price-field-" + index_addon).parent().parent().addClass("mjpa_hidden")
                    }
                }
            } if (($(selectObject).val() == 'select') || ($(selectObject).val() == 'radio')) {
                $("#yith-mjpa-addons-onoff-wrapp-" + index_addon).addClass("mjpa_hidden")
                $("#group_" + index_addon).removeClass("mjpa_hidden")
                if (($("#yith-mjpa-addons-radio-fixed-" + index_addon).prop('checked') == true)) {
                    $(".class_option_price_show_" + index_addon).removeClass("mjpa_hidden")
                } else {
                    $(".class_option_price_show_" + index_addon).addClass("mjpa_hidden")
                }
            }
        }
        yith_mjpa_add_new_option = function (event) {
            if ($(event).prop("id")) {
                let index_addon = $(event).prop("id").slice(14, $(event).prop("id").length)
                let clone = $(".GHOST_" + index_addon).clone();
                clone.removeClass('GHOST_' + index_addon);
                clone.find("#id_field_option_GHOST").removeAttr("id")
                clone.find("#id_field_price_GHOST").removeAttr("id")
                clone.removeClass('mjpa_hidden');
                clone.insertBefore($('#id_add_option_' + index_addon));
            }
        }
        yith_mjpa_add_new_option_base = function (id) {
            let clone = $(".GHOST_" + id).clone();
            clone.removeClass('GHOST_' + id);
            clone.find("#id_field_option_GHOST").removeAttr("id")
            clone.find("#id_field_price_GHOST").removeAttr("id")
            clone.removeClass('mjpa_hidden');
            clone.insertBefore($('#id_add_option_' + id));
        }
        trash_event_delete = function (event) {
            $(event).parent().parent().remove()
        }
        delete_addon = function (event) {
            event.preventDefault();
            event.target.parentElement.parentElement.parentElement.remove()
        }

    })
    if (typeof attr !== 'undefined') {
        jQuery(function ($) {
            prepare_saved_addons(attr['index_post']);
            for (let index = 0; index < attr['index_post']; index++) {
                show_hide_price_fields(index);
            }

        });
    }
})
