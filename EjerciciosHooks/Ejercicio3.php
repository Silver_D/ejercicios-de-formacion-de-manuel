<?php

/*
Plugin Name: Ejercicio3
Plugin URI: 
Description: Ejercicio3
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/

/*
Primero Obtenemos los dos IDs de los dos autores, admin y Manuel, luego recogemos sus nickname, en el caso de Manuel 
ha sido necesario escribir textualmente ya que el nickname sólo es Manuel, y si usáramos display name no funcionaría. 
Luego comprobamos si el autor1 o 2 es igual al autor actual del post, en caso afirmativo se colocará el nombre del otro autor.

*/
function TwoPost($content) {
    if(!is_feed() && !is_home()) {
        $IdAutorPrimerPost=get_post(0)->post_author;
        $IdAutorSegundoPost=get_post(1)->post_author;
        $autor1= "Manuel Jesús Alonso";
        $autor2= get_the_author_meta('nickname',$IdAutorSegundoPost);
        error_log ( print_r(  $IdAutorPrimerPost, true ) );
        error_log ( print_r(  $IdAutorSegundoPost, true ) );
        if ($autor1 == get_the_author()) {
            $content.= $autor2;
        }
        if ($autor2 == get_the_author()) {
            $content.= $autor1;
        }
    }
    return $content;
}
add_filter ('the_content', 'TwoPost');



?>