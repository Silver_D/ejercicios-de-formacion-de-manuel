<?php
/**
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Badge_Woocommerce_Admin' ) ) {

	/**
	 * YITH_MJPA_Badge_Woocommerce_Admin
	 */
	class YITH_MJPA_Badge_Woocommerce_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Badge_Woocommerce_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		private $_panel;
		private $panel_page;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Badge_Woocommerce_Admin Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Badge_Woocommerce_Admin constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_order_item_name', array( $this, 'yith_mjpa_create_new_order_line_order_admin' ), 10, 2 );
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_MJPA_DIR_PATH . '/' . basename( YITH_MJPA_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 3 );
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			add_action( 'yith_test_plugin_print_custom_field', array( $this, 'print_custom_field' ), 10, 1 );
			// add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
			add_action( 'admin_init', array( $this, 'add_metabox' ), 10 );
		}
		/**
		 * Add_Admin_Styles
		 *
		 * @return void
		 */
		public function add_input_styles() {
			wp_register_style( 'yith_mjpa_admin_css', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-admin.css', array( 'yith-plugin-fw-fields' ), YITH_MJPA_VERSION, false );
			wp_enqueue_style( 'yith_mjpa_admin_css' );
			// wp_register_style( 'check', YITH_MJPA_DIR_ASSETS_CSS_URL . '/check.css', array(), YITH_MJPA_VERSION, false );
			// wp_enqueue_style( 'check' );
		}
		/**
		 * Add_Admin_Scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {
			wp_register_script( 'yith_mjpa_admin_js', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-admin.js', array( 'yith-plugin-fw-fields' ), YITH_MJPA_VERSION, true );
			wp_enqueue_script( 'yith_mjpa_admin_js' );
			wp_register_style( 'yith_mjpa_admin_css', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-admin.css', array( 'yith-plugin-fw-fields' ), YITH_MJPA_VERSION, false );
			wp_enqueue_style( 'yith_mjpa_admin_css' );

		}
		/**
		 * Yith_mjpa_create_new_order_line_order_admin
		 *
		 * @param product_name mixed $product_name product_name.
		 * @param item mixed         $item item.
		 * @return product_name void
		 */
		public function yith_mjpa_create_new_order_line_order_admin( $product_name, $item ) {
			if ( isset( $item['yith_mjpa_input_personalize_id'] ) ) {
				$product_name .= sprintf(
					'<p>%s: %s</p>',
					'Your name',
					esc_html( $item['yith_mjpa_input_personalize_id'] )
				);
			}
			return $product_name;
		}
		/**
		 * Action Links
		 * add the action links to plugin admin page
		 *
		 * @param array $links Plugin links.
		 *
		 * @return  array
		 * @use     plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			return yith_add_action_links( $links, $this->panel_page, true, YITH_MJPA_SLUG );
		}
		public function add_metabox() {
			$args = array(
				'label'    => __( 'Metabox Label', 'your-plugin-text-domain' ),
				'pages'    => 'post',
				'context'  => 'normal',
				'priority' => 'default',
				'tabs'     => array(
					'settings' => array( // tab
						'label'  => __( 'Settings', 'your-plugin-text-domain' ),
						'fields' => array(
							'meta_text' => array(
								'label'   => __( 'Text test', 'your-plugin-text-domain' ),
								'desc'    => __( 'This is a description for the metabox test field.', 'your-plugin-text-domain' ),
								'type'    => 'text',
								'private' => false,
								'std'     => '',
							),
						),
					),
				),
			);

			$metabox1 = YIT_Metabox( 'yith_metabox_test' );
			$metabox1->init( $args );
		}
		/**
		 * Adds action links to plugin admin page
		 *
		 * @param array    $row_meta_args Row meta args.
		 * @param string[] $plugin_meta   An array of the plugin's metadata, including the version, author, author URI, and plugin URI.
		 * @param string   $plugin_file   Path to the plugin file relative to the plugins directory.
		 *
		 * @return array
		 */
		public function plugin_row_meta( $row_meta_args, $plugin_meta, $plugin_file ) {
			if ( YITH_MJPA_INIT === $plugin_file ) {
				$row_meta_args['slug']       = YITH_MJPA_SLUG;
				$row_meta_args['is_premium'] = true;
			}

			return $row_meta_args;
		}
		public function register_panel() {
			if ( ! empty( $this->_panel ) ) {
				return;
			}

			$admin_tabs = array(
				'settings' => __( 'Settings', 'yith-mjpa-text-domain' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Test Plugin', // this text MUST be NOT translatable
				'menu_title'         => 'YITH Test Plugin', // this text MUST be NOT translatable
				'plugin_description' => __( 'Put here your plugin description', 'yith-mjpa-text-domain' ),
				'capability'         => 'manage_options',
				'class'              => yith_set_wrapper_class(),
				'parent'             => 'yith-mjpa-text-domain',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => 'yith_test_plugin_panel',
				'admin-tabs'         => $admin_tabs,
				'options-path'       => YITH_MJPA_DIR_PATH . 'plugin-options',
			);

			$this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );

		}
		/**
		 * Print_custom_field
		 *
		 * @param field  mixed $field field.
		 * @return void
		 */
		public function print_custom_field( $field ) {
			$value          = ! ! $field['value'] && is_array( $field['value'] ) ? $field['value'] : array();
			$name           = $field['name'];
			$padding_top    = isset( $value['padding_top'] ) ? $value['padding_top'] : 0;
			$padding_bottom = isset( $value['padding_bottom'] ) ? $value['padding_bottom'] : 0;
			$padding_right  = isset( $value['padding_right'] ) ? $value['padding_right'] : 0;
			$padding_left   = isset( $value['padding_left'] ) ? $value['padding_left'] : 0;

			$html  = "<div class='yith-mjpa-text-domain-custom-field-container' >";
			$html .= "<div class='yith-mjpa-text-domain-custom-field__field' >";
			$html .= '<label>' . __( 'Top', 'yith-mjpa-text-domain' ) . '</label>';
			$html .= "<input class='yith_mjpa_size' type='number' name='{$name}[padding_top]' value='{$padding_top}' />";
			$html .= '<label>' . __( 'Bottom', 'yith-mjpa-text-domain' ) . '</label>';
			$html .= "<input class='yith_mjpa_size' type='number' name='{$name}[padding_bottom]' value='{$padding_bottom}' />";
			$html .= '<label>' . __( 'Right', 'yith-mjpa-text-domain' ) . '</label>';
			$html .= "<input class='yith_mjpa_size' type='number' name='{$name}[padding_right]' value='{$padding_right}' />";
			$html .= '<label>' . __( 'Left', 'yith-mjpa-text-domain' ) . '</label>';
			$html .= "<input class='yith_mjpa_size' type='number' name='{$name}[padding_left]' value='{$padding_left}' />";
			$html .= '</div>';
			$html .= '</div>';
			echo $html;
		}
	}
}
