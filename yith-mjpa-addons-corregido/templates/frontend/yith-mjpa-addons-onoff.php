<?php
$name_id_field        = 'onoff_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];
$string               = $args[0]['textarea_field'];

if ( ! isset( $args[0]['onoff_field'] ) ) {
	$args[0]['onoff_field'] = 'off';
}
?>

<div>
<?php

if ( 'on' === $args[0]['onoff_field'] ) {
	echo( '<div class="yith_mjpa_wrap_onoff"><label class="switch"><input checked name="' . esc_html( $name_id_field ) . '" class="yith_mjpa_input_onoff yith_price_addon" type="checkbox" name="' . esc_html( $name_id_field ) . '" id="' . esc_html( $name_id_field ) . '" value="' . esc_html( $string ) . '"><span class="slider round"></span>  </label><div class="yith_mjpa_text_onoff_container"><span class="yith_mjpa_text_onoff">' . esc_html( $string ) . '</span></div></div>' );
} else {
	echo( '<div class="yith_mjpa_wrap_onoff"><label class="switch"><input name="' . esc_html( $name_id_field ) . '" class="yith_mjpa_input_onoff yith_price_addon" type="checkbox" name="' . esc_html( $name_id_field ) . '" id="' . esc_html( $name_id_field ) . '" value="' . esc_html( $string ) . '"><span class="slider round"></span>  </label><div class="yith_mjpa_text_onoff_container"><span class="yith_mjpa_text_onoff">' . esc_html( $string ) . '</span></div></div>' );
}


if ( 'fixed_price' === $args[0]['radio_field'] && ( 'on' === $args[0]['onoff_field'] ) ) {
	echo( '<p class="yith_mjpa_price_onoff yith_price_addon_value">+' . esc_html( $args[0]['price_field'] ) . '.00$</p>' );
} else {
	echo( '<p class="yith_mjpa_price_onoff yith_price_addon_value">+0.00$</p>' );
}
?>
</div>
