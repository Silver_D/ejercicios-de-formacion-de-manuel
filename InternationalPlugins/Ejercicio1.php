<?php

/*
Plugin Name: Ejercicio1-International
Plugin URI: 
Description: Ejercicio1
Version: 1.0
Author: Manuel
Text Domain: languages-ejercicio
Domain Path: /languages
*/
/*
Añadimos la función insertaNotaFinal al hook the_content, comprobamos si no estás en la interfaz de homr ni en la página de entradas de tu blog

*/
/**
 * Proceso para llevar a cabo una traducción.
 *Se tiene que ejecutar desde la raíz del proyecto asíq eu no entres a languages porque no leerá las cadenas
 * 1º Generar un archivo .pot mediante el siguiente comando en {vagrant ssh} ->  wp i18n make-pot . languages/('textdomain').pot
 * 2ºincluir el archivo de traducción mediante la función "my_plugin_load_plugin_textdomain".
 * 3ºIncluir las cadenas a traducir mediante los métodos--> _x(),__(),_e()
 * 4ºUtilizar Poedit para incluir el .po
 * 5ºModificar cabecera
 * Notas:
 * Siempre se debe actualizar el archivo .pot ya que es estático
 */

function my_plugin_load_plugin_textdomain() {
    load_plugin_textdomain( 'languages-ejercicio', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'my_plugin_load_plugin_textdomain' );


function insertaNotaFinal($content) {
    if(!is_feed() && !is_home()) {
        $content.= __('This translation is wonderful', 'languages-ejercicio');
        $content.= "<h1>" . __('This translation is wonderful', 'languages-ejercicio') . "</h1>";
    }
    return $content;
}
add_filter ('the_content', 'insertaNotaFinal');

?>