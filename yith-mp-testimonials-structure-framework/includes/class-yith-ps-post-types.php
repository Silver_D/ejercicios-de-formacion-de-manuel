<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PS_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PS_Post_Types' ) ) {

	class YITH_PS_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_PS_Post_Types
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Post type name
		 *
		 * @var YITH_PS_Post_Types
		 * @since 1.0
		 * @access public
		 */
		public static $post_type = 'yith_testimonials';


		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PS_Post_Types Main instance
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_PS_Post_Types constructor.
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'setup_post_type' ) );

		}

		/**
		 * Setup the 'Skeleton' custom post type
		 */
		public function setup_post_type() {
			$labels = array(
				'name'               => __( 'testimonials', 'yith-plugin-skeleton' ),
				'singular_name'      => __( 'testimony', 'yith-plugin-skeleton' ),
				'add_new'            => __( 'Add New testimony', 'yith-plugin-skeleton' ),
				'add_new_item'       => __( 'Add New testimony', 'yith-plugin-skeleton' ),
				'edit_item'          => __( 'Edit testimony', 'yith-plugin-skeleton' ),
				'new_item'           => __( 'Add New testimony', 'yith-plugin-skeleton' ),
				'view_item'          => __( 'View testimony', 'yith-plugin-skeleton' ),
				'search_items'       => __( 'Search testimony', 'yith-plugin-skeleton' ),
				'not_found'          => __( 'No testimony found', 'yith-plugin-skeleton' ),
				'not_found_in_trash' => __( 'No testimonials found in trash', 'yith-plugin-skeleton' ),
			);

			$args = array(
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => null,
				'menu_icon'          => 'dashicons-calendar-alt',
				'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
				'capability_type'    => self::$post_type,
				'capabilities'       => array(
					'delete_post'            => 'delete_testimonial',
					'edit_post'              => 'edit_testimonial',
					'read_post'              => 'read_testimonial',
					'read_private_posts'     => 'read_private_testimonials',
					'delete_others_posts'    => 'delete_others_testimonials',
					'delete_posts'           => 'delete_testimonials',
					'delete_post'            => 'delete_testimonial',
					'delete_published_posts' => 'delete_published_testimonials',
					'edit_others_posts'      => 'edit_others_testimonials',
					'edit_posts'             => 'edit_testimonials',
					'edit_published_posts'   => 'edit_published_testimonials',
					'publish_posts'          => 'publish_testimonials',
				),
			);

			register_taxonomy(
				'category',
				array( self::$post_type ),
				array(
					'hierarchical' => true,
					'labels'       => $labels,
					'show_ui'      => true,
					'query_var'    => true,
					'rewrite'      => array( 'slug' => 'genero' ),
				)
			);
			register_taxonomy(
				'police',
				array( self::$post_type ),
				array(
					'hierarchical' => false,
					'labels'       => $labels,
					'show_ui'      => true,
					'query_var'    => true,
					'rewrite'      => array( 'slug' => 'genero' ),
				)
			);

			register_post_type( self::$post_type, $args );
		}

	}
}
