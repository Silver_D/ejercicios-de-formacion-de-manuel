<?php
/*
 * This file belongs to the YITH MJPA framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Ticket' ) ) {
	class YITH_MJPA_Ticket {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Ticket
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
		/**
		 * Main Admin Instance
		 *
		 * @var YITH_MJPA_Plugin_Ticket_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_MJPA_Plugin_Ticket_Frontend
		 * @since 1.0
		 */
		public $frontend = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Ticket Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6
		}

		/**
		 * YITH_MJPA_Ticket constructor.
		 */
		private function __construct() {
			add_action( 'plugins_loaded', array( $this, 'plugin_fw_loader' ), 15 );
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );

			$require = apply_filters(
				'yith_mjpa_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-mjpa-custom-post-type.php',
						 // 'includes/class-yith-mjpa-ajax.php',
						 // 'includes/class-yith-mjpa-compatibility.php',
						 // 'includes/class-yith-mjpa-other-class.php',
					),
					'admin'    => array(
						'includes/class-yith-mjpa-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-mjpa-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			/*
				Here set any other hooks ( actions or filters you'll use on this class)
			*/

			// Finally call the init function
			$this->init();

		}
		public function plugin_fw_loader() {
			if ( ! defined( 'YIT_CORE_PLUGIN' ) ) {
				global $plugin_fw_data;
				if ( ! empty( $plugin_fw_data ) ) {
					$plugin_fw_file = array_shift( $plugin_fw_data );
					require_once $plugin_fw_file;
				}
			}
		}

		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_MJPA_DIR_PATH . $class ) ) {
						require_once YITH_MJPA_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 **/
		public function init_classes() {
			YITH_MJPA_Custom_Post_Type::get_instance();
			// $this->function = YITH_MJPA_Other_Class::get_instance();
			// $this->ajax = YITH_MJPA_Ajax::get_instance();
			// $this->compatibility = YITH_MJPA_Compatibility::get_instance();
		}
		/**
		 * Register plugins for activation tab
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_MJPA_INIT, YITH_MJPA_SECRETKEY, YITH_MJPA_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_MJPA_SLUG, YITH_MJPA_INIT );
			}
		}

		/**
		 * Function init()
		 *
		 * Instance the admin or frontend classes
		 *
		 * @author Manuel Jesús Peraza Alonso
		 * @since  1.0
		 * @return void
		 * @access protected
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_MJPA_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_MJPA_Frontend::get_instance();
			}
		}

	}
}
/**
 * Get the YITH_MJPA_Ticket instance
 *
 * @return YITH_MJPA_Ticket
 */
if ( ! function_exists( 'yith_mjpa_ticket' ) ) {
	function yith_mjpa_ticket() {
		return YITH_MJPA_Ticket::get_instance();
	}
}
