<?php
/**
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package  WordPress
 */

// HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions.

/**
 * Include templates
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_ps_get_template' ) ) {
	/**
	 * Yith_ps_get_template
	 *
	 * @param mixed $file_name string file name.
	 * @param  mixed $args arguments.
	 * @return void
	 */
	function yith_ps_get_template( $file_name, $args = array(), $args_users = array() ) {
		extract( $args );
		$full_path = YITH_MJPA_DIR_TEMPLATES_PATH_DATABASE_PLUGIN . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}


/**
 * Include views
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_ps_get_view' ) ) {
	/**
	 * Yith_ps_get_view
	 *
	 * @param  mixed $file_name file name.
	 * @param  mixed $args arguments.
	 * @return void
	 */
	function yith_ps_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_MJPA_DIR_VIEWS_PATH_DATABASE_PLUGIN . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
require_once YITH_MJPA_DIR_INCLUDES_PATH_DATABASE_PLUGIN . '/user-form.php';


add_filter( 'cron_schedules', 'create_cron_job_interval' );

/**
 * Ejemplo_Crear_intervalo_cron_job
 *
 * @param  mixed $schedules schedules.
 * @return array void
 */
function create_cron_job_interval( $schedules ) {
	$schedules['six_hours'] = array(
		'interval' => 21600,
		'display'  => 'cada sesenta segundos',
	);
	return $schedules;
}

add_action( 'init', 'diary_notification_email' );


/**
 * Diary_notification_email
 *
 * @return void
 */
function diary_notification_email() {
	if ( ! wp_next_scheduled( 'notify_users_daily' ) ) {
		wp_schedule_event(
			time(),
			'six_hours',
			'notify_users_daily'
		);
	}
}

add_action( 'notify_users_daily', 'enviar_email' );

/**
 * Enviar_email
 *
 * @return void
 */
function enviar_email() {
	global $wpdb;
	$my_emails = $wpdb->get_results( 'SELECT email FROM wp_yith_raffle ', 'ARRAY_N' );

	foreach ( $my_emails as $key => $value ) {
		wp_mail(
			$value,
			'Mandando nuevo correo con cron',
			'no olvides la chaqueta'
		);
	}
}
