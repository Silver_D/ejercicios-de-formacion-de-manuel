jQuery(document).ready(function ($) {
    if ($('.GHOST').length !== 0) {
        let clone = $('.GHOST').clone()
        clone.removeClass('GHOST')
        clone.removeClass('mjpa_hidden')
        $(clone).find('#yith_mjpa_name_id_INDEX').attr('name', "yith_mjpa_name[] ");
        $(clone).find('#yith_mjpa_surname_id_INDEX').attr('name', "yith_mjpa_surname[] ");
        clone.html($(clone.html().replaceAll('INDEX', 1)))
        $('#yith_mjpa_wrapp_all_tickets').append(clone);
    }
    if (($('#yith_mjpa_wrapp_all_tickets').children().length - 1) < parseInt($('.input-text.qty.text').val())) {
        console.log(($('#yith_mjpa_wrapp_all_tickets').children().length - 1))
        console.log(parseInt($('.input-text.qty.text').val()))
        $('#yith_mjpa_wrapp_all_tickets').next().find('.input-text.qty.text').val(1)
    }

    $(".input-text.qty.text").change(function () {
        let yith_mjpa_input_index_cart = parseInt($('.input-text.qty.text').val());
        //let yith_mjpa_length_tickets = $('#yith_mjpa_wrapp_all_tickets').children().length - 1;
        while (($('#yith_mjpa_wrapp_all_tickets').children().length - 1) !== yith_mjpa_input_index_cart) {
            if (($('#yith_mjpa_wrapp_all_tickets').children().length - 1) < yith_mjpa_input_index_cart) {
                let clone = $('.GHOST').clone()
                clone.removeClass('GHOST')
                clone.removeClass('mjpa_hidden')
                $(clone).find('#yith_mjpa_name_id_INDEX').attr('name', "yith_mjpa_name[] ");
                $(clone).find('#yith_mjpa_surname_id_INDEX').attr('name', "yith_mjpa_surname[] ");
                clone.html($(clone.html().replaceAll('INDEX', ($('#yith_mjpa_wrapp_all_tickets').children().length))))
                $('#yith_mjpa_wrapp_all_tickets').append(clone);
            } else {
                $('#yith_mjpa_wrapp_all_tickets').children().last().remove()
            }
        }
    })
    $(function () {
        function runEffect_title() {
            $(".yith_title_product_content").toggle();
        };
        $("#myInput").keyup(function () {
            // Declare variables
            var input, filter, li;
            input = $('#myInput');
            filter = input.val();
            li = $('.yith_mjpa_post_id');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i];
                txtValue = a.textContent || a.innerText;
                if (txtValue.indexOf(filter) > -1) {
                    $(li[i]).parent().show()
                } else {
                    $(li[i]).parent().hide()
                    $(li[i]).parent().next().hide()
                }
            }
        });
        // Set effect from select menu value
        $(".dispatcher").on("click", function () {
            $(this).parent().next().toggle();
        });
        $("#yith_title_product").on("click", function () {
            runEffect_title();
        });

    });
    jQuery('.yith_mjpa_button.dashicons.dashicons-thumbs-up').on('click', function () {
        let yith_mjpa_post_id_js = $(this).attr('id')
        jQuery.ajax({
            type: "POST",
            url: attr_ajax.ajax_url, // Pon aquí tu URL
            data: {
                action: "ticket_variation",
                yith_mjpa_post_id: yith_mjpa_post_id_js
            },
            error: function (response) {
                console.log(response);
            },
            success: function (response) {
                if ($('#icon' + yith_mjpa_post_id_js)[0].classList.contains('yith_mjpa_not_checked')) {
                    $('#icon' + yith_mjpa_post_id_js)[0].classList.add('yith_mjpa_checked')
                    $('#icon' + yith_mjpa_post_id_js)[0].classList.remove('yith_mjpa_not_checked')
                } else {
                    if ($('#icon' + yith_mjpa_post_id_js)[0].classList.contains('yith_mjpa_checked')) {
                        $('#icon' + yith_mjpa_post_id_js)[0].classList.add('yith_mjpa_not_checked')
                        $('#icon' + yith_mjpa_post_id_js)[0].classList.remove('yith_mjpa_checked')
                    }
                }
            }
        })

    });
})