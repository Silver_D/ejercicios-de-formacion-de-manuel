<?php
$settings = array(
	'settings' => array(
		'general-options'               => array(
			'title' => __( 'General Options', 'yith-mjpa-text-domain' ),
			'type'  => 'title',
			'desc'  => '',
		),
		'yith-framework-padding-custom' => array(
			'id'        => 'yith_mjpa_id_custom_field',
			'name'      => __( 'Padding:', 'yith-mjpa-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'custom',
			'action'    => 'yith_test_plugin_print_custom_field',
			'desc'      => __( 'This is a custom woocommerce field', 'yith-mjpa-text-domain' ),
		),
		'yith-framework-style-select'   => array(
			'id'        => 'yith_mjpa_id_select_field',
			'name'      => __( 'Style:', 'yith-mjpa-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'select',
			'class'     => 'wc-enhanced-select',
			'options'   => array(
				'option1' => 'Option 1',
				'option2' => 'Option 2',
			),
			'desc'      => __( 'This is a select woocommerce field', 'yith-mjpa-text-domain' ),
		),
		'yith-framework-colorpicker'    => array(
			'id'        => 'yith_mjpa_id_colorpicker_field',
			'name'      => __( 'Color Picker:', 'yith-mjpa-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'colorpicker',
			'default'   => '#000000',
			'desc'      => __( 'This is a color-picker woocommmerce field', 'yith-mjpa-text-domain' ),
		),
		'yith-framework-number'         => array(
			'id'        => 'yith_mjpa_id_number_field',
			'name'      => __( 'Radius:', 'yith-mjpa-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'number',
			'min'       => 0,
			'max'       => 100,
			'step'      => 1,
			'default'   => 0,
			'desc'      => __( 'This is a number woocommerce field', 'yith-mjpa-text-domain' ),
		),
		'yith-framework-radio-shop'     => array(
			'id'        => 'yith_mjpa_id_radio_shop_field',
			'name'      => __( 'Badge Position Shop:', 'yith-mjpa-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'default'   => 'top_left',
			'options'   => array(
				'top_left'  => 'Top Left',
				'top_right' => 'Top Right',
			),
			'desc'      => __( 'This is a radio woocommerce field', 'yith-mjpa-text-domain' ),
		),
		'yith-framework-radio-product'  => array(
			'id'        => 'yith_mjpa_id_radio_product_field',
			'name'      => __( 'Badge Position Product:', 'yith-mjpa-text-domain' ),
			'type'      => 'yith-field',
			'yith-type' => 'radio',
			'default'   => 'top_left',
			'options'   => array(
				'top_left'  => 'Top Left',
				'top_right' => 'Top Right',
			),
			'desc'      => __( 'This is a radio woocommerce field', 'yith-mjpa-text-domain' ),
		),
		'general-options-end'           => array(
			'type' => 'sectionend',
			'id'   => 'yith-wcbk-general-options',
		),

		'section-2-options'             => array(
			'title' => __( 'Section 2', 'yith-mjpa-text-domain' ),
			'type'  => 'title',
		),

		// SECTION 2 Options...

		'section-2-options-end'         => array(
			'type' => 'sectionend',
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
