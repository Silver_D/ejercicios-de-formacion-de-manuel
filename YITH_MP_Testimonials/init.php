<?php

/*
Plugin Name: YITH_MP_Testimonials
Plugin URI: 
Description: Ejercicios de formación.
Version: 1.0
Author: Manuel
Author URI: 
Text Domain: YITH-mp-testimonials-language
Domain Path: /languages
License: 
*/

/**
 * Adding .pot file. 
 */
/*




//error_log ( print_r(  basename( dirname( __FILE__ ), true ) ));
function my_plugin_load_plugin_textdomain() {
    load_plugin_textdomain( 'YITH-mp-testimonials-language', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'my_plugin_load_plugin_textdomain' );

/**
 * Adding Scripts 
 */

function add_theme_scripts() {
    if (post_type_exists( "yith_testimonials" ) ) {
      wp_register_script('script', plugin_dir_url( __FILE__ ) . "assets/index.js");
      wp_register_style('style', plugin_dir_url( __FILE__ ) . "assets/index.css" );
      wp_enqueue_style( 'style' );
      wp_enqueue_script( 'script');
    }
  }
  function add_admin_scripts() {
      wp_enqueue_style( 'wp-color-picker' );
      wp_enqueue_script( 'color_picker_script', plugin_dir_url( __FILE__ ) . "assets/color_picker.js", array( 'wp-color-picker' ), false, true );
      wp_register_script('script', plugin_dir_url( __FILE__ ) . "assets/index.js");
      wp_register_style('admin', plugin_dir_url( __FILE__ ) . "assets/admin.css" );
      wp_enqueue_style( 'admin' );
      wp_enqueue_script( 'script');
      wp_enqueue_script( 'color_picker_script');
  }


  add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
  add_action( 'admin_enqueue_scripts', 'add_admin_scripts' );
  add_filter('autoptimize_filter_imgopt_lazyload_js_noptimize','__return_false');
  
/////////////////////////////////////////////////////////////////////

! defined( 'ABSPATH' ) && exit;   //Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_PS_VERSION' ) ) {
	define( 'YITH_PS_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PS_DIR_URL' ) ) {
	define( 'YITH_PS_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PS_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PS_DIR_ASSETS_URL', YITH_PS_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PS_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PS_DIR_ASSETS_CSS_URL', YITH_PS_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PS_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PS_DIR_ASSETS_JS_URL', YITH_PS_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PS_DIR_PATH' ) ) {
	define( 'YITH_PS_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PS_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PS_DIR_INCLUDES_PATH', YITH_PS_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PS_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PS_DIR_TEMPLATES_PATH', YITH_PS_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PS_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PS_DIR_VIEWS_PATH', YITH_PS_DIR_PATH . '/views' );
}


/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_ps_init_classes' ) ) {

	function yith_ps_init_classes() {

		load_plugin_textdomain( 'YITH-mp-testimonials-language', false, basename( dirname( __FILE__ ) ) . '/languages' );

      require_once 'includes/YITH-mp-print-testimonial.php';
      require_once 'includes/YITH-mp-testimonial-post-type.php';

		/*
		*	Call the main function
		*/

		//yith_ps_plugin_skeleton();
	}
}


add_action( 'plugins_loaded', 'yith_ps_init_classes', 11 );