<?php

/*
Plugin Name: Ejercicio1
Plugin URI: 
Description: Ejercicio1
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/
/*
Añadimos la función insertaNotaFinal al hook the_content, comprobamos si no estás en la interfaz de homr ni en la página de entradas de tu blog

*/
function insertaNotaFinal($content) {
    if(!is_feed() && !is_home()) {
        $content.= "<h4>Este post fue escrito por Manuel</h4>";
    }
    return $content;
}
add_filter ('the_content', 'insertaNotaFinal');

?>