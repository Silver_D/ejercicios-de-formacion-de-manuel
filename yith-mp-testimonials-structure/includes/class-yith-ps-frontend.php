<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PS_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PS_Frontend' ) ) {

	class YITH_PS_Frontend {

        /**
		 * Main Instance
		 *
		 * @var YITH_PS_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_PS_Frontend Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PS_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'add_frontend_styles' ));
			add_action( 'wp_enqueue_scripts', array( $this, 'add_frontend_scripts' ));
		}

		public function add_frontend_scripts() {
			wp_register_script('script', YITH_PS_DIR_ASSETS_JS_URL . "/admin.js");
			wp_enqueue_script( 'script');
		}
		public function add_frontend_styles() {
			$css = '
			.format_columns_testimonials .yith_testimonial_mjpa_container{
				border-radius:' . get_option( 'yith_mp_shortcode_border_radius', '7' ) . 'px;
			}
			.url_company_color{
				color:' . get_option( 'yith_mp_shortcode_color', '#effeff' ) . ';
			}';
		
			wp_register_style('style', YITH_PS_DIR_ASSETS_CSS_URL . "/index.css" );
			wp_enqueue_style( 'style' );
			wp_add_inline_style( 'style', $css);
	   }

	}	
}