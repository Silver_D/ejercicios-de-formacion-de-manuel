<?php
/*
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

 // HERE SET FUNCTIONS TO USE ON YOUR PROJECT LIKE Template functions

 /**
  * Include templates
  *
  * @param $file_name name of the file you want to include.
  * @param array $args (array) (optional) Arguments to retrieve.
  */
if ( ! function_exists( 'yith_mjpa_get_template' ) ) {
	function yith_mjpa_get_template( $file_name, $args = array() ) {
		$full_path = YITH_MJPA_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
			/**
			 * print a form field for product metabox
			 *
			 * @since 2.0.0
			 */
if ( ! function_exists( 'yith_mjpa_metabox_form_field' ) ) {
	function yith_mjpa_metabox_form_field( $field ) {

		$defaults = array(
			'class'     => '',
			'title'     => '',
			'label_for' => '',
			'desc'      => '',
			'data'      => array(),
			'fields'    => array(),
			'deps'      => array(),
		);
		$field    = apply_filters( 'yith_wcact_product_metabox_form_field_args', wp_parse_args( $field, $defaults ), $field );
		/**
		 * @var string $class
		 * @var string $title
		 * @var string $label_for
		 * @var string $desc
		 * @var array  $data
		 * @var array  $fields
		 */
		extract( $field );

		if ( ! $label_for && $fields ) {
			$first_field = current( $fields );
			if ( isset( $first_field['id'] ) ) {
				$label_for = $first_field['id'];
			}
		}

		$data_html = '';
		foreach ( $data as $key => $value ) {
			$data_html .= "data-{$key}='{$value}' ";
		}

		$label_for = $field['fields']['id'];
		// llamada para el tratamiento de deps.
		if ( ! empty( $deps ) ) {
			$data_html .= yith_mjpa_fw_dep( $deps, $fields['id'] );
		}

		$html  = '';
		$html .= "<div class='yith-wcact-form-field {$class}' {$data_html}>";
		$html .= "<label class='yith-wcact-form-field__label' for='{$label_for}'>{$title}</label>";

		$html .= "<div class='yith-wcact-form-field__container'>";
		ob_start();
		yith_plugin_fw_get_field( $fields, true ); // Print field using plugin-fw
		$html .= ob_get_clean();
		$html .= '</div><!-- yith-wcact-form-field__container -->';

		if ( $desc ) {
			$html .= "<div class='yith-wcact-form-field__description'>{$desc}</div>";
		}

		$html .= '</div><!-- yith-wcact-form-field -->';

		echo apply_filters( 'yith_wcact_product_metabox_form_field_html', $html, $field );
	}
}
if ( ! function_exists( 'yith_mjpa_fw_dep' ) ) {
	/**
	 * Yith_mjpa_fw_dep
	 *
	 * @param deps mixed   $deps deps.
	 * @param fields mixed $fields fields.
	 * @return void
	 */
	function yith_mjpa_fw_dep( $deps, $id_me ) {
		$deps_data = '';
		if ( isset( $deps ) && ( isset( $deps['target_id'] ) ) && isset( $deps['value'] ) ) {
			$deps       = $deps;
			$id         = isset( $deps['target_id'] ) ? $deps['target_id'] : 'none';
			$dep_id     = isset( $id_me ) ? $id_me : 'none';
			$dep_values = isset( $deps['value'] ) ? $deps['value'] : 'none';
			$dep_type   = isset( $deps['type'] ) ? $deps['type'] : 'default';

			$deps_data = 'data-dep-target="' . esc_attr( $id ) . '" data-dep-id="' . esc_attr( $dep_id ) . '" data-dep-value="' . esc_attr( $dep_values ) . '" data-dep-type="' . esc_attr( $dep_type ) . '"';
		}

		return $deps_data;
	}
}

// get_option( 'yith_mjpa_id_custom_field', 'yes' )['yith-mjpa-checkbox']
// error_log( print_r( get_option( 'yith_mjpa_id_custom_field', 'yes' ), true ) );
// error_log( print_r( get_option( 'yith_mjpa_id_select_field', 'yes' ), true ) );

// Example of use
/*
yith_mjpa_get_template( '/frontend/testimonials.php', array(
	'testimonial_ids' => $testimonial_ids,
	'show_image'      => $args['show_image'],
	'hover_effect'    => $args['hover_effect'],
	'posts_number'    => $args['number']
) );
*/


/**
 * Include views
 *
 * @param $file_name name of the file you want to include.
 * @param array $args (array) (optional) Arguments to retrieve.
 */
if ( ! function_exists( 'yith_mjpa_get_view' ) ) {
	function yith_mjpa_get_view( $file_name, $args = array() ) {
		$full_path = YITH_MJPA_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
if ( ! function_exists( 'yith_mjpa_testing_code' ) ) {
	/**
	 * Yith print custom field.
	 *
	 * @param mixed $field The array that contains the field for WooCommerce custom tab.
	 * @return void
	 */
	function yith_mjpa_testing_code( $field ) {
		$class  = isset( $field['class'] ) ? $field['class'] : '';
		$title  = isset( $field['title'] ) ? $field['title'] : '';
		$desc   = isset( $field['desc'] ) ? $field['desc'] : '';
		$fields = isset( $field['fields'] ) ? $field['fields'] : '';

		?>
		<div class="<?php echo esc_attr( $class ); ?>">
		<?php
		if ( isset( $title ) ) {
			?>
			<label class="yith-mjpa-form-field__label" for="<?php echo esc_attr( $fields['id'] ); ?>"><?php echo esc_html( $title ); ?></label>
			<?php
		}
		?>
			<div class="yith-mjpa-form-field__container">
			<?php
				yith_plugin_fw_get_field( $fields, true );
			?>
			</div>
			<?php
			if ( $desc ) {
				?>
				<div class='yith-mjpa-form-field__description'><?php echo esc_html( $desc ); ?></div>
				<?php
			}
			?>
		</div>
		<?php
	}
}
