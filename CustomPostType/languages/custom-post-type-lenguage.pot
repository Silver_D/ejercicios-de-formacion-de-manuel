# Copyright (C) 2020 Manuel
# This file is distributed under the same license as the Ejercicios-CustomPostType plugin.
msgid ""
msgstr ""
"Project-Id-Version: Ejercicios-CustomPostType 1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/CustomPostType\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2020-10-16T08:55:47+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.5.0-alpha-9061b3e\n"
"X-Domain: custom-post-type-lenguage\n"

#. Plugin Name of the plugin
#. Description of the plugin
msgid "Ejercicios-CustomPostType"
msgstr ""

#. Author of the plugin
msgid "Manuel"
msgstr ""

#: EjercicoPostType.php:35
msgid "Books"
msgstr ""

#: EjercicoPostType.php:36
msgid "Book"
msgstr ""

#: EjercicoPostType.php:37
#: EjercicoPostType.php:38
#: EjercicoPostType.php:40
msgid "Add New Book"
msgstr ""

#: EjercicoPostType.php:39
msgid "Edit Book"
msgstr ""

#: EjercicoPostType.php:41
msgid "View Book"
msgstr ""

#: EjercicoPostType.php:42
msgid "Search Book"
msgstr ""

#: EjercicoPostType.php:43
msgid "No Book found"
msgstr ""

#: EjercicoPostType.php:44
msgid "No Books found in trash"
msgstr ""

#: EjercicoPostType.php:181
msgid "Post Costum Post Title"
msgstr ""

#: EjercicoPostType.php:183
msgid "Post Costum Content"
msgstr ""

#: EjercicoPostType.php:185
msgid "ISBN"
msgstr ""

#: EjercicoPostType.php:187
msgid "Price"
msgstr ""
