<?php
/*
 * This file belongs to the YITH MJPA Plugin Addons.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Plugin_Products_Addons_Frontend' ) ) {

	class YITH_MJPA_Plugin_Products_Addons_Frontend {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Plugin_Products_Addons_Frontend
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Plugin_Products_Addons_Frontend Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Frotend constructor.
		 */
		private function __construct() {
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_mjpa_print_addons' ) );
			add_filter( 'woocommerce_add_to_cart_validation', array( $this, 'yith_mjpa_add_to_cart_validation' ), 10, 3 );
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'yith_mjpa_add_to_cart_new_item' ), 10, 3 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_mjpa_get_new_item_data' ), 10, 2 );
			add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'yith_mjpa_create_new_order_line' ), 10, 4 );

			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_mjpa_custom_price' ), 99 );

			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_scripts' ) );

		}
		/**
		 * Add_input_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {
			wp_register_style( 'yith-mjpa-style', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style-frontend.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style' );
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {
			global $post;
			$get_addons = get_post_meta( $post->ID, 'addons', false );
			wp_register_script( 'yith-mjpa-product', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-front-addons.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-mjpa-product' );
			$attr_addons = array();
			if ( ! empty( $get_addons ) ) {
				foreach ( $get_addons[0] as $key => $value ) {
					if ( 'on' === $value['_checkbox_enable'] ) {
						array_push( $attr_addons, $value );
					}
					wp_localize_script(
						'yith-mjpa-product',
						'yith_mjpa_vars',
						array(
							'addons' => $attr_addons,
						)
					);
				}
			}
		}

		/**
		 * Yith_mjpa_print_addons
		 *
		 * @return void
		 */
		public function yith_mjpa_print_addons() {
			global $post;
			$get_addons    = get_post_meta( $post->ID, 'addons', false );
			$addons_number = 0;
			foreach ( $get_addons[0] as $key => $value ) {
				if ( 'on' === $value['_checkbox_enable'] ) {
					$attr = array( $value, $addons_number );
					yith_mjpa_get_template( '/frontend/addons.php', $attr );
					$addons_number = ++$addons_number;
				}
			}
			$this->yith_mjpa_print_prices();
		}
		/**
		 * Yith_mjpa_print_prices
		 *
		 * @return void
		 */
		public function yith_mjpa_print_prices() {
			global $post;
			$product = wc_get_product( $post->ID );
			$attr    = array( $product->get_price(), $product );
			yith_mjpa_get_template( '/frontend/summary.php', $attr );
		}
		/**
		 * Yith_mjpa_add_to_cart_validation
		 *
		 * @param true mixed       $true validation passed.
		 * @param product_id mixed $product_id product_id.
		 * @param quantity mixed   $quantity quantity.
		 * @return true void
		 */
		public function yith_mjpa_add_to_cart_validation( $true, $product_id, $quantity ) {
			// no hace falta porque no estoy validando nada
			return $true;
		}
		/**
		 * Yith_mjpa_add_to_cart_new_item
		 *
		 * @param cart_item_data mixed $cart_item_data cart_item_data.
		 * @param product_id mixed     $product_id product_id.
		 * @param variation_id mixed   $variation_id variation_id.
		 * @return cart_item_data void
		 */
		public function yith_mjpa_add_to_cart_new_item( $cart_item_data, $product_id, $variation_id ) {
			$addons_value = array();
			$index        = 0;
			foreach ( $_POST as $key => $value ) {
				if ( 'hidden_input_price' !== $key && 'hidden_input_names' !== $key && 'quantity' !== $key && 'add-to-cart' !== $key ) {
					$split_string                  = explode( '_', $key );
					$split_string                  = $split_string[ count( $split_string ) - 1 ];
					$addons_value[ $split_string ] = $value;
				}
			}
			$addons_prices                   = explode( ',', substr( $_POST['hidden_input_price'], 1, strlen( $_POST['hidden_input_price'] ) - 1 ) );
			$addons_names                    = explode( ',', substr( $_POST['hidden_input_names'], 0, strlen( $_POST['hidden_input_names'] ) - 1 ) );
			$product                         = wc_get_product( $product_id );
			$attr                            = array( $product->get_price(), $product );
			$cart_item_data['addons_prices'] = $addons_prices;
			$cart_item_data['addons_names']  = $addons_names;
			$cart_item_data['product_price'] = $attr;
			$cart_item_data['addons_value']  = $addons_value;
			return $cart_item_data;
		}
		/**
		 * Yith_mjpa_get_new_item_data
		 *
		 * @param item_data mixed      $item_data item_data.
		 * @param cart_item_data mixed $cart_item_data cart_item_data.
		 * @return item_data void
		 */
		public function yith_mjpa_get_new_item_data( $item_data, $cart_item_data ) {
			$addons_prices = $cart_item_data['addons_prices'];
			$addons_names  = $cart_item_data['addons_names'];
			$product_price = $cart_item_data['product_price'];
			$addons_value  = $cart_item_data['addons_value'];
			$for_delimiter = count( $addons_names );
			$item_data[]   = array(
				'key'   => 'Base Price ',
				'value' => '$' . $product_price[0] . '.00',
			);
			for ( $i = 0; $i < $for_delimiter; $i++ ) {
				$item_data[] = array(
					'key'   => $addons_names[ $i ] . '(+$' . $addons_prices[ $i ] . ')',
					'value' => $addons_value[ $i ],
				);
			}
			return $item_data;
		}
		/**
		 * Yith_mjpa_custom_price
		 *
		 * @param cart_object mixed $cart_object cart_object.
		 * @return void
		 */
		public function yith_mjpa_custom_price( $cart_object ) {
			foreach ( WC()->cart->get_cart() as $key => $value ) {
				$added_price_total_addons = 0;
				if ( isset( $value['addons_prices'] ) ) {
					foreach ( $value['addons_prices'] as $llave => $valor ) {
						$added_price_total_addons = $added_price_total_addons + $valor;
					}
				}
				$original_price = floatval( $value['data']->get_price() );
				$value['data']->set_price( $original_price + $added_price_total_addons );
			}
		}
		/**
		 * Yith_mjpa_create_new_order_line
		 *
		 * @param item mixed          $item item.
		 * @param cart_item_key mixed $cart_item_key cart_item_key.
		 * @param values mixed        $values values.
		 * @param order mixed         $order order.
		 * @return void
		 */
		public function yith_mjpa_create_new_order_line( $item, $cart_item_key, $values, $order ) {
			if ( isset( $values['product_price'] ) ) {
				$item->add_meta_data(
					'Product Price: ',
					$values['product_price'][0],
					true
				);
			}
			if ( isset( $values['addons_prices'] ) && isset( $values['addons_names'] ) ) {
				$for_delimiter = count( $values['addons_names'] );
				for ( $i = 0; $i < $for_delimiter; $i++ ) {
					$item->add_meta_data(
						$values['addons_names'][ $i ],
						$values['addons_prices'][ $i ],
						true
					);
				}
			}
		}
	}
}
