<?php
/**
 * Plugin Name: YITH Database Plugin Skeleton
 * Description: Skeleton for YITH Plugins
 * PHP version 7.4
 * Author: Manuel Jesús Peraza Alonso
 * Author URI: https://yithemes.com/
 * Text Domain: yith-mjpa-database-plugin
 *
 * @category Template_Class
 * @package  WordPress
 * @author   Author <author@domain.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://localhost/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
};   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_MJPA_VERSION_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_VERSION_DATABASE_PLUGIN', '1.0.0' );
}

if ( ! defined( 'YITH_MJPA_DIR_URL_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_URL_DATABASE_PLUGIN', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_URL_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_URL_DATABASE_PLUGIN', YITH_MJPA_DIR_URL_DATABASE_PLUGIN . 'assets' );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_CSS_URL_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_CSS_URL_DATABASE_PLUGIN', YITH_MJPA_DIR_ASSETS_URL_DATABASE_PLUGIN . '/css' );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_JS_URL_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_JS_URL_DATABASE_PLUGIN', YITH_MJPA_DIR_ASSETS_URL_DATABASE_PLUGIN . '/js' );
}

if ( ! defined( 'YITH_MJPA_DIR_PATH_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_PATH_DATABASE_PLUGIN', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_MJPA_DIR_INCLUDES_PATH_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_INCLUDES_PATH_DATABASE_PLUGIN', YITH_MJPA_DIR_PATH_DATABASE_PLUGIN . '/includes' );
}

if ( ! defined( 'YITH_MJPA_DIR_TEMPLATES_PATH_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_TEMPLATES_PATH_DATABASE_PLUGIN', YITH_MJPA_DIR_PATH_DATABASE_PLUGIN . '/templates' );
}

if ( ! defined( 'YITH_MJPA_DIR_VIEWS_PATH_DATABASE_PLUGIN' ) ) {
	define( 'YITH_MJPA_DIR_VIEWS_PATH_DATABASE_PLUGIN', YITH_MJPA_DIR_PATH_DATABASE_PLUGIN . '/views' );
}

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_mjpa_init_classes_database_plugin' ) ) {


	/**
	 * Yith_mjpa_init_classes_database_plugin
	 *
	 * @return void
	 */
	function yith_mjpa_init_classes_database_plugin() {

		load_plugin_textdomain( 'yith-mjpa-database-plugin', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once YITH_MJPA_DIR_INCLUDES_PATH_DATABASE_PLUGIN . '/class-yith-mjpa-database-plugin-structure.php';

		if ( class_exists( 'YITH_MJPA_Database_Plugin_Structure' ) ) {

			if ( ! function_exists( 'yith_wcact_install' ) ) {

				/**
				 * Yith_wcact_install
				 *
				 * @return void
				 */
				function yith_wcact_install() {
					YITH_MP_Database::install();
				}
			}

			yith_mjpa_database_plugin_structure();
		}
	}
}


add_action( 'plugins_loaded', 'yith_mjpa_init_classes_database_plugin', 11 );
add_action( 'plugins_loaded', 'yith_wcact_install', 12 );
