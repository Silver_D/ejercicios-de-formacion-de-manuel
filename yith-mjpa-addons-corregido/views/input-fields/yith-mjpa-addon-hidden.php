<?php
	$post_index = array_pop( $args );

if ( isset( get_post_meta( $post_index, 'addons', false )[0] ) ) {
	$addons_value  = get_post_meta( $post_index, 'addons', false )[0];
	$addons_length = count( $addons_value );
	?>
		<div id="yith_mjpa_addons_INDEX" class="wrapp-accordion mjpa_hidden">
				<div class="accordion-title js-accordion-title">
					<div class="wrap-title-checkbox">
						<h4 class="yith_title_addon">Accordion Title</h4>
						<div class="class_checkbox_wrap">
							<span class="class_checkbox_enable"><input type="checkbox" id="yith-mjpa-addons-checkbox-enable-{{INDEX}}" name="yith-mjpa-addons[{{INDEX}}][checkbox_enable]" class="_checkbox_enable"> Enable/Disable</span>
						</div>
					</div>
				</div>
				<div class="accordion-content options-group">
					<?php YITH_MJPA_Admin_Addons_Fixed::yith_mjpa_print_input( $args ); ?>
					<div class="delete_addon_button_wrap">
						<button class="delete_addon_button button button-primary button-large" onclick="delete_addon(event)">Eliminar Addon</button>
					</div>
				</div>
		</div> 
		<?php
		for ( $i = 0; $i < $addons_length; $i++ ) {
			$arr_saved_addons = array(
				'name_field'             => array(
					'type'                 => 'text',
					'id_field'             => 'yith-mjpa-addons-name-' . $i,
					'class_field'          => 'class_name_field',
					'label_field'          => 'Name Field',
					'name_field'           => 'yith-mjpa-addons[' . $i . '][name_field]',
					'class_field_show'     => '',
					'default_text_value'   => $addons_value[ $i ]['name_field'],
					'default_color_picker' => '',
				),
				'textarea_input_type'    => array(
					'type'             => 'textarea',
					'id_field'         => 'yith-mjpa-addons-textarea-' . $i,
					'default_input'    => $addons_value[ $i ]['textarea_field'],
					'class_field'      => 'short class_textarea',
					'label_field'      => 'Text area',
					'name_field'       => 'yith-mjpa-addons[' . $i . '][textarea_field]',
					'class_field_show' => '',
				),
				'select_input_type'      => array(
					'type'             => 'select',
					'id_field'         => 'yith-mjpa-addons-select-' . $i,
					'class_field'      => 'short class_select radio-group-[' . $i . ']',
					'label_field'      => 'Select Label',
					'default_input'    => $addons_value[ $i ]['select_field'],
					'name_field'       => 'yith-mjpa-addons[' . $i . '][select_field]',
					'class_field_show' => '',
					'options_value'    => array( 'text', 'textarea', 'select', 'radio', 'checkbox', 'onoff' ),
				),
				'option_select'          => array(
					'type'                    => 'option',
					'id_field'                => 'id_field',
					'default_text_value'      => '',
					'id_field_option_name'    => 'id_field_option_GHOST',
					'type_option'             => 'text',
					'class_field'             => 'class_option_select block',
					'class_field_price'       => 'class_option_price class_option_select',
					'class_field_option'      => 'class_option_option class_option_select',
					'class_field_wrap_option' => 'class_flex class_option_price_show class_option_price_show_' . $i,
					'name_field_option'       => 'yith-mjpa-addons[' . $i . '][name_field_option][]',
					'id_field_price'          => 'id_field_price_GHOST',
					'name_field_price'        => 'yith-mjpa-addons[' . $i . '][price_field_option][]',
					'label_field_option'      => 'Name',
					'label_field_price'       => 'Price',
					'class_field_show'        => 'wrap_options_select mjpa_hidden',
					'class_group_show'        => 'wrap-options mjpa_hidden GHOST_' . $i,
					'id_add_option'           => 'id_add_option_' . $i,
					'class_class_add_option'  => 'add_new_option taxonomy-add-new',
					'options_stored'          => $addons_value[ $i ]['name_field_option'],
					'price_stored'            => $addons_value[ $i ]['price_field_option'],
					'group_option_class'      => 'group_' . $i,
					'index_addon'             => $i,
				),
				'radio_input_type_price' => array(
					'type'             => 'radio',
					'class_field'      => '',
					'class_field_show' => 'class_show radio-group-[' . $i . ']',
					'options'          => array(
						'first'  => array(
							'type'             => 'radio',
							'id_field'         => 'yith-mjpa-addons-radio-free-' . $i,
							'name_field'       => 'yith-mjpa-addons[' . $i . '][radio_field]',
							'value_field'      => 'free',
							'class_field'      => 'class_radio_price_type_free',
							'label_field'      => 'Free',
							'default'          => ( 'free' === $addons_value[ $i ]['radio_field'] ) ? 'true' : 'false',
							'class_field_show' => 'class_show_free',
						),
						'second' => array(
							'type'             => 'radio',
							'id_field'         => 'yith-mjpa-addons-radio-fixed-' . $i,
							'name_field'       => 'yith-mjpa-addons[' . $i . '][radio_field]',
							'value_field'      => 'fixed_price',
							'label_field'      => 'Fixed Price',
							'class_field'      => 'class_radio_price_type_fixed',
							'default'          => ( 'fixed_price' === $addons_value[ $i ]['radio_field'] ) ? 'true' : 'false',
							'class_field_show' => '',
						),
						'third'  => array(
							'type'             => 'radio',
							'id_field'         => 'yith-mjpa-addons-radio-price-per-character-' . $i,
							'name_field'       => 'yith-mjpa-addons[' . $i . '][radio_field]',
							'value_field'      => 'price_per_character',
							'label_field'      => 'Price Per Character',
							'default'          => ( 'price_per_character' === $addons_value[ $i ]['radio_field'] ) ? 'true' : 'false',
							'class_field'      => 'class_radio_price_type_character',
							'class_field_show' => 'class_price_per_character',
						),
					),
				),
				'price_field'            => array(
					'type'                 => 'text',
					'id_field'             => 'yith-mjpa-addons-price-field-' . $i,
					'class_field'          => 'class_price_field',
					'label_field'          => 'Price Field',
					'name_field'           => 'yith-mjpa-addons[' . $i . '][price_field]',
					'class_field_show'     => 'price_show_class mjpa_hidden',
					'default_text_value'   => $addons_value[ $i ]['price_field'],
					'default_color_picker' => '',
				),
				'onoff'                  => array(
					'type'             => 'onoff',
					'id_field_wrap'    => 'yith-mjpa-addons-onoff-wrapp-' . $i,
					'id_field'         => 'yith-mjpa-addons-onoff-' . $i,
					'class_field'      => 'class_onoff',
					'label_field'      => 'Default Enabled:',
					'name_field'       => 'yith-mjpa-addons[' . $i . '][onoff_field]',
					'default'          => ( isset( $addons_value[ $i ]['onoff_field'] ) ) ? $addons_value[ $i ]['onoff_field'] : 'off',
					'class_field_show' => 'onnoff_show_class mjpa_hidden',
				),
				'free_characters'        => array(
					'type'             => 'number',
					'id_field'         => 'yith-mjpa-addons-free-characters-input-' . $i,
					'class_field'      => 'class_free_characters_input short',
					'label_field'      => 'Free Characters:',
					'name_field'       => 'yith-mjpa-addons[' . $i . '][free-characters-field]',
					'default'          => $addons_value[ $i ]['free-characters-field'],
					'class_field_show' => 'free_characters_show_class mjpa_hidden',
				),
			);
			?>
			<div id="<?php echo( esc_html( 'yith_mjpa_addons_' . $i ) ); ?>" class="wrapp-accordion">
				<div class="accordion-title js-accordion-title">
					<div class="wrap-title-checkbox">
						<h4 class="yith_title_addon"><?php echo( esc_html( $addons_value[ $i ]['name_field'] ) ); ?></h4>
						<div class="class_checkbox_wrap">
							<span class="class_checkbox_enable"><input 
							<?php
							if ( isset( $addons_value[ $i ]['checkbox_enable'] ) ) {
								if ( 'on' === $addons_value[ $i ]['checkbox_enable'] ) {
									echo( 'checked' );
								}
							}
							?>
							 type="checkbox" id="<?php echo( esc_html( 'yith-mjpa-addons-checkbox-enable-' . $i ) ); ?>" name="<?php echo( esc_html( 'yith-mjpa-addons[' . $i . '][checkbox_enable]' ) ); ?>" class="_checkbox_enable"> Enable/Disable</span>
						</div>
					</div>
				</div>
				<div class="accordion-content options-group">
					<?php YITH_MJPA_Admin_Addons_Fixed::yith_mjpa_print_input( $arr_saved_addons ); ?>
					<div class="delete_addon_button_wrap">
						<button class="delete_addon_button button button-primary button-large" onclick="delete_addon(event)">Eliminar Addon</button>
					</div>
				</div>
		</div> 
			<?php
		}
} else {
	?>
	 

<div id="yith_mjpa_addons_INDEX" class="wrapp-accordion mjpa_hidden">
		<div class="accordion-title js-accordion-title">
			<div class="wrap-title-checkbox">
				<h4 class="yith_title_addon">Accordion Title 1</h4>
				<div class="class_checkbox_wrap">
					<span class="class_checkbox_enable"><input type="checkbox" id="yith-mjpa-addons-checkbox-enable-{{INDEX}}" name="yith-mjpa-addons[{{INDEX}}][checkbox_enable]" class="_checkbox_enable"> Enable/Disable</span>
				</div>
			</div>
		</div>
		<div class="accordion-content options-group">
			<?php YITH_MJPA_Admin_Addons_Fixed::yith_mjpa_print_input( $args ); ?>
			<div class="delete_addon_button_wrap">
				<button class="delete_addon_button button button-primary button-large" onclick="delete_addon(event)">Eliminar Addon</button>
			</div>
		</div>
</div>
	<?php } ?>
