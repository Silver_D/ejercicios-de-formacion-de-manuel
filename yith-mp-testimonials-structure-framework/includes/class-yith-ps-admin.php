<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_PS_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}


if ( ! class_exists( 'YITH_PS_Admin' ) ) {

	class YITH_PS_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_PS_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_PS_Admin Main instance
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}


		public function add_admin_scripts() {
			  wp_register_script( 'script', YITH_PS_DIR_ASSETS_JS_URL . '/admin.js' );
			  wp_enqueue_script( 'script' );
			  wp_enqueue_script( 'color_picker_script', YITH_PS_DIR_ASSETS_JS_URL . '/color_picker.js', array( 'wp-color-picker' ), false, true );
			  wp_enqueue_script( 'color_picker' );
		}
		public function add_admin_styles() {
			  wp_register_style( 'style', YITH_PS_DIR_ASSETS_CSS_URL . '/admin.css' );
			  wp_enqueue_style( 'style' );
			  wp_enqueue_style( 'wp-color-picker' );
		}
		/**
		 * YITH_PS_Admin constructor.
		 */
		private function __construct() {

			add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'save_meta_box' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_scripts' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_styles' ) );

			// See definition on class-wp-post-list-table.php  manage_{$post_type}_posts_columns;
			add_filter( 'manage_ps-skeleton_posts_columns', array( $this, 'add_skeleton_post_type_columns' ) );

			// See definition on class-wp-post-list-table.php manage_{$post->post_type}_posts_custom_column
			add_action( 'manage_ps-skeleton_posts_custom_column', array( $this, 'display_skeleton_post_type_custom_column' ), 10, 2 );

			// Admin menu
			add_action( 'admin_menu', array( $this, 'create_menu_for_general_options' ) );
			add_action( 'admin_init', array( $this, 'register_settings' ) );
			// add Roles
			add_action( 'init', array( $this, 'register_role' ) );
		}

		/**
		 * Setup the role
		 */
		public function register_role() {
			if ( ! get_option( 'yith_add_role_testimonial', false ) ) {
				// añadimos rol
				$capabilities_testimonial_manager = array(
					'read'                      => true,
					'edit_testimonial'          => true,
					'edit_others_testimonials'  => true,
					'read_testimonial'          => true,
					'read_private_testimonials' => true,
					'publish_testimonials'      => true,
					'edit_testimonials'         => true,
					'delete_testimonial'        => true,
					'delete_testimonials'       => true,
					'upload_files'              => true,
					'manage_testimonials'       => true,

				);
				add_role( 'testimonial_manager', 'Testimonial Manager', $capabilities_testimonial_manager );

				$administrator_capabilities = array(
					'edit_testimonial',
					'edit_others_testimonials',
					'read_testimonial',
					'publish_testimonials',
					'edit_testimonials',
					'delete_testimonial',
					'delete_testimonials',
					'manage_testimonials',
				);
				$role                       = get_role( 'administrator' );
				foreach ( $administrator_capabilities as $key => $value ) {
					$role->add_cap( $value );
				}

				update_option( 'yith_add_role_testimonial', true );
			} else {
				return;
			}

		}

		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box(
				'yith-ps-additional-information',
				__( 'Additional information', 'yith-plugin-skeleton' ),
				array(
					$this,
					'view_meta_boxes',
				),
				YITH_PS_Post_Types::$post_type
			);
		}

		/**
		 * Wiev meta boxes
		 *
		 * @param $post
		 */
		public function view_meta_boxes( $post ) {
			yith_ps_get_view( '/metaboxes/plugin-testimonial-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save meta box values
		 *
		 * @param $post_id
		 */
		public function save_meta_box( $post_id ) {

			if ( YITH_PS_Post_Types::$post_type !== get_post_type( $post_id ) ) {
				return;
			}

			if ( isset( $_POST['info'] ) ) {
				update_post_meta( $post_id, 'info', $_POST['info'] );
			}
			if ( isset( $_POST['company'] ) ) {
				update_post_meta( $post_id, 'company', $_POST['company'] );
			}
			if ( isset( $_POST['url_company'] ) ) {
				update_post_meta( $post_id, 'url_company', $_POST['url_company'] );
			}
			if ( isset( $_POST['email'] ) ) {
				update_post_meta( $post_id, 'email', $_POST['email'] );
			}
			if ( isset( $_POST['rating'] ) ) {
				update_post_meta( $post_id, 'rating', $_POST['rating'] );
			}
			if ( isset( $_POST['vip'] ) ) {
				update_post_meta( $post_id, 'vip', $_POST['vip'] );
			}
			if ( isset( $_POST['badge'] ) ) {
				update_post_meta( $post_id, 'badge', $_POST['badge'] );
				if ( $_POST['badge'] == 'affirmative_badge' ) {
					if ( ! isset( $_POST['text_badge'] ) ) {
						$_POST['text_badge'] = '';
					} else {
						update_post_meta( $post_id, 'text_badge', $_POST['text_badge'] );
					}
					if ( ! isset( $_POST['color'] ) ) {
						$_POST['color'] = '#effeff';
					} else {
						update_post_meta( $post_id, 'color', $_POST['color'] );
					}
				}
			}

		}

		public function add_skeleton_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_ps_skeleton_custom_columns ',
				array(
					'column1' => esc_html__( 'Role', 'yith-plugin-skeleton' ),
					'column2' => esc_html__( 'Company', 'yith-plugin-skeleton' ),
					'column3' => esc_html__( 'Email', 'yith-plugin-skeleton' ),
					'column4' => esc_html__( 'Stars', 'yith-plugin-skeleton' ),
					'column5' => esc_html__( 'VIP', 'yith-plugin-skeleton' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}
		/**
		 * Fires for each custom column of a specific post type in the Posts list table.
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 * */
		public function display_skeleton_post_type_custom_column( $column_name, $post_id ) {

			switch ( $column_name ) {

				case 'column1':
						// Operations for get information for column1 example
						$value = get_post_meta( $post_id, 'info', 'yes' );
						// $value_custom_2 = get_post_meta($post_id,'_meta_column1',true);
						// echo $value_custom_1;

						echo $value;

					break;
				case 'column2':
						// Operations for get information for column2 example
						echo ( "<a href='" . get_post_meta( $post_id, 'url_company', 'yes' ) . "'>" . get_post_meta( $post_id, 'company', 'yes' ) . '</a> ' );
						// $value_custom_2 = get_post_meta($post_id,'_meta_column2',true);
						// echo $value_custom_2;

					break;
				case 'column3':
					// Operations for get information for column2 example
					$value = get_post_meta( $post_id, 'email', 'yes' );
					// $value_custom_2 = get_post_meta($post_id,'_meta_column2',true);
					// echo $value_custom_2;

					echo $value;
					break;
				case 'column4':
					$rating_number = get_post_meta( $post_id, 'rating', 'yes' );
					echo ( "
					<div class='stars__wrapper__admin__column'>
                        <a href='#' class='starcol star" . $post_id . "' disabled>★</a>
                        <a href='#' class='starcol star" . $post_id . "' disabled>★</a>
                        <a href='#' class='starcol star" . $post_id . "' disabled>★</a>
                        <a href='#' class='starcol star" . $post_id . "' disabled>★</a>
                        <a href='#' class='starcol star" . $post_id . "' disabled>★</a>
                    </div>
                    <script>
                        paint_stars($rating_number$post_id );
					</script>
					" );

					break;
				case 'column5':
					$value = get_post_meta( $post_id, 'vip', 'yes' );
					// $value_custom_2 = get_post_meta($post_id,'_meta_column2',true);
					// echo $value_custom_2;

					echo $value;
					break;
				default:
					do_action( 'yith_ps_skeleton_display_custom_column', $column_name, $post_id );
					break;
			}

		}

			/**
			 *  Create menu for general options
			 */
		public function create_menu_for_general_options() {
			// See the following option https://developer.wordpress.org/reference/functions/add_menu_page/
				add_menu_page(
					esc_html__( 'Plugin Skeleton Options', 'yith-plugin-skeleton' ),
					esc_html__( 'Plugin Skeleton Options', 'yith-plugin-skeleton' ),
					'manage_testimonials',
					'plugin_skeleton_options',
					array( $this, 'skeleton_custom_menu_page' ),
					'',
					40
				);
		}
		/**
		 *  Callback custom menu page
		 */
		function skeleton_custom_menu_page() {
			yith_ps_get_view( '/admin/plugin-options-panel.php', array() );
		}
		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function register_settings() {

			$page_name      = 'ps-options-page';
			$section_name   = 'options_section';
			$setting_fields = array(
				array(
					'id'       => 'yith_ps_shortcode_number',
					'title'    => esc_html__( 'Number', 'yith-plugin-skeleton' ),
					'callback' => 'print_number_input',
				),
				array(
					'id'       => 'yith_ps_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-plugin-skeleton' ),
					'callback' => 'print_show_image',
				),
				array(
					'id'       => 'yith_ps_shortcode_hover_effect',
					'title'    => esc_html__( 'Hover effect', 'yith-plugin-skeleton' ),
					'callback' => 'print_hover_effect',
				),
				array(
					'id'       => 'yith_mp_shortcode_border_radius',
					'title'    => esc_html__( 'Border radius', 'yith-plugin-skeleton' ),
					'callback' => 'print_border_radius',
				),
				array(
					'id'       => 'yith_mp_shortcode_color',
					'title'    => esc_html__( 'Color', 'yith-plugin-skeleton' ),
					'callback' => 'print_color',
				),
			);
			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-skeleton' ),
				'',
				$page_name
			);
			foreach ( $setting_fields as $field ) {
				extract( $field );
				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);
				register_setting( $page_name, $id );
			}
		}

		/**
		 * Print the number input field
		 */
		public function print_number_input() {
			$tst_number = intval( get_option( 'yit_yith-mjpa-plugin-framework_options', 6 )['yith-mjpa-number-testimonials'] );
			?>
			<input type="number" id="yith_ps_shortcode_number" name="yith_ps_shortcode_number"
				   value="<?php echo '' !== $tst_number ? $tst_number : 6; ?>">
			<?php
		}
		/**
		 * Print the show image toggle field
		 */
		public function print_show_image() {
			?>
			<input type="checkbox" class="ps-tst-option-panel__onoff__input" name="yith_ps_shortcode_show_image"
				   value='yes'
				   id="yith_ps_shortcode_show_image"
				<?php checked( get_option( 'yit_yith-mjpa-plugin-framework_options', '' )['yith-mjpa-checkbox'], 'yes' ); ?>
			>
			<label for="shortcode_show_image" class="ps-tst-option-panel__onoff__label ">
				<span class="ps-tst-option-panel__onoff__btn"></span>
			</label>
			<?php
		}
				/**
				 * Print the hover_effects field
				 */
		public function print_hover_effect() {

			?>
			<input type="radio" name="yith_ps_shortcode_hover_effect" value="zoom" <?php checked( get_option( 'yit_yith-mjpa-plugin-framework_options', '' )['yith-mjpa-select'], 'zoom' ); ?>>
			<label for="hover_zoom">Zoom</label><br>
			<input type="radio"  name="yith_ps_shortcode_hover_effect" value="highlight" <?php checked( get_option( 'yit_yith-mjpa-plugin-framework_options', '' )['yith-mjpa-select'], 'highlight' ); ?>>
			<label for="hover_highlight">Highlight</label><br>
			<input type="radio" name="yith_ps_shortcode_hover_effect" value="default" <?php checked( get_option( 'yit_yith-mjpa-plugin-framework_options', '' )['yith-mjpa-select'], 'default' ); ?>>
			<label for="hover_default">Default</label>
			<?php
		}
		/**
		 * Print the border radius input field
		 */
		public function print_border_radius() {
			$tst_number = intval( get_option( 'yit_yith-mjpa-plugin-framework_options', 7 )['yith-mjpa-number-radio-testimonials'] );
			?>
			<input type="number" id="yith_mp_shortcode_border_radius" name="yith_mp_shortcode_border_radius"
				   value="<?php echo '' !== $tst_number ? $tst_number : 7; ?>">
			<?php
		}
		/**
		 * Print the color input field
		 */
		public function print_color() {
			$tst_number = get_option( 'yit_yith-mjpa-plugin-framework_options', '' )['yith-mjpa-colorpicker'];
			?>
			<input type='text' name='yith_mp_shortcode_color' value="<?php echo '' !== $tst_number ? $tst_number : '#effeff'; ?>" class='my-color-field' data-default-color='#effeff' />
			<?php
		}

	}
}
