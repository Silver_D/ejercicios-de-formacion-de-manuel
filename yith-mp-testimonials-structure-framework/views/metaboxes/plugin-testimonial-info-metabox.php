<?php
    $args=array("info","company","url_company","email","rating","vip","badge","text_badge","color");
	$info = get_post_meta( $post->ID, $args[0], true );
	$company = get_post_meta( $post->ID, $args[1], true );
    $url_company = get_post_meta( $post->ID, $args[2], true );
    $email = get_post_meta( $post->ID, $args[3], true );
    $rating = get_post_meta( $post->ID, $args[4], true );
    $vip = get_post_meta( $post->ID, $args[5], true );
    $badge = get_post_meta( $post->ID, $args[6], true );
    $text_badge = get_post_meta( $post->ID, $args[7], true );
    $color = get_post_meta( $post->ID, $args[8], true );
	
?>


<h1>Rol</h1>
<input type="text" name="<?php echo $args[0]; ?>" value="<?php echo esc_textarea( $info ); ?>" class="widefat">
<h1>Company</h1>
<input type="text" name="<?php echo $args[1]; ?>" value="<?php echo esc_textarea( $company ); ?>" class="widefat">
<h1>Url Company</h1>
<input type="text" name="<?php echo $args[2]; ?>" value="<?php echo esc_textarea( $url_company ); ?>" class="widefat">
<h1>Email</h1>
<input type="text" name="<?php echo $args[3]; ?>" value="<?php echo esc_textarea( $email ); ?>" class="widefat">
<h1>Rating</h1>
<div class='container'>
    <div class='stars__wrapper'>
    <a href='#' class='star star1'>★</a>
    <a href='#' class='star star2'>★</a>
    <a href='#' class='star star3'>★</a>
    <a href='#' class='star star4'>★</a>
    <a href='#' class='star star5'>★</a>
    </div>
    <input type='text' class='invi' id='rating' value=" <?php echo esc_textarea( $rating ); ?>" name="<?php echo $args[4]; ?>">
    <script>
        window.onload = stars_loaded(<?php echo $rating ?>);
    </script>
</div>
<h1>VIP</h1>
<?php 
    if ($vip=='affirmative') {
?>
        <input type='radio' id='affirmative' name="<?php echo $args[5]; ?>" value='affirmative' checked>
        <label for='affirmative'>Si</label><br>
        <input type='radio' id='negative' name="<?php echo $args[5]; ?>" value='negative' >
        <label for='negative'>No</label><br>
<?php       
    }
    else{
?>
        <input type='radio' id='affirmative' name="<?php echo $args[5]; ?>" value='affirmative' >
        <label for='affirmative'>Si</label><br>
        <input type='radio' id='negative' name="<?php echo $args[5]; ?>" value='negative' checked>
        <label for='negative'>No</label><br>
<?php       
    }
?>
<h1>Badge</h1>

<?php  
    if ($badge=='affirmative_badge') {
?>
    <input type='radio' id='affirmative_badge' name="<?php echo $args[6]; ?>" value='affirmative_badge' checked>
    <label for='affirmative'>Si</label><br>
    <input type='radio' id='negative_badge' name="<?php echo $args[6]; ?>" value='negative_badge' >
    <label for='negative'>No</label><br>
<?php 
    }
    else{
?>
    <input type='radio' id='affirmative_badge' name="<?php echo $args[6]; ?>" value='affirmative_badge' >
    <label for='affirmative'>Si</label><br>
    <input type='radio' id='negative_badge' name="<?php echo $args[6]; ?>" value='negative_badge' checked>
    <label for='negative'>No</label><br>
<?php     
    }
    if ($badge=='affirmative_badge') {
?>
        <h1>Text Badge</h1>
        <input type='text' name="<?php echo $args[7]; ?>" value=" <?php echo esc_textarea( $text_badge ); ?>" class='widefat'>
        <h1>Color</h1>
        <input type='text' name="<?php echo $args[8]; ?>" value=" <?php echo esc_textarea( $color ); ?>" class='my-color-field' data-default-color='#effeff' />
<?php  
    }
?>