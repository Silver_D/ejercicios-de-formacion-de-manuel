<?php

/*
Plugin Name: Ejercicio4
Plugin URI: 
Description: Ejercicio4
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/

/* 
Cuando se guarda un post se ejecuta la función change_fields 
la cual inserta en la base de datos una serie de entradas que 
serán mostradas en el content gracias a la función get_post_meta
*/
function change_fields($post_id) {
    //id de un post real
    $id_array=array(1,2,3);
    $meta_array=array("NombrePadreAutor","NombreMadreAutor","Edad");
    $id_value=array("Juan","Juana",25);
    for ($i = 0; $i < count($id_array); $i++) {
        update_post_meta($id_array[$i],$meta_array[$i],$id_value[$i],false);
    }
}
add_action ('save_post', 'change_fields');


function show_fields_post($content){ //revisar
    if(!is_feed() && !is_home()) {

        $valor=get_post_meta(3,'Edad',false);
        $content.="la edad es ". $valor[0];
        $valor=get_post_meta(1,'NombrePadreAutor',false);
        $content.="El Nombre del Padre ". $valor[0];
        $valor=get_post_meta(2,'NombreMadreAutor',false);
        $content.="El Nombre de la Madre ". $valor[0];
    
        return $content;
    }
}
add_filter('the_content','show_fields_post')

?>