<?php
/**
 * This file belongs to the YITH MJPA Plugin Addons
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Framework_Printer' ) ) {
	/**
	 * YITH_MJPA_Framework_Printer
	 */
	class YITH_MJPA_Framework_Printer {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Framework_Printer
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Framework_Printer Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
		}
		public function add_input_styles() {
			wp_register_style( 'yith-mjpa-style', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style' );
			// wp_enqueue_style( 'wp-color-picker' );
		}
		/**
		 * Yith_mjpa_print_input
		 *
		 * @param array mixed $arr_attr_input_types array.
		 * @return void
		 */
		public static function yith_mjpa_print_input( $arr_attr_input_types ) {
			foreach ( $arr_attr_input_types as $key => $value ) {
				yith_mjpa_get_view( '/input-fields/yith-mjpa-' . $value['type'] . '.php', $value );
			}
		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {

			wp_register_script( 'yith_mjpa_button_addon', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-button-addon.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith_mjpa_button_addon' );
			wp_localize_script(
				'yith_mjpa_button_addon',
				'array_aux',
				array(
					'ajaxurl' => admin_url( 'admin-ajax.php' ),
				)
			);
			// wp_enqueue_script( 'mjpa-admin-color-picker', YITH_MJPA_DIR_ASSETS_JS_URL . '/mjpa-admin-color-picker.js', array( 'wp-color-picker' ), false, true );
		}
		/**
		 * Yith_mjpa_print_radio_options
		 *
		 * @param option mixed $option args.
		 * @return void
		 */
		public function yith_mjpa_print_radio_options( $option ) {
			yith_mjpa_get_view( '/input-fields/yith-mjpa-radio-options.php', $option );
		}

	}
}
