<?php
$name_id_field        = 'text_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];
?>

<div>
  <p class="yith_mjpa_description_addon"><?php print_addons_value( $args[0]['textarea_field'] ); ?></p>
	<input class="yith_mjpa_input_text yith_price_addon" type="text" name="<?php echo ( esc_html( $name_id_field ) ); ?>" id="<?php echo ( esc_html( $name_id_field ) ); ?>">
  <p class="yith_price_addon_value <?php echo ( esc_html( $yith_mjpa_price_type ) ); ?>">+0.00$</p>
</div>
