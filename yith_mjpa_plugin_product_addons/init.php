<?php

/*
 * Plugin Name: YITH MJPA Plugin Product Addons
 * Description: Skeleton for YITH Plugins
 * Version: 1.0.0
 * Author: Manuel Jesús Peraza Alonso
 * Author URI: https://yithemes.com/
 * Text Domain: yith_mjpa_plugin_product_addons
 */

! defined( 'ABSPATH' ) && exit;   // Before all, check if defined ABSPATH.

/*Create some constant where defined PATH for Style, Assets, Templates, Views */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	define( 'YITH_MJPA_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_MJPA_DIR_URL' ) ) {
	define( 'YITH_MJPA_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_URL' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_URL', YITH_MJPA_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_CSS_URL', YITH_MJPA_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_MJPA_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_MJPA_DIR_ASSETS_JS_URL', YITH_MJPA_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_MJPA_DIR_PATH' ) ) {
	define( 'YITH_MJPA_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_MJPA_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_MJPA_DIR_INCLUDES_PATH', YITH_MJPA_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_MJPA_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_MJPA_DIR_TEMPLATES_PATH', YITH_MJPA_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_MJPA_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_MJPA_DIR_VIEWS_PATH', YITH_MJPA_DIR_PATH . '/views' );
}

/**
 * Include the scripts
 */
if ( ! function_exists( 'yith_mjpa_init_classes' ) ) {

	function yith_mjpa_init_classes() {

		load_plugin_textdomain( 'yith-plugin-skeleton', false, basename( dirname( __FILE__ ) ) . '/languages' );

		// Require all the files you include on your plugins. Example
		require_once YITH_MJPA_DIR_INCLUDES_PATH . '/class-yith-mjpa-plugin-product-addons.php';

		if ( class_exists( 'YITH_MJPA_Plugin_Products_Addons' ) ) {
			/*
			*	Call the main function
			*/
			yith_mjpa_plugin_products_addons();
		}
	}
}


add_action( 'plugins_loaded', 'yith_mjpa_init_classes', 11 );
