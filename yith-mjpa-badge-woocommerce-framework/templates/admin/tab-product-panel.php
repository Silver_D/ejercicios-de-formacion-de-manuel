<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

$mjpa_product = wc_get_product( $post );
$mjpa_post_id = '';

if ( $mjpa_product instanceof WC_Product ) {
	$mjpa_post_id = $mjpa_product->get_id();

} else {
	if ( ! empty( $_GET['product_id'] ) ) {
		$mjpa_product = wc_get_product( $_GET['product_id'] );
		$mjpa_post_id = ( isset( $mjpa_product ) && $mjpa_product instanceof WC_Product ) ? $mjpa_product->get_id() : '';
	}
}

?>
	<div>
		<h3><?php _e( 'Badge Text Title', 'yith-mjpa-text-domain' ); ?></h3>
	</div>

<?php

do_action( 'yith_before_auction_tab', $mjpa_post_id );
