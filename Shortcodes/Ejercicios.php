<?php

/*
Plugin Name: Ejercicios-ShortCodes
Plugin URI: 
Description: Ejercicios-ShortCodes
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/
//Crear un shortcode que le pase el post_id y me devuelva el content

add_shortcode( 'get_content_post', 'getting_post_content' );

    function inicializar_shortcode(){
        apply_filters( 'the_content', 'getting_post_content' );
        function getting_post_content($id) {
            $var=get_post($id['id'])->post_content;
            echo $var;
        }
    }
add_action('init', 'inicializar_shortcode');

//Crear un shortcode que me liste los últimos 5 post que se han creado en la web

add_shortcode( 'list_last_posts', 'getting_last_posts' );

    function inicializar_shortcode2(){
        apply_filters( 'the_content', 'getting_last_posts' );
        function getting_last_posts() {
            $var=wp_get_recent_posts(array('numberposts'=> 5));;
            foreach ($var as $value) {
                echo "<p>".$value['post_title']."</p>";
            }
        }
    }
add_action('init', 'inicializar_shortcode2');

//Crear un shortcode que muestre los datos de un usuario --> nombre, apellidos, nickname


function ready_to_show_content_post(){
    add_shortcode('yith_list_user_info', 'yith_getting_user_info');
}
add_action('init', 'ready_to_show_content_post');

function yith_getting_user_info($attr) {
    ob_start();
   // $identificador=$attr['id'];
    $identificador=apply_filters("yith_swap_value", $attr);
    $nick=get_user_meta($identificador, 'nickname');
    $nombre=get_user_meta($identificador, 'first_name');
    $apellidos=get_user_meta($identificador, 'last_name');
    do_action("hook_before_print");
    echo "<p>" . $nick[0] . $nombre[0] . $apellidos[0] . "</p>";
    do_action("hook_after_print",$identificador);
    return ob_get_clean() ;
    
}
add_filter( 'yith_swap_value', 'modify_id' );
function modify_id( $value  ) {

    if ($value["id"]!=1 && $value["id"]!=2) {
        $error="No se puede acceder a esos id";
        echo $error;
    }
    else{
        $success="Se muestra la información del id 2 porque me apetece";
        echo $success;
        $value["id"] = 2;  
    }
  return $value;
}


add_action('hook_before_print', 'yith_print_hola');
add_action("hook_after_print", 'yith_print_adios');


function yith_print_hola(){
    echo "hola";
}
function yith_print_adios($id){
    echo "adios" . $id["id"];
}

if( !function_exists('yith_carlos_function') ) {

    function yith_carlos_function() {

        echo '<p>Carlos es el mejor de todos los profesores</p>';

    }

    add_action( 'hook_before_print', 'yith_carlos_function',1 );

} 

add_action("wp_loaded","yith_bye_carlos");
function yith_bye_carlos(){
    remove_action("hook_before_print", "yith_carlos_function",1);
}

/*
////////////////EJEMPLO////////////////////
function suma ( ) {
    $a = apply_filters('yith_carlos_valor_de_a', 3 );
    $b = 2;
    $c = $a + $b
    return $c 
  }
add_filter( 'yith_carlos_valor_de_a', 'modify_a_value' );
function modify_a_value( $value  ) {
  error_log( print_r( $value ,true) );  // En este punto de la función $value sería = 3
  $value = 5;  
  error_log( print_r( $value ,true) );  // En este punto de la función $value sería = 5

  return $value;
}*/
?>

 