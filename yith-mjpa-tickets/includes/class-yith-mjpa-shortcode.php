<?php
/**
 * This file belongs to the YITH MJPA Tickets
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Shortcode_Tickets' ) ) {
	/**
	 * YITH_MJPA_Shortcode_Tickets
	 */
	class YITH_MJPA_Shortcode_Tickets {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Shortcode_Tickets
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Shortcode_Tickets Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_mjpa_add_shortcode' ) );
			add_action( 'wp_ajax_nopriv_ticket_variation', array( $this, 'yith_mjpa_receive_ajax_call_ticket' ) );
			add_action( 'wp_ajax_ticket_variation', array( $this, 'yith_mjpa_receive_ajax_call_ticket' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_input_scripts' ) );
		}
				/**
				 * Add_input_scripts
				 *
				 * @return void
				 */
		public function add_input_scripts() {
			wp_register_script( 'yith-mjpa-script-ticket-front', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-script-product-type-ticket-admin.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-mjpa-script-ticket-front' );
			wp_localize_script(
				'yith-mjpa-script-ticket-front',
				'attr_ajax',
				array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
				)
			);

		}
		/**
		 * Yith_mp_add_shortcode
		 *
		 * @return void
		 */
		public function yith_mjpa_add_shortcode() {
			add_shortcode( 'yith_mjpa_show_shortcode', array( $this, 'yith_mjpa_call_shortcode' ) );
		}

		/**
		 * Yith_mp_setup_testimonials
		 *
		 * @return void
		 */
		public function yith_mjpa_call_shortcode( $atts ) {
			global $wpdb;
			$product_id   = $atts['id'];
			$order_status = array( 'wc-completed', 'wc-processing' );
			$results      = $wpdb->get_col(
				"
				SELECT order_items.order_id
				FROM {$wpdb->prefix}woocommerce_order_items as order_items
				LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
				LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
				WHERE posts.post_type = 'shop_order'
				AND posts.post_status IN ( '" . implode( "','", $order_status ) . "' )
				AND order_items.order_item_type = 'line_item'
				AND order_item_meta.meta_key = '_product_id'
				AND order_item_meta.meta_value = '$product_id'
			"
			);
			ob_start();
			$attr     = array();
			$aux_attr = array();
			array_push( $attr, array( 'id' => $product_id ) );
			foreach ( $results as $key => $value ) {
				if ( ! in_array( $value, $aux_attr, true ) ) {
					array_push( $aux_attr, $value );
				}
			}
			array_push( $attr, $aux_attr );
			yith_mjpa_get_template( '/frontend/yith_mjpa_front_ticket_info.php', $attr );
			return ob_get_clean();
		}

		/**
		 * Yith_mjpa_receive_ajax_call
		 *
		 * @return void
		 */
		public function yith_mjpa_receive_ajax_call_ticket() {
			$yith_mjpa_post_id = isset( $_POST['yith_mjpa_post_id'] ) ? $_POST['yith_mjpa_post_id'] : false;//phpcs:ignore
			 error_log( print_r( $yith_mjpa_post_id, true ) );
			if ( 'yith_no_check' === get_post_status( $yith_mjpa_post_id ) ) {
				wp_update_post(
					array(
						'ID'          => $yith_mjpa_post_id,
						'post_status' => 'yith_check',
					)
				);
			} else {
				wp_update_post(
					array(
						'ID'          => $yith_mjpa_post_id,
						'post_status' => 'yith_no_check',
					)
				);
			}
			wp_die();
		}
	}
}
