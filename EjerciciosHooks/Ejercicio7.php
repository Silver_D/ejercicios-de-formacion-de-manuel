<?php

/*
Plugin Name: Ejercicio7
Plugin URI: 
Description: Ejercicio7
Version: 1.0
Author: Manuel
Author URI: 
License: 
*/
/*
Para no mostrar el "Creado por Manuel" usamos la prioridad para aplicar el cambio al final.
*/
function filter_the_title( $title, $id ) { 
    if ($id==28) {
        return $title . " Creado por Manuel"; 

    }
    else{
        return $title;
    }
}; 
function add_date($content){
    if (get_the_ID()==28) {
        $content.= get_the_date();
    }
    
    return $content;
}
function change_author (){
    global $post;
    $var=$post->post_title;
    return $var;
 
}
// add the filter 
add_filter( 'the_title', 'filter_the_title', 10, 2 ); 
add_filter( 'the_title', 'change_author', 25 ,0); 
add_filter ('the_content', 'add_date');
?>