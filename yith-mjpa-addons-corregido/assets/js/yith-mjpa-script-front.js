
jQuery(document).ready(function ($) {
    let total = 0;
    let product_price = 0;
    let concat_names = ''

    $(".yith_mjpa_title_addon").each(function (index) {
        concat_names += $(this).text() + ',';
    })
    $(".yith_mjpa_hidden_input_names").val(concat_names)
    $(".yith_price_addon_value").each(function (index) {
        let aux = $(this).text().slice(1, $(this).text().length)
        aux = aux.split('.');
        aux = parseInt(aux[0]);
        total = total + aux
    });
    if (product_price[1] != undefined) {
        product_price = ($(".yith_mjpa_product_price").text()).split('+');
        product_price = product_price[1].split('.')
        product_price = parseInt(product_price[0])
        $(".yith_mjpa_total_addon_price").text("Additional options total: " + "+" + total + ".00")
        $(".yith_mjpa_total_price").text("Total: " + "+" + (total + product_price) + ".00")
    }

    $(".yith_mjpa_input_text").on('input', function () {
        let actual_id = $(this).attr('id').split('_');
        actual_id = actual_id[2]
        let addon = yith_mjpa_vars['addons'][actual_id]
        let text = $(this).val().length;
        switch (addon['radio_field']) {
            case 'free':
                $(this).next().text('+0.00$')
                break;
            case 'fixed_price':
                if (text - addon['free-characters-field'] > 0) {
                    $(this).next().text('+' + addon['price_field'] + '.00')
                } else {
                    $(this).next().text('+0.00$')
                }
                break;
            case 'price_per_character':
                real_length = text - addon['free-characters-field']
                if (real_length > 0) {
                    $(this).next().text('+' + (addon['price_field'] * real_length) + '.00')
                } else {
                    $(this).next().text('+0.00$')
                }
                break;

            default:
                break;
        }
    });
    $(".yith_mjpa_input_select").change(function () {
        let actual_id = $(this).attr('id').split('_');
        actual_id = actual_id[2]
        let addon = yith_mjpa_vars['addons'][actual_id]
        if (addon['radio_field'] != 'free') {
            $(this).next().text('+' + addon['price_field_option'][$(this).prop('selectedIndex')] + ".00$")
        }
    });
    $(".yith_mjpa_input_radio").change(function () {
        let actual_id = $(this).attr('id').split('_');
        actual_id = actual_id[2]
        let addon = yith_mjpa_vars['addons'][actual_id]
        if (addon['radio_field'] != 'free') {
            let radius_value = $(this).parent().parent().find("input:checked").val()
            let index = addon['name_field_option'].indexOf(radius_value)
            $(this).parent().parent().children().last().text('+' + addon['price_field_option'][index] + ".00$")
        }
    });
    $(".yith_mjpa_input_checkbox").change(function () {
        let actual_id = $(this).attr('id').split('_');
        actual_id = actual_id[2]
        let addon = yith_mjpa_vars['addons'][actual_id]
        if ($(this).prop('checked') && addon['price_field'] != '') {
            $(this).parent().parent().children().last().text('+' + addon['price_field'] + ".00$")
        } else {
            $(this).parent().parent().children().last().text("+0.00$")
        }

    });
    $(".yith_mjpa_input_onoff").change(function () {
        let actual_id = $(this).attr('id').split('_');
        actual_id = actual_id[2]
        let addon = yith_mjpa_vars['addons'][actual_id]
        if ($(this).prop('checked') && addon['price_field'] != '') {
            $(this).parent().parent().parent().children().last().text('+' + addon['price_field'] + ".00$")
        } else {
            $(this).parent().parent().parent().children().last().text("+0.00$")
        }

    });
    $(".yith_price_addon").change(function () {
        let total = 0;
        let product_price = 0;
        let input_content = '';
        $(".yith_price_addon_value").each(function (index) {
            let aux = $(this).text().slice(1, $(this).text().length)
            aux = aux.split('.');
            aux = parseInt(aux[0]);
            input_content = input_content + ',' + aux.toString()
            total = total + aux
            product_price = ($(".yith_mjpa_product_price").text()).split('+');
            product_price = product_price[1].split('.')
            product_price = parseInt(product_price[0])
        });
        $(".yith_mjpa_total_addon_price").text("Additional options total: " + "+" + total + ".00")
        $(".yith_mjpa_total_price").text("Total: " + "+" + (total + product_price) + ".00")
        $(".yith_mjpa_hidden_input_price").val(input_content)
    });
    $(function () {
        yith_mjpa_calculate_totals = function () {
            let total = 0;
            let product_price = 0;
            let input_content = '';
            $(".yith_price_addon_value").each(function (index) {
                let aux = $(this).text().slice(1, $(this).text().length)
                aux = aux.split('.');
                aux = parseInt(aux[0]);
                input_content = input_content + ',' + aux.toString()
                total = total + aux
                product_price = ($(".yith_mjpa_product_price").text()).split('+');
                product_price = product_price[1].split('.')
                product_price = parseInt(product_price[0])
            });
            $(".yith_mjpa_total_addon_price").text("Additional options total: " + "+" + total + ".00")
            $(".yith_mjpa_total_price").text("Total: " + "+" + (total + product_price) + ".00")
            $(".yith_mjpa_hidden_input_price").val(input_content)
        }
    })
    jQuery(function ($) {
        yith_mjpa_calculate_totals()
    });

});
