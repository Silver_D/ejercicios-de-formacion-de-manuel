<?php
require_once 'YITH-mp-html-testimonial-format.php';

function getting_print_testimonials($attr) {
/**
 * Recogemos los últimos 6 testimonios.
 */

//error_log ( print_r(  get_post_meta(134,"info",'yes'), true ) );
 /**
  * parámetros 
  *int number
  *string ids
  *string show_image
  *string hover_effect   [zoom,highligh,default]
  */
  if (isset($attr["hover_effect"])) {
    $hover_effect=$attr["hover_effect"];
  }
  else{
    $hover_effect="default";
  }
  if (isset($attr["show_image"])) {
    $show_image=$attr["show_image"];
  }
  else{
    $show_image="yes";
  }
  if (isset($attr["ids"])) {
    $aux=$attr["ids"];
    $ids = explode(",", $aux);
    $number_of_testimonials=sizeof($ids);
  }
  if (isset($attr["number"]) && !isset($attr["ids"])) {
      $number_of_testimonials=$attr["number"];
  }
  if(!isset($attr["number"]) && !isset($attr["ids"])){
    $number_of_testimonials=6;
  }
  
    if(isset($attr["ids"])){
      $arguments = array(
        'numberposts' => $number_of_testimonials,
        'post_type'   => 'yith_testimonials',
        'include'     => $ids
        );
      $testimonials_posts=get_posts($arguments);
    }
    else{
      $arguments = array(
        'numberposts' => $number_of_testimonials,
        'post_type'   => 'yith_testimonials'
        );
      $testimonials_posts = get_posts( $arguments );
    }
    
    ob_start();
    echo "<div class='format_columns_testimonials'>";
    foreach ($testimonials_posts as $key => $value) {
        formatting_html_testimonials($value,$show_image,$hover_effect);
    }
    echo "</div>";
    return ob_get_clean();
}

function yith_print_init_shortcode(){
    add_shortcode( 'print_testimonials', 'getting_print_testimonials' );
}
add_action('init', 'yith_print_init_shortcode');

?>