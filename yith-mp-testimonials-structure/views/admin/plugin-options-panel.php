<div class="wrap">
	<h1><?php esc_html_e( 'Settings', 'yith-plugin-skeleton' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'ps-options-page' );
			do_settings_sections( 'ps-options-page' );
			submit_button();
		?>

	</form>
</div>
