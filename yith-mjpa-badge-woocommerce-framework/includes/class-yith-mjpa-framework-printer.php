<?php
/**
 * This file belongs to the YITH_MJPA_BAGDE_WOOCOMMERCE_EXERCISE
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Framework_Printer' ) ) {
	/**
	 * YITH_MJPA_Framework_Printer
	 */
	class YITH_MJPA_Framework_Printer {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Framework_Printer
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Framework_Printer Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			// HERE ACTIONS
		}

		/**
		 * Yith_mjpa_print_input
		 *
		 * @param array mixed $arr_attr_input_types array.
		 * @return void
		 */
		public function yith_mjpa_print_input( $arr_attr_input_types ) {
			// se recibe todo el objeto y se recorre, permitiendo que las options se generen dentro o bien con una llamada desde la view o desde aquí.
			echo( "<div id='badge_options' class='panel woocommerce_options_panel'>" );
			foreach ( $arr_attr_input_types as $key => $value ) {
				yith_mjpa_get_view( '/input-fields/yith-mjpa-' . $value['type'] . '.php', $value );
			}
			echo( '</div>' );
		}
		/**
		 * Yith_mjpa_print_radio_options
		 *
		 * @param option mixed $option args.
		 * @return void
		 */
		public function yith_mjpa_print_radio_options( $option ) {
			yith_mjpa_get_view( '/input-fields/yith-mjpa-radio-options.php', $option );
		}

	}
}
