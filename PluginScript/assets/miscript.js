
function change_display() {
    let element = document.getElementById("data")
    if (element.style.display == "none" || element.style.display == "") {
        element.style.display = "block";
    } else {
        element.style.display = "none";
    }
}