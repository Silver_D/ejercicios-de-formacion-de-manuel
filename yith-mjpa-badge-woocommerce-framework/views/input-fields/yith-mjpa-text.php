<?php
		global $post;
if ( ! function_exists( 'default_color_input' ) ) {
	/**
	 * default_color_input
	 *
	 * @return void
	 */
	function default_color_input( $args, $post ) {
		$aux = get_post_meta( $post->ID, $args['id_field'], true );
		if ( ( '' === $aux ) ) {
			echo( esc_html( $args['default_color_picker'] ) );
		} else {
			echo( esc_html( $aux ) );
		}
	}
}
if ( ! function_exists( 'get_default_text_value' ) ) {
	/**
	 * Get_default_text_value
	 *
	 * @return void
	 */
	function get_default_text_value( $args, $post ) {
		$aux = get_post_meta( $post->ID, $args['id_field'], true );
		if ( ( '' === $aux ) ) {
			echo( esc_html( $args['default_text_value'] ) );
		} else {
			echo( esc_html( $aux ) );
		}
	}
}

?>
<div class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>">
	<p class="form-field ">
		<label for="<?php echo( esc_html( $args['id_field'] ) ); ?>" ><?php echo( esc_html( $args['label_field'] ) ); ?></label>
		<input value="<?php get_default_text_value( $args, $post ); ?>" type="<?php echo( esc_html( $args['type'] ) ); ?>" name="<?php echo( esc_html( $args['name_field'] ) ); ?>" id="<?php echo( esc_html( $args['id_field'] ) ); ?>" class="<?php echo( esc_html( $args['class_field'] ) ); ?>" data-default-color="<?php default_color_input( $args, $post ); ?>">
	</p>
</div>
