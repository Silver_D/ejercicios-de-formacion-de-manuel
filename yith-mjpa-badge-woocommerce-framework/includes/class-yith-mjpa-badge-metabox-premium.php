<?php
/**
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Badge_Metabox_Premium' ) ) {
	/**
	 * YITH_MJPA_Badge_Metabox_Premium
	 */
	class YITH_MJPA_Badge_Metabox_Premium {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Badge_Metabox_Premium
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Badge_Metabox_Premium Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'yith_before_auction_tab', array( $this, 'yith_before_auction_tab' ) );
			add_action( 'yith_after_auction_tab', array( $this, 'yith_after_auction_tab' ) );
		}

		/**
		 * YITH before auction tab
		 *
		 * Add input in auction tab
		 *
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 * @since 1.0.11
		 */

		public function yith_before_auction_tab( $post_id ) {

			$product = wc_get_product( $post_id );

			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Enable Plugin:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-onoff',
						'type'  => 'onoff',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_onoff_enable', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_onoff_enable', true ) : 'no',
						'id'    => '_yith_mjpa_onoff_enable',
						'name'  => '_yith_mjpa_onoff_enable',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Note Label:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-text',
						'type'  => 'text',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_note_label', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_note_label', true ) : '',
						'id'    => '_yith_mjpa_note_label',
						'name'  => '_yith_mjpa_note_label',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Text area:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-textarea',
						'type'  => 'textarea',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_textarea', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_textarea', true ) : '',
						'id'    => '_yith_mjpa_textarea',
						'name'  => '_yith_mjpa_textarea',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-mjpa-flex yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Choose type of text:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class'   => 'yith-mjpa-product-metabox-radio',
						'type'    => 'radio',
						'value'   => ( get_post_meta( $post_id, '_yith_mjpa_radio', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_radio', true ) : 'text',
						'options' => array(
							'text'     => 'Text',
							'textarea' => 'Textarea',
						),
						'id'      => '_yith_mjpa_radio',
						'name'    => '_yith_mjpa_radio',
					),
					'deps'   => array(
						'target_id' => '_yith_mjpa_onoff_enable',
						'value'     => 'no',
						'type'      => 'hide',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-mjpa-flex yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Choose type of price:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class'   => 'yith-mjpa-product-metabox-radio-price',
						'type'    => 'radio',
						'value'   => ( get_post_meta( $post_id, '_yith_mjpa_radio_price', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_radio_price', true ) : 'free',
						'options' => array(
							'free'                => 'Free',
							'fixed_price'         => 'Fixed Price',
							'price_per_character' => 'Price Per Character',
						),
						'id'      => '_yith_mjpa_radio_price',
						'name'    => '_yith_mjpa_radio_price',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Price:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-number-price',
						'type'  => 'number',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_number_price', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_number_price', true ) : '0',
						'id'    => '_yith_mjpa_number_price',
						'name'  => '_yith_mjpa_number_price',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Free Characters:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-number-free-characters',
						'type'  => 'number',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_number_free_characters', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_number_free_characters', true ) : '0',
						'id'    => '_yith_mjpa_number_free_characters',
						'name'  => '_yith_mjpa_number_free_characters',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Allow Badge:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-checkbox-allow-badge',
						'type'  => 'checkbox',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_checkbox_allow_badge', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_checkbox_allow_badge', true ) : 'no',
						'id'    => '_yith_mjpa_checkbox_allow_badge',
						'name'  => '_yith_mjpa_checkbox_allow_badge',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-wcact-form-field form-field yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Badge Text:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'class' => 'yith-mjpa-product-metabox-text',
						'type'  => 'text',
						'value' => ( get_post_meta( $post_id, '_yith_mjpa_badge_text', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_badge_text', true ) : '',
						'id'    => '_yith_mjpa_badge_text',
						'name'  => '_yith_mjpa_badge_text',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Badge Background Color:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'type'    => 'colorpicker',
						'value'   => ( get_post_meta( $post_id, '_yith_mjpa_color_picker_background_badge_color', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_color_picker_background_badge_color', true ) : '#ff5030',
						'id'      => '_yith_mjpa_color_picker_background_badge_color',
						'name'    => '_yith_mjpa_color_picker_background_badge_color',
						'default' => '#ff5030',
					),
				)
			);
			yith_mjpa_metabox_form_field(
				array(
					'class'  => 'yith-plugin-ui yith_mjpa_form_field_selector',
					'title'  => esc_html__( 'Badge Text Color:', 'yith-auctions-for-woocommerce' ),
					'desc'   => esc_html__( 'This is a description' ),
					'fields' => array(
						'type'    => 'colorpicker',
						'value'   => ( get_post_meta( $post_id, '_yith_mjpa_color_picker_text_badge_color', true ) ) ? get_post_meta( $post_id, '_yith_mjpa_color_picker_text_badge_color', true ) : '#ff5030',
						'id'      => '_yith_mjpa_color_picker_text_badge_color',
						'name'    => '_yith_mjpa_color_picker_text_badge_color',
						'default' => '#ff5030',
					),
				)
			);
		}
	}
}
