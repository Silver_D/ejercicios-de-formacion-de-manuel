<?php
	global $post;
?>
<p class="form-field  <?php echo( esc_html( $args['class_field_show'] ) ); ?> <?php
if ( 'fixed_price' === $args['value_field'] ) {
	echo( 'fixed_price_field_class' );
}
?>
">
	<input 			
	<?php
	if ( ( 'true' === $args['default'] ) ) {
		echo( 'checked' );
	}
	?>
		type="<?php echo( esc_html( $args['type'] ) ); ?>" id="<?php echo( esc_html( $args['id_field'] ) ); ?>" name="<?php echo( esc_html( $args['name_field'] ) ); ?>" class="<?php echo( esc_html( $args['class_field'] ) ); ?>" value="<?php echo( esc_html( $args['value_field'] ) ); ?>" >
	<label for="<?php echo( esc_html( $args['id_field'] ) ); ?>"><?php echo( esc_html( $args['label_field'] ) ); ?></label>
</p>
