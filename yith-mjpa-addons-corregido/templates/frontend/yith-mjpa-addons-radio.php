<?php
$name_id_field        = 'radio_field_' . $args[1];
$yith_mjpa_price_type = 'yith_mjpa_price_type' . $args[1];

?>

<div>
	<p class="yith_mjpa_description_addon"><?php print_addons_value( $args[0]['textarea_field'] ); ?></p>
	<?php
	foreach ( $args[0]['name_field_option'] as $key => $value ) {
		echo( '<div class="yith_mjpa_radio_flex">' );
		if ( $value === $args[0]['name_field_option'][0] ) {
			echo( '<input class="yith_mjpa_input_radio yith_price_addon" checked type="radio" id="' . esc_html( $name_id_field ) . '" name="' . esc_html( $name_id_field ) . '" value="' . esc_html( $value ) . '">' );
		} else {
			echo( '<input class="yith_mjpa_input_radio yith_price_addon" type="radio" id="' . esc_html( $name_id_field ) . '" name="' . esc_html( $name_id_field ) . '" value="' . esc_html( $value ) . '">' );
		}
		echo( ' <label for=' . esc_html( $name_id_field ) . '>' . esc_html( $value ) . '</label>' );
		echo( '</div>' );
	}
	?>
	</select>
	<?php
	if ( 'free' === $args[0]['radio_field'] ) {
		echo( '<p class="yith_mjpa_price_radio yith_price_addon_value">+0.00$</p>' );
	} else {
		echo( '<p class="yith_mjpa_price_radio yith_price_addon_value">+' . $args[0]['price_field_option'][0] . '.00$</p>' );
	}
	?>
</div>
