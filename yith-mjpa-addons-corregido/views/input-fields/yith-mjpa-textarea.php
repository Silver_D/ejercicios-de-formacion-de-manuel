<?php
	global $post;
	// falta añadir el valor del textarea desde la bdd
?>
<div class="options_group <?php echo( esc_html( $args['class_field_show'] ) ); ?>">
	<p class="form-field ">
		<label for="<?php echo( esc_html( $args['id_field'] ) ); ?>" class=""><?php echo( esc_html( $args['label_field'] ) ); ?></label>
		<textarea class="<?php echo( esc_html( $args['class_field'] ) ); ?>" id="<?php echo( esc_html( $args['id_field'] ) ); ?>" name="<?php echo( esc_html( $args['name_field'] ) ); ?>" rows="2" cols="20"><?php echo( esc_html( $args['default_input'] ) ); ?></textarea>
	</p>
</div>
