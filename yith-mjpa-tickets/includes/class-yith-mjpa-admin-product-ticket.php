<?php
/**
 * This file belongs to the YITH MJPA Ticket.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http: //www.gnu.org/licenses/gpl-3.0.txt
* phpcs:ignore Wordpress.Security.ValidatedSanitizedInput.InputNotSanitized
 *
 * @package  WordPress
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}
if ( ! class_exists( 'YITH_MJPA_Product_Type_Tickets' ) ) {
	/**
	 * YITH_MJPA_Product_Type_Tickets
	 */
	class YITH_MJPA_Product_Type_Tickets extends WC_Product {
		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Product_Type_Tickets
		 * @since 1.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Get_instance
		 *
		 * @return YITH_MJPA_Product_Type_Tickets Main instance.
		 * @author Manuel Jesús Peraza Alonso.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() {
			$this->product_type = 'event_ticket';
			parent::__construct();
			add_filter( 'product_type_selector', array( $this, 'add_ticket_product_type' ) );
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'ticket_product_tab' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_styles' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'add_input_scripts' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'event_ticket_tab_product_tab_content' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_event_ticket_product_settings' ) );
			add_action( 'woocommerce_single_product_summary', 'ticket_product_front' );
		}

		/**
		 * Add_frontend_styles
		 *
		 * @return void
		 */
		public function add_input_styles() {

			wp_register_style( 'yith-mjpa-style-product-type-ticket-admin', YITH_MJPA_DIR_ASSETS_CSS_URL . '/yith-mjpa-style-product-type-ticket-admin.css', array(), true );
			wp_enqueue_style( 'yith-mjpa-style-product-type-ticket-admin' );

		}
		/**
		 * Add_input_scripts
		 *
		 * @return void
		 */
		public function add_input_scripts() {

			wp_register_script( 'yith-mjpa-script-product-type-ticket-admin', YITH_MJPA_DIR_ASSETS_JS_URL . '/yith-mjpa-script-product-type-ticket-admin.js', array( 'jquery' ), 1, false );
			wp_enqueue_script( 'yith-mjpa-script-product-type-ticket-admin' );

		}
		/**
		 * Add_ticket_product_type
		 *
		 * @param Types mixed $types types.
		 * @return types void
		 */
		public function add_ticket_product_type( $types ) {
			$types['event_ticket'] = __( 'Event Ticket', 'mjpa_language' );
			return $types;
		}
		/**
		 * Ticket_product_tab
		 *
		 * @param Tabs mixed $tabs tabs.
		 * @return tabs void
		 */
		public function ticket_product_tab( $tabs ) {

			$tabs['event_ticket'] = array(
				'label'  => __( 'Event Ticket', 'mjpa_language' ),
				'target' => 'event_ticket_product_options',
				'class'  => 'show_if_event_ticket_product',
			);
			return $tabs;
		}
		/**
		 * Event_ticket_tab_product_tab_content
		 *
		 * @return void
		 */
		public function event_ticket_tab_product_tab_content() {

			?>
			<div id='event_ticket_product_options' class='panel woocommerce_options_panel'>
				<div class='options_group'>
			<?php

			woocommerce_wp_text_input(
				array(
					'id'          => 'event_ticket_product_info',
					'label'       => __( 'Event Ticket Label', 'mjpa_language' ),
					'placeholder' => '',
					'desc_tip'    => 'true',
					'description' => __( 'Enter Event Ticket product Info.', 'mjpa_language' ),
					'type'        => 'text',
				)
			);
			?>
				</div>
			</div>
			<?php
		}
		/**
		 * Save_event_ticket_product_settings
		 *
		 * @param post_id mixed $post_id post_id.
		 * @return void
		 */
		public function save_event_ticket_product_settings( $post_id ) {

			$event_ticket_product_info = $_POST['event_ticket_product_info'];

			if ( ! empty( $event_ticket_product_info ) ) {

				update_post_meta( $post_id, 'event_ticket_product_info', esc_attr( $event_ticket_product_info ) );
			}
		}

		/**
		 * Ticket_product_front
		 *
		 * @return void
		 */
		public function ticket_product_front() {
			global $product;

			if ( 'event_ticket' == $product->get_type() ) {
				echo( get_post_meta( $product->get_id(), 'event_ticket_product_info' )[0] );

			}
		}
	}
}
