
jQuery(document).ready(function ($) {
    //Plugin Enable
    if ($('.class_checkbox_enable').prop('checked')) {

        $('.class_show').show();
        if (($('.class_show_free').value === 'free')) {
            $('.class_show_price').hide();
        }
        else {
            $('.class_show_price').show();
        }
        if ($('.class_badge_enable').prop('checked')) {
            $('.class_show_badge').show();
        } else {
            $('.class_show_badge').hide();
        }

    } else {
        $('.class_show').hide();
    }

    if ($('.class_checkbox_enable').change(function () {
        if (!$(this).prop('checked')) {
            $('.class_show').hide();
        } else {
            $('.class_show').show();
            badge_init();
            init_radio();
        }
    }));
    //Price display
    function init_radio() {
        if (($('.class_show_free').value === undefined) && ($('.class_checkbox_enable').prop('checked'))) {
            $('.class_show_price').hide();
        }
    }
    function changeHandler(event) {
        if (this.value === 'free') {
            $('.class_show_price').hide();
        } if (this.value === 'fixed_price') {
            $('.class_show_price').show();
        } if (this.value === 'price_per_character') {
            $('.class_show_price').show();
        }
    }
    let radios = document.querySelectorAll('input[type=radio][name="_radio_price_type"]');
    Array.prototype.forEach.call(radios, function (radio) {
        radio.addEventListener('change', changeHandler);
    });
    //Badge Display
    function badge_init() {
        console.log("hola");
        if ($('.class_badge_enable').prop('checked')) {
            $('.class_show_badge').show();
        } else {
            $('.class_show_badge').hide();
        }
    }

    if ($('.class_badge_enable').change(function () {
        if (!$('.class_badge_enable').prop('checked')) {
            $('.class_show_badge').hide();
        } else {
            $('.class_show_badge').show();
            init_radio();
        }
    }));
});