<?php
/**
 * This file belongs to the YITH MJPA Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_MJPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_MJPA_Badge_Woocommerce_Admin' ) ) {

	/**
	 * YITH_MJPA_Badge_Woocommerce_Admin
	 */
	class YITH_MJPA_Badge_Woocommerce_Admin {

		/**
		 * Main Instance
		 *
		 * @var YITH_MJPA_Badge_Woocommerce_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_MJPA_Badge_Woocommerce_Admin Main instance
		 * @author Manuel Jesús Peraza Alonso
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * YITH_MJPA_Badge_Woocommerce_Admin constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_order_item_name', array( $this, 'yith_mjpa_create_new_order_line_order_admin' ), 10, 2 );
		}
		/**
		 * Yith_mjpa_create_new_order_line_order_admin
		 *
		 * @param product_name mixed $product_name product_name.
		 * @param item mixed         $item item.
		 * @return product_name void
		 */
		public function yith_mjpa_create_new_order_line_order_admin( $product_name, $item ) {
			if ( isset( $item['yith_mjpa_input_personalize_id'] ) ) {
				$product_name .= sprintf(
					'<p>%s: %s</p>',
					'Your name',
					esc_html( $item['yith_mjpa_input_personalize_id'] )
				);
			}
			return $product_name;
		}
	}
}
