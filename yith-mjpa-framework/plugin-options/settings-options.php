<?php
$settings = array(
	'settings' => array(
		'section-1' => array(
			array(
				'name' => __( 'Section Title', 'yith-mjpa-plugin-framework' ),
				'type' => 'title',
			),
			array(
				'id'   => 'yith-mjpa-number-testimonials',
				'name' => __( 'Number', 'yith-mjpa-plugin-framework' ),
				'type' => 'number',
				'min'  => '0',
				'max'  => '50',
				'step' => '1',
				'std'  => '0',
				'desc' => __( 'This is a number field', 'yith-mjpa-plugin-framework' ),
			),
			array(
				'id'   => 'yith-mjpa-checkbox',
				'name' => __( 'Show Image', 'yith-mjpa-plugin-framework' ),
				'type' => 'checkbox',
				'desc' => __( 'This is a checkbox field', 'yith-mjpa-plugin-framework' ),
			),
			array(
				'id'      => 'yith-mjpa-select',
				'name'    => __( 'Select Effect', 'yith-mjpa-plugin-framework' ),
				'type'    => 'select',
				'class'   => 'wc-enhanced-select',
				'options' => array(
					'zoom'      => 'Zoom',
					'highlight' => 'Highlight',
					'default'   => 'Default',
				),
				'desc'    => __( 'This is a select field', 'yith-mjpa-plugin-framework' ),
			),
			array(
				'id'   => 'yith-mjpa-number-radio-testimonials',
				'name' => __( 'Radio', 'yith-mjpa-plugin-framework' ),
				'type' => 'number',
				'min'  => '0',
				'max'  => '50',
				'step' => '1',
				'std'  => '0',
				'desc' => __( 'This is a number field', 'yith-mjpa-plugin-framework' ),
			),
			array(
				'id'   => 'yith-mjpa-colorpicker',
				'name' => __( 'Link color', 'yith-mjpa-plugin-framework' ),
				'type' => 'colorpicker',
				'std'  => '#000000',
				'desc' => __( 'This is a color-picker field', 'yith-mjpa-plugin-framework' ),
			),
			array(
				'type' => 'close',
			),
		),
		'section-2' => array(
			// here you can set another section of options
		),
	),
);

return apply_filters( 'yith_test_plugin_panel_settings_options', $settings );
